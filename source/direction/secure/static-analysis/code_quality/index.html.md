---
layout: markdown_page
title: "Category Direction - Code Quality"
description: "Automatically analyze your source code to surface issues and see if quality is improving or getting worse with the latest commit. Learn more!"
canonical_path: "/direction/secure/static-analysis/code_quality/"
---

- TOC
{:toc}

## Code Quality

Automatically analyze your static source code to surface issues and see if quality is improving or getting worse with the latest commit. Our vision for Code Quality is to provide actionable data across an organization to empower users to make quality visible with every commit and in every release.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ACode%20Quality)
- [Overall Vision](/direction/ops/#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in the issues where we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

To move closer to our long term Vision of a [Code Quality Dashboard](#code-quality-dashboard-vision) we need to first make sure that users are only looking at the Code Quality alerts that matter to them and that they get the context about the severity they need. The next capability that will move the category this way let teams enforce code quality standards by utilizing [Merge Request approvals](https://gitlab.com/gitlab-org/gitlab/-/issues/34982) with Code Quality scans. After working the code quality data into the developer workflow we will move to [gitlab#238858](https://gitlab.com/gitlab-org/gitlab/-/issues/238858) which lets teams set a threshold for the minimum severity level of issues to display from code quality scans.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is "Viable" (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- ~~[Display Code Quality Severity Rating](https://gitlab.com/gitlab-org/gitlab/-/issues/2529)~~ [Delivered in 13.6](https://about.gitlab.com/releases/2020/11/22/gitlab-13-6-released/#display-code-quality-severity-ratings).
- ~~[Show code quality notices on diffs/MRs](https://gitlab.com/groups/gitlab-org/-/epics/4609)~~ [Delivered in 14.1](https://about.gitlab.com/releases/2021/07/22/gitlab-14-1-released/#inline-code-quality-notices-on-mr-diffs)
- [Do not display issues below set threshold](https://gitlab.com/gitlab-org/gitlab/-/issues/238858)
- [Prevent merge on code quality degradation](https://gitlab.com/gitlab-org/gitlab/-/issues/34982)

We may find in research that only some of these issues are needed to move the vision for this category forward. The work to move the vision is captured and being tracked in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/3686).

## Competitive Landscape

### SonarQube

SonarQube is a commonly used static analysis tool that provides a user information about quality and security problems in their code. Some of the notable features users we hear about from users are the quality gate, blocking a merge request until issues are resolved, and the letter grade provided by the tooling. We understand this letter grading to mean that a high level and easy to understand and track quality measure is provided so team leads and directors can see when a project moves from an F to a C when it comes to quality. Many users we talk to want to get this kind of data in GitLab through the Code Quality feature set OR the SonarQube->GitLab integration, but they would prefer to have one fewer tool to manage.

### Azure DevOps

Azure DevOps does not offer in-product quality testing in the same way we do with CodeClimate, but does have a number of easy to find and install plugins in their [marketplace](https://marketplace.visualstudio.com/search?term=code%20quality&target=AzureDevOps&category=All%20categories&sortBy=Relevance) that are both paid and free. Their [SonarQube plugin](https://marketplace.visualstudio.com/items?itemName=SonarSource.sonarqube) appears to be the most popular, though it seems to have some challenges with the rating.

In order to remain ahead of Azure DevOps, we should continue to push forward the feature capability of our own open-source integration with CodeClimate. Issues like [Code Quality report for default branch](https://gitlab.com/gitlab-org/gitlab-ee/issues/2766) moves both our vision forward as well as ensures we have a high quality integration in our product. To be successful here, though, we need to support formats Microsoft-stack developers use. The current Code Quality scanner has some limited scanning capability but the issue [Support C# code quality results direction](https://gitlab.com/gitlab-org/gitlab/issues/29218) extends this to be more in line with scanning provided for other languages. Because CodeClimate does not yet have deep .NET support, we may need to build something ourselves.

## Top Customer Success/Sales Issue(s)

The field teams have told us that the top priorities for customers looking to replace a competitor and get the full value of GitLab are Quality Gates and a Quality Dashboard. We have a good understanding about quality gates and are working to resolve this issue by resolving [Prevent merge on code quality degradation](https://gitlab.com/gitlab-org/gitlab/-/issues/34982). We are investigating what outcomes customers are looking to get from a Quality Dashboard and will iterate on a solution in our [MVC issue](https://gitlab.com/gitlab-org/gitlab/-/issues/322021). 

## Top Customer Issue(s)

The top customer request currently is to allow for [multiple code quality](https://gitlab.com/gitlab-org/gitlab/-/issues/9014) reports to in the full report. We believe customers are running multiple scanners besides the one provided by the GitLab template to get around other issues such as DinD and limitations with pulling from DockerHub. Not being able to see these reports natively within GitLab may result in them finding another solution for their code quality needs.

Another top customer priority is to be able to see the [Code quality report for default branch](https://gitlab.com/gitlab-org/gitlab/-/issues/2766) which will let developers get information about code quality issues in the default branch outside of a pipeline or MR context. 

## Top Internal Customer Issue(s)

A top ask from our internal customers they want to enforce code quality standards accross departments by enforcing code quality cannot decrease in a [merge request without an approval](https://gitlab.com/gitlab-org/gitlab/-/issues/34982).

## Top Vision Item(s)

Our vision for Code Quality is for it to become another rich signal of confidence for users of GitLab. This will be not just a signal of the quality of a change but one of many inputs like Code Coverage to be able to view a project at a high level and make decisions about what code needs attention, additional tests or refactoring, to bring it up to the quality requirements of the group. This long term vision is captured in the issues [Instance wide code statistics](https://gitlab.com/gitlab-org/gitlab/-/issues/8406) and [Code Quality Dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/207117) and the team has started brainstorming what this may look like by creating wireframes like the design below.

As we think about the future of the Code Quality category we realize that there are limitations in what the open source scanner we have used to date provides. Adding additional scanners starting with the [linters](https://gitlab.com/gitlab-org/gitlab/-/issues/231538) in super linter not present in the existing scanner would be a first step towards this vision and dropping support for the current engine that requires DinD.

## Code Quality Dashboard Vision

![Design for Vision of Code Testing and Coverage data summary](/images/code-testing-data-view-vision.png)
