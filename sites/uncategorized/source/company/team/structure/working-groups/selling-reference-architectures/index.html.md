
---
layout: markdown_page
title: "Selling Reference Architectures Working Group"
canonical_path: "/company/team/structure/working-groups/selling-reference-architectures/"
description: "Learn more about the Selling Reference Architectures working group goals, processes, and teammates!"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | December 10, 2021 |
| Target End Date | May 10, 2021|
| Slack           | #wg_selling_ref_architectures |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1neXHEgNlfv7ck49ik5-whgo-zvyYeT-dH1KZUA4CA-M/edit?usp=sharing) (only accessible by GitLab team members) |
| Internal Docs   | [Wiki](https://gitlab.com/gitlab-org/quality/performance/-/wikis/home) (only accessible to GitLab team members) |
| Issues          | TBD |
| Label           | TBD |
| Associated KPIs/OKRs | TBD |
| GitLab group for working group| TBD|


## Business Goals
TBD 

### Protocols and Processes

1. We will have a recurring weekly sync with the working group 
1. We will use a issue board for assigning tasks and delegating efforts across the working group 
1. We will track efforts in phases, which will be broken up into epics 

### Exit Criteria

1. We have defined a list of metrics, used in the sales lifecycle, that help field team members and our customers select and implement the most appropriate reference architecture for their needs.
2. We have documented recommendations for Gitaly Cluster


## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     |  |  |
| Facilitator           | Sarah Waldner | Group Manager, Product |
| Contributor           | | |
| Functional Lead       |  |  |
| Reviewer              | | |

## Meetings

Meetings are recorded and available on
YouTube in the Working Group - Selling Reference Architectures playlist (TBD). The playlist is private. 

