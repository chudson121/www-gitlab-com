---
layout: job_family_page
title: "Corporate Events"
---
 
## Levels

### Corporate Events Coordinator 

The Corporate Events Coordinator reports to Manager, Corporate Events.

#### Corporate Events Coordinator Job Grade

The Corporate Events Manager (Intermediate) is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Corporate Events Coordinator Responsibilities

* Work cross-functionally with internal and external stakeholders to develop end-to-end programs
* Collaborate across teams to support initiatives led by the Corporate Events Team, ranging from branded events to sponsored events to internal events. 
* Manage post-program deliverables,including event analysis, reports of budget, attendee feedback
* Manage and on-board different vendor and partner contractors
* Coordinate internal processes & deliverables (ie email support, campaign elements & documentation)
* Think about how to connect the brand to all 

#### Corporate Events Coordinator Requirements

* Relevant, progressive experience
* Capability to coordinate across many teams and perform in fast-moving startup environment
* Outstanding written and verbal communications skills
* You embrace our values, and work in accordance with those values.
* Self-sufficient worker capable of managing multiple deadlines with little supervision.
* Ability to work smart under pressure and efficiently on multiple project requests simultaneously, and to deal with potential for tight deadlines and unexpected complexities.
* Exemplary communicator and collaborator; able to work with a variety of teams and personalities, possessing excellent verbal and written communications skills.
* Very detail-oriented.
* Ability to use GitLab
* Ability to travel if needed and comply with the company’s travel policy. 
* If employed by GitLab Federal, LLC, team members need to also comply with the applicable vaccination policies.

### Corporate Events Manager (Intermediate)
 
The Corporate Events Manager (Intermediate) reports to Manager, Corporate Events.
 
#### Corporate Events Manager (Intermediate) Job Grade 
 
The Corporate Events Manager (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Corporate Events Manager (Intermediate) Responsibilities
 
* Oversee execution of corporate events  to amplify our brand story and image at national and international trade shows, internal events, user conferences, brand activations and all other events.
* Proactively manage and strategize all event needs. End-to-end event management, from lead-handling and content creation to venue selection and event execution.
* Establish event goals and budget: gather and track ROI, engagement analytics, and feedback to consistently assess and implement opportunities for improvement.
* Develop pre-event, at-event, and post-event marketing plans in conjunction with product marketing, content, alliances, demand generation and field teams.
* Advance, maintain, and manage external partners and vendors for multiple events and swag, including negotiating optimal terms.
* Partner with internal cross-functional teams to create event agendas, campaigns, and content that meet engagement targets.
* Oversee creation of event communications and promotion for event programs, in partnership with demand generation and sales development teams, to drive attendance.
* Develop and deliver event enablement to include pre-event briefings, at-event briefings, and drive post-event follow-up briefings and event retrospectives.
* Solicit speaking sessions for all relevant corporate conferences and industry trade shows.
 
#### Corporate Events Manager (Intermediate) Requirements
 
* Relevant, progressive experience
* Self-sufficient worker capable of managing multiple deadlines with little supervision.
* Ability to work smart under pressure and efficiently on multiple project requests simultaneously, and to deal with potential for tight deadlines and unexpected complexities.
* Exemplary communicator and collaborator; able to work with a variety of teams and personalities, possessing excellent verbal and written communications skills.
* Proven skills interacting with executive/senior management teams.
* Exemplary project management and decision-making skills.
* Very detail-oriented.
* Flexible work schedule and the availability to travel approximately 50% of the time.
* Ability to use GitLab
Ability to travel if needed and comply with the company’s travel policy. 
* If employed by GitLab Federal, LLC, team members need to also comply with the applicable vaccination policies.
* Additional Requirement for Europe candidates:
  * Must be in Europe and must be fully eligible to travel within the EU.
  * Regional knowledge and existing network of vendors in EMEA.

### Coordinator and Intermediate Performance Indicators 

* [Net new business created](/handbook/marketing/performance-indicators/#net-new-business-pipeline-created)
* [Pipe to Spend](/handbook/marketing/performance-indicators/#pipe-to-spend-per-marketing-activity)

### Senior Corporate Events Manager
 
The Senior Corporate Events Manager reports to Manager, Corporate Events and Branding.
 
#### Senior Corporate Events Manager Job Grade
 
The Senior Corporate Events Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Senior Corporate Events Manager Responsibilities
 
* Extends that of the Corporate Events Manager (Intermediate) responsibilities
 
#### Senior Corporate Events Manager Requirements

* Extends the Corporate Events Manager (Intermediate) Requirements
* Relevant, progressive experience.
* Ability to travel if needed and comply with the company’s travel policy. 
* If employed by GitLab Federal, LLC, team members need to also comply with the applicable vaccination policies.

#### Senior Corporate Events Manager Performance Indicators 

* [50% or more SOV compared to most published competitor](/handbook/marketing/performance-indicators/#50-or-more-sov-compared-to-most-published-competitor)
 
### Senior Global Events Manager

The Senior Global Events Manager reports to the Manager, Brand Activation.

#### Senior Global Events Manager Job Grade

The Senior Global Events Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Global Events Manager Requirements

* Extends the Senior Corporate Event Manager Responsibilities
* Exemplary analytical skills and proven aptitude to use data to optimize program performance and inform future strategies.
* Plans and operatea in a transparent manner for cross-organizational visibility and be a leader in sharing best practices with other corporate event managers.
Ability to travel if needed and comply with the company’s travel policy. 
* If employed by GitLab Federal, LLC, team members need to also comply with the applicable vaccination policies.

#### Senior Global Events Manager Responsibilities

* Extends the Senior Corporate Event Manager Requirements
* Relevant, progressive experience
* Capacity to easily transition from high level strategic thinking to creative and detailed execution.
* Excellent communicator with proven aptitude to clearly convey ideas and data in written and verbal presentations to a variety of audiences.
* Advocates for improvements to event quality, results, and efficiency that have particular impact across your team and others.
* Ability to solve complex problems of the highest scope and complexity for your team.
* Exerts significant influence on the overall objectives and long-range goals of the team.
* Helps shepherd the definition and improvement of our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
* Drive innovation on the team with a willingness to experiment and to boldly confront problems of immense complexity and scope.
* Actively seeks out difficult impediments to our efficiency as a team, propose and implement solutions that will enable the entire team to iterate faster.
* Represents GitLab and its values in public communication around broad initiatives, specific projects, and community contributions. Interact with customers and other external stakeholders as a consultant and spokesperson for the work of your team.
* Provides mentorship for all event managers on the team to help them grow in their tactical responsibilities and remove blockers to their autonomy.
* Confidently ships large scale campaigns and events with minimal guidance and support from other team members. Collaborate with the team on larger projects.

#### Senior Global Events Manager Performance Indicators

* [Net new business created](/handbook/marketing/performance-indicators/#net-new-business-pipeline-created)
* [Pipe to Spend](/handbook/marketing/performance-indicators/#pipe-to-spend-per-marketing-activity)
* [50% or more SOV compared to most published competitor](/handbook/marketing/performance-indicators/#50-or-more-sov-compared-to-most-published-competitor)
* [Marketing efficency ratio](/handbook/marketing/performance-indicators/#marketing-efficiency-ratio)

## Specialties
 
### Sales Events
 
* Focus on Sales specific Events
* Flexible work schedule and the ability to travel approximately 30% of the time.
 
### Production Manager
 
The Production Manager reports to Manager, Corporate Events and Branding.
 
#### Production Manager Responsibilities
 
* Ensuring there is a DRI for all content-related tasks for corporate marketing events to see through a successful execution of all elements of the event. This includes making sure all elements of branded events have an owner and reporting back on updates and deadlines.
* Working closely with event DRI, GitLab digital production and Local to provide quality AV systems and on‐site support for live corporate events. Collaborates to build event orders for audio, video, staging, and lighting systems. Assist in the planning and coordination of productions, including but not limited to scouting locations, talent, and crew.
* Working hand-in-hand with digital production on how content is used and promoted post-event. Potential writing and/or editing treatments and scripts for multimedia packages.
* Liaising with internal teams to assist with pre- and post-production for GitLab-branded audio and video projects and events, including but not limited to Contribute, Sales Events, Commit user conferences, Developer Evangelism, Community, Employment branding, sales enablement, social, and livestreams.
* Writing keynote scripts and acting as the day-of contact for speakers and running the day-of show for all event production-related needs for opening and closing keynotes.
* Working with brand, design, and social media to make sure brand is represented in slide decks.
* Managing local production staff and accounts.
* Owning the integrated marketing campaigns and connecting the dots between demand generation, content marketing, and corporate events.
* Managing the corporate events branded website design and execution to drive ticket sales and properly promote our events overall.
* Manage our self-hosted events Call For Proposals (CFP) process, working hand-in-hand with internal and external thought leaders on their talk submissions.
* Review slide decks and develop a speaker training program.
 
#### Production Manager Requirements
 
* Excellent written and verbal communication skills
* Exceptional organizational skills
* Relationships in the software DevOps space are a plus
* Substantial knowledge with live corporate events and conferences.
* Highly collaborative, fostering effective relationships across all parts of the business and must enjoy learning/sharing knowledge
* This position will require doing occasional work on‐site and includes occasional weekend work
* Ability to travel if needed and comply with the company’s travel policy. 
* If employed by GitLab Federal, LLC, team members need to also comply with the applicable vaccination policies.
* Anticipated travel 15 to 20%
* 5+ years in events work and 2+ in software or tech related space
 
### Manager, Corporate Events
 
The Manager, Corporate Events reports to Director, Corporate Events.
 
#### Manager, Corporate Events Job Grade
 
The Manager, Corporate Events is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Manager, Corporate Events Responsibilities
 
* Excel at all of the corporate events manager responsibilities above.
* Oversee strategic and creative development of corporate events to amplify our brand story and image at national and international trade shows and events.
* Strategically connect business priorities, goals, and key messages for complete brand experience.
* Develop and manage external partners and vendors for multiple events and swag, including negotiating optimal terms.
* Be a team player responsible to build out and manage processes that will ensure the success of our events across functional groups and with production partners.
* Perform all management tasks for managing an effective, results focused team.
* Participate with peers in creating and executing the Corporate Marketing strategy.

#### Manager, Corporate Events Requirements
 
* Relevant, progressive experience
* Strategic marketing experience that goes beyond event operations/production, and includes substantial understanding of marketing communications, campaigns, event messaging, product and corporate content, and customer experience.
* Self-sufficient worker capable of managing multiple deadlines with little supervision.
* Ability to work smart under pressure and efficiently on multiple project requests simultaneously, and to deal with potential for tight deadlines and unexpected complexities.
* Exemplary communicator and collaborator; able to work with a variety of teams and personalities, possessing excellent verbal and written communications skills.
* Proven skills interacting with executive/senior management teams.
* Exemplary project management and decision-making skills.
* Very detail oriented.
* Ability to travel if needed and comply with the company’s travel policy. 
* If employed by GitLab Federal, LLC, team members need to also comply with the applicable vaccination policies.
* Flexible work schedule and the ability to travel approximately 30% of the time.
 
### Senior Manager, Corporate Events 

The Senior Manager, Corporate Events reports to Director, Corporate Events. 

#### Senior Manager, Corporate Events Job Grade

The Senior Manager, Corporate Events is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager, Corporate Events Responsibilities

* Extends that of the Manager, Corporate Events responsibilities
* Works with leadership to collect overall feedback to provide a multi-faced events strategy.
* Manages additional resources and agencies to help the events team execute on responsibilities.
* Data-centric marketer that measures event success from agreed upon metrics year-to-year. 
* Provides vision for internal and external event series to make sure that is reflected on GitLab Web identities.
* Works cross-functionally after internal and external events to make sure they are merchandised successfully and integrated into content marketing, brand campaigns and GTM campaigns.

#### Senior Manager, Corporate Events Requirements

* Extends that of the Manager, Corporate Events requirements
* Relevant, progressive experience
* Background working with leadership to collaborate on corporate event strategy, goals and execution.
* Ability to think creatively to solve problems that arise.
* Proven experience managing budgets for large corporate marketing events. 
* Ability to travel if needed and comply with the company’s travel policy. 
* If employed by GitLab Federal, LLC, team members need to also comply with the applicable vaccination policies.

#### Manager and Senior Manager Performance Indicators

* [Net new business created](/handbook/marketing/performance-indicators/#net-new-business-pipeline-created)
* [Pipe to Spend](/handbook/marketing/performance-indicators/#pipe-to-spend-per-marketing-activity)
* [50% or more SOV compared to most published competitor](/handbook/marketing/performance-indicators/#50-or-more-sov-compared-to-most-published-competitor)
* [Marketing efficency ratio](/handbook/marketing/performance-indicators/#marketing-efficiency-ratio)
* [LTV / CAC ratio](https://about.gitlab.com/handbook/marketing/performance-indicators/#ltv--cac-ratio)

### Director, Corporate Events

Director, Corporate Events reports to VP, Corporate Marketing. 

### Director, Corporate Events Job Grade

The Director, Corporate Events is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, Corporate Events Responsibilities

* Extends that of Senior Manager, Corporate Events responsibilities.
* Thinks strategically and forward looking about overall team, budget and resources needed to execute corporate events.
* Sets OKRs and overall goals for the team. 
* Works collaboratively with leaders across the organization to align on objectives and overall structure for coroprate events.
* Shares overall results of events, and then takes feedback and data to iterate on future events.

#### Director, Corporate Events Requirements

* Extends that of the Senior Manager, Corporate Events requirements.
* Relevant, progressive experience
* Proven experience managing and growing teams.
* Proven experience managing agencies and contractors to accomplish goals.
* Ability to travel if needed and comply with the company’s travel policy. 
* If employed by GitLab Federal, LLC, team members need to also comply with the applicable vaccination policies.

#### Director Performance Indicators

* [Net new business created](/handbook/marketing/performance-indicators/#net-new-business-pipeline-created)
* [Pipe to Spend](/handbook/marketing/performance-indicators/#pipe-to-spend-per-marketing-activity)
* [50% or more SOV compared to most published competitor](/handbook/marketing/performance-indicators/#50-or-more-sov-compared-to-most-published-competitor)
* [Marketing efficency ratio](/handbook/marketing/performance-indicators/#marketing-efficiency-ratio)
* [LTV / CAC ratio](https://about.gitlab.com/handbook/marketing/performance-indicators/#ltv--cac-ratio)

## Career Ladder
 
The next step in the Corporate Events job family is not yet defined at GitLab.
 
## Hiring process
 
Candidates for these positions can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
 
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#conducting-a-screening-call) with one of our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the Hiring Manager
* Next, candidates will be invited to interview with 2-5 team members
* There may be a final executive interview

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
