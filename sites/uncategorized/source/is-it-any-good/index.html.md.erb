---
layout: markdown_page
title: Is it any good?
description: "Don't only take our word for it! GitLab is the only comprehensive DevOps platform being delivered as a single application. Learn more here!"
canonical_path: "/is-it-any-good/"
suppress_header: true
---

## Why is this page called 'Is it any good?'

When people hear about a new product they often ask if it is any good. [A Hacker News user once wrote](https://news.ycombinator.com/item?id=3067434): "Starting immediately, all raganwald projects will have a 'Is it any good?' section in the readme, and the answer shall be 'yes'." We were inspired and added that section to the [GitLab readme](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md#is-it-any-good).

Don't only take our word for it! GitLab is the only comprehensive [DevOps platform](/solutions/devops-platform/) being delivered as a single application, and our unique breadth in DevOps has enabled us to enjoy similarly [broad coverage across analysts’ categories and reports](/analysts/).

The GitLab solution is the result of collaboration between [thousands of community contributors](http://contributors.gitlab.com/), based on feedback from the over 100,000 organizations using GitLab to power their [DevOps transformations](/topics/devops/). The GitLab community contributes code, documentation, translation, design, and product ideas based on their real world challenges, making GitLab more useful and valuable every day.

Because of the community behind GitLab, in many cases we see rapid adoption through better developer experience and collaboration. We've [liked thousands of supportive tweets](https://twitter.com/gitlab/likes) from [our 100,000+ followers on Twitter](https://twitter.com/gitlab/).

This page shares more of the many acknowledgements GitLab has enjoyed.

## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab is the most used DevOps tool

GitLab now has more than **30 million estimated registered users**!

This is an estimate, since not all self-managed instances of GitLab report back their user count. How did we arrive at 30 million? We currently use the [Service Ping](https://docs.gitlab.com/ee/development/service_ping/) to estimate the total registered users. The Service Ping is an optional feature for Self-Managed instances. To date, ~68% of paid Self-Managed instances have shared their usage data with GitLab.

`Estimated Lifetime Total Registered Users (TRU)` = `Lifetime Estimated Self-Managed Free Users` + `Lifetime Estimated Self-Managed Paid Users` + `Lifetime Self-Managed EDU Users` + `Lifetime GitLab.com SaaS Users`

- `Lifetime Estimated Self-Managed Free Users` - this is estimated using the opt-in rate for Paid Accounts.
- `Lifetime Estimated Self-Managed Paid Users` - this is estimated using the opt-in rate for Paid Accounts.
- `Lifetime Self-Managed EDU Users` - this is known and is the lifetime highest user count per EDU account.
- `Lifetime GitLab.com SaaS Users` - this is known and is the TRU of GitLab.com.

In using this calculation we are making an assumption that paid installations have the service ping enabled at a similar rate as unpaid installations. Our [Customer Success group asks customers to turn service ping on](/handbook/customer-success/tam/service-ping-faq/) in order to better help our customers.

Here's the calculation with actual numbers:
- `16.21mm` * (1 / `0.68`) + `0.65mm` * (1 / `0.68`) + `0.20mm` + `6.20mm` = `31.19 million users` for July 2020

We have been hesitant to release these numbers as they are not exact. It isn’t possible for us to have exact numbers because we are an open source project that people can run without registering. Previously, we reported the number of GitLab installations, however, this lead people to mistaken the number of installations as our number of users.

All of these numbers are based on registered users since there are no publicly known numbers for active users reported by our competitors. Registrations on self-managed GitLab installations that were online but are no longer online are disregarded.

GitHub, which has [~50 million users](https://github.com/search?q=type:user&type=Users), is closed source software that is more frequently used to produce open source software. GitLab is [open core](https://en.wikipedia.org/wiki/Open-core_model) software that is more commonly used to write closed source software. Most software written is closed source. Typically (not always) closed source is a core part of a job, while contributing to open source typically (not always) is a smaller part of a job or something that happens outside work. The statement that GitLab has higher usage incorporates the expectation that a registered user in GitLab uses the application more frequently than a typical registered user in GitHub.

## GitLab is DevOps.com's 'Best DevOps Solution Provider'

[![DevOps Dozen Best Solution Provider graphic](/images/blogimages/DevOpsDozen2019SolutionProvider-500x250.png){: .small.left.wrap-text}](https://devops.com/5th-annual-devops-dozen-winners-announced/)

GitLab was [awarded the DevOps Dozen award for Best DevOps Solution Provider by industry leaders MediaOps and DevOps.com](https://devops.com/5th-annual-devops-dozen-winners-announced/), who wrote: "GitLab has really separated itself from the pack, from the breadth of its vision for an end-to-end solution to the many different ways that GitLab operates as a company."

"But it is not just that GitLab is different; GitLab has captured a broad swath of the market who feel bought into the company."

Once we had been chosen as finalists, in true GitLab fashion we used GitLab.com to openly create and share [our case for the top prize, still available as a page in our handbook](/handbook/marketing/strategic-marketing/dod12/2019-Best-Solution/).

## GitLab has been voted as G2 Crowd Leader

GitLab is values-driven and we place [the highest value on results](/handbook/values/#hierarchy). Above all, [we strive to improve our customers' results](/handbook/values/#customer-results).

Our customers and users have invariably voted GitLab a G2 Crowd Leader across dozens of categories, including recently:

| **Enterprise<br>Version Control**<br><a title="GitLab is a leader in Version Control Hosting on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Version Control Hosting on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1234/medal.svg" /></a> | **Enterprise CI**<br><a title="GitLab is a leader in Continuous Integration on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Continuous Integration on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1234/medal.svg" /></a> | **Enterprise<br>Peer Code Review**<br><a title="GitLab is a leader in Peer Code Review on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Peer Code Review on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1234/medal.svg" /></a> | <a title="GitLab is a leader in Easiest to Use on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Easiest to Use on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1251/medal.svg" /></a> | <a title="GitLab is a leader in Fastest Implementation on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Fastest Implementation on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1267/medal.svg" /></a> | <a title="GitLab is a leader in Highest User Adoption on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Highest User Adoption on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1271/medal.svg" /></a> |
|:-:|:-:|:-:|:-:|:-:|:-:|
| <a title="GitLab is a leader in Easiest to Do Business With on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Easiest to Do Business With on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1295/medal.svg" /></a> | **DevOps Platforms**<br><a title="GitLab is a leader in DevOps Platforms on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in DevOps Platforms on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1235/medal.svg" /></a> | **Application Release Orchestration**<br><a title="GitLab is a leader in Application Release Orchestration on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Application Release Orchestration on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1235/medal.svg" /></a> | **Dynamic Application Security Testing**<br><a title="GitLab is a leader in Dynamic Application Security Testing (DAST) on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Dynamic Application Security Testing (DAST) on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1235/medal.svg" /></a> | **Cloud Infrastructure Automation**<br><a title="GitLab is a leader in Cloud Infrastructure Automation on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Cloud Infrastructure Automation on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1235/medal.svg" /></a> | **Static App Security Testing**<br><a title="GitLab is a leader in Static Application Security Testing (SAST) on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Static Application Security Testing (SAST) on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1235/medal.svg" /></a> |
| **Continuous Integration**<br><a title="GitLab is a leader in Continuous Integration on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Continuous Integration on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1235/medal.svg" /></a> | **Version Control**<br><a title="GitLab is a leader in Version Control Hosting on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Version Control Hosting on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1235/medal.svg" /></a> | **Bug Tracking**<br><a title="GitLab is a leader in Bug Tracking on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Bug Tracking on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1235/medal.svg" /></a> | **Peer Code Review**<br><a title="GitLab is a leader in Peer Code Review on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Peer Code Review on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1235/medal.svg" /></a> | **Continuous Delivery**<br><a title="GitLab is a leader in Continuous Delivery on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Continuous Delivery on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1235/medal.svg" /></a> | **MM Version Control Hosting**<br><a title="GitLab is a leader in Version Control Hosting on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Version Control Hosting on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1233/medal.svg" /></a> |
| **MM Continuous Integration**<br><a title="GitLab is a leader in Continuous Integration on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Continuous Integration on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1233/medal.svg" /></a> | **MM Peer Code Review**<br><a title="GitLab is a leader in Peer Code Review on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Peer Code Review on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1233/medal.svg" /></a> | **MM Cloud Infrastructure Automation**<br><a title="GitLab is a leader in Cloud Infrastructure Automation on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Cloud Infrastructure Automation on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1233/medal.svg" /></a> | **Application Release Orchestration**<br><a title="GitLab is a leader in Application Release Orchestration on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Application Release Orchestration on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1287/medal.svg" /></a> | **Static App Security Testing**<br><a title="GitLab is a leader in Static Application Security Testing (SAST) on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Static Application Security Testing (SAST) on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1287/medal.svg" /></a> | **Peer Code Review**<br><a title="GitLab is a leader in Peer Code Review on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Peer Code Review on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1299/medal.svg" /></a> |
| **Dynamic App Security Testing**<br><a title="GitLab is a leader in Dynamic Application Security Testing (DAST) on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Dynamic Application Security Testing (DAST) on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1299/medal.svg" /></a> | **Cloud Infrastructure Automation**<br><a title="GitLab is a leader in Cloud Infrastructure Automation on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Cloud Infrastructure Automation on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1299/medal.svg" /></a> | **Version Control Hosting**<br><a title="GitLab is a leader in Version Control Hosting on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Version Control Hosting on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1299/medal.svg" /></a> | **DevOps Platforms**<br><a title="GitLab is a leader in DevOps Platforms on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in DevOps Platforms on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1299/medal.svg" /></a> | **Application Release Orchestration**<br><a title="GitLab is a leader in Application Release Orchestration on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Application Release Orchestration on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1299/medal.svg" /></a> | **Continuous Integration**<br><a title="GitLab is a leader in Continuous Integration on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Continuous Integration on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1299/medal.svg" /></a> |
| **Bug Tracking**<br><a title="GitLab is a leader in Bug Tracking on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Bug Tracking on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1299/medal.svg" /></a> | **SMB Continuous Integration**<br><a title="GitLab is a leader in Continuous Integration on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Continuous Integration on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1232/medal.svg" /></a> | **SMB Version Control**<br><a title="GitLab is a leader in Version Control Hosting on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Version Control Hosting on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1232/medal.svg" /></a> | **SMB Bug Tracking**<br><a title="GitLab is a leader in Bug Tracking on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Bug Tracking on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1232/medal.svg" /></a> | **High Performer, Europe**<br><a title="GitLab is a leader in Version Control Hosting on G2" href="https://www.g2.com/products/gitlab/reviews?utm_source=rewards-badge"><img style="width: 125px;" alt="GitLab is a leader in Version Control Hosting on G2" src="https://images.g2crowd.com/uploads/report_medal/image/1314/medal.svg" /></a> |   |

G2 have published hundreds of vetted customer and user reviews of GitLab, with those including: _"[Powerful team collaboration tool for managing software development projects](https://www.g2.com/products/gitlab/reviews/gitlab-review-1976773)," "[Great self-hosted, open source source control system](https://www.g2.com/products/gitlab/reviews/gitlab-review-436746)," "[Efficient, can trace back, easy to collaborate](https://www.g2.com/products/gitlab/reviews/gitlab-review-701837)," and "[Perfect solution for cloud and on-premise DevOps tool](https://www.g2.com/products/gitlab/reviews/gitlab-review-2388492)."_

## GitLab recognized as a 451 Firestarter by 451 Research

[![GitLab recognized by 451 Research as a 451 Firestarter](/images/blogimages/451FIRESTARTER2019.png){: .small.right.wrap-text}](/press/releases/2020-01-14-gitlab-recognized-as-451-firestarter.html)

[GitLab received a 451 Firestarter award from leading technology research and advisory firm 451 Research](/press/releases/2020-01-14-gitlab-recognized-as-451-firestarter.html), recognizing exceptional innovation within the information technology industry.

"451 Research has built its reputation on helping organizations understand innovation and disruption in the enterprise IT industry, and the Firestarter award is one of the ways we spotlight important trends and players," said Jay Lyman, 451 Research Principal Analyst.

"Innovative approaches from companies such as GitLab — with its open source software technology and community contributions as well as transparency with customers — merit the recognition of a 451 Firestarter award."

## GitLab ranked above GitHub as a top developer tool

[![GitLab ranked above GitHub in Axosoft top 20 Dev Tools for 2019](/images/blogimages/axosoft-top-20-devtools-2019.png){: .small.left.wrap-text}](https://blog.axosoft.com/top-developer-tools-2019/)

The results of the [Axosoft 2019 survey of over 1,000 software engineers from elite organizations across the world](https://blog.axosoft.com/top-developer-tools-2019/) ranked GitLab in the top 20 developer tools. As they put it, "We could have put together our own list of the best development tools, but for the last two years, we’ve asked our community instead."

"GitLab is giving GitHub a run for its money! GitLab climbed the ranks 4 spots and overtook GitHub for the first year. While GitHub had the most significant drop in rankings from number 3 to number 11 year-over-year."

### GitLab is ahead of GitHub in features

GitLab releases major features earlier than GitHub. Here are some GitLab features and their corresponding GitHub release dates:

GitLab feature | GitLab release | Recent GitHub Feature | GitHub release |
-------------- | -------------- | --------------------- | -------------- |
[Manual pipeline jobs](https://docs.gitlab.com/ee/ci/yaml/#whenmanual) | 2016-07-22 ([Release 8.10](https://gitlab.com/gitlab-org/gitlab/-/releases/v8.10.0-ee)) | [Manual trigger in workflows](https://github.com/github/roadmap/issues/78) | 2020-07-25 |
[GitLab Runner](https://docs.gitlab.com/runner/) | 2015-05-03 ([Runner Release v0.3.0](https://gitlab.com/gitlab-org/gitlab-runner/blob/master/CHANGELOG.md#v030-2015-05-03)) [(blog)](https://about.gitlab.com/blog/2015/05/03/unofficial-runner-becomes-official/) | [Self-hosted runner groups](https://github.com/github/roadmap/issues/73) | 2020-07-25 |
[Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast) | 2017-12-22 ([Release 10.3](https://gitlab.com/gitlab-org/gitlab/-/releases/v10.3.0-ee)) | [Code scanning (server)](https://github.com/github/roadmap/issues/85) | 2020-07-25 |
[Shared runners pipeline minutes quota](https://docs.gitlab.com/ee/user/admin_area/settings/continuous_integration.html#shared-runners-pipeline-minutes-quota) | 2017-01-22 ([Release 8.16](https://gitlab.com/gitlab-org/gitlab/-/releases/v8.16.0-ee)) | [Org admins in Enterprise accounts can view Actions and Packages usage/billing](https://github.com/github/roadmap/issues/61) | 2020-09-08 |
[GitLab Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/) | 2016-05-22 ([Release 8.8](https://gitlab.com/gitlab-org/gitlab/-/releases/v8.8.0-ee)) [(blog)](https://about.gitlab.com/blog/2016/05/23/gitlab-container-registry/) | [Packages: Support for GHES (Beta)](https://github.com/github/roadmap/issues/90) | 2020-09-28 |
[Geo](https://docs.gitlab.com/ee/administration/geo/) | 2016-06-22 ([Release 8.9](https://gitlab.com/gitlab-org/gitlab/-/releases/v8.9.0-ee)) | [GHES Cluster HA/DR](https://github.com/github/roadmap/issues/110) | 2020-09-28 |
[GitLab CI/CD](https://docs.gitlab.com/ee/ci/) | 2015-09-22 ([Release 8.0](https://gitlab.com/gitlab-org/gitlab/-/releases/v8.0.0-ee)) | [GitHub Actions for GHES](https://github.com/github/roadmap/issues/100) and [GitHub Actions on GitHub AE](https://github.com/github/roadmap/issues/71) | 2020-09-29 |
GitLab Docs, now found at [docs.gitlab.com](https://docs.gitlab.com) | 2013-06-06 ([First commit](https://gitlab.com/gitlab-com/doc-gitlab-com/-/commit/676f391285d5793a6d40193c3bf45f57b9731617)) | [Open Source Documentation for docs.github.com](https://github.com/github/roadmap/issues/112) | 2020-10-23 |
[Pipeline visualization](https://docs.gitlab.com/ee/ci/pipelines/#visualize-pipelines) | 2016-08-22 ([Release 8.11](https://gitlab.com/gitlab-org/gitlab/-/releases/v8.11.0-ee)) | [Actions: Workflow visualization](https://github.com/github/roadmap/issues/88) | 2020-12-11 |
[Protected manual jobs](https://docs.gitlab.com/ee/ci/yaml/#protecting-manual-jobs) | 2017-05-22 ([Release 9.2](https://gitlab.com/gitlab-org/gitlab/-/releases/v9.2.0-ee)) | [Actions: Manual approval in workflows](https://github.com/github/roadmap/issues/99) | 2020-12-18 |

<div class="center">

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">In December GitLab CI ran 2^8 million builds! (256 million) GitHub Actions is now at &quot;70 million jobs run per month&quot; according to <a href="https://t.co/bpHjfbdk5h">https://t.co/bpHjfbdk5h</a> GitLab was last below 70m in November of 2018. In some aspects GitLab is years ahead of GitHub: <a href="https://t.co/Bfh2Om6siV">https://t.co/Bfh2Om6siV</a></p>&mdash; Sid Sijbrandij (@sytses) <a href="https://twitter.com/sytses/status/1356104348061720576?ref_src=twsrc%5Etfw">February 1, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

</div>

This puts GitLab's featureset, on average, almost *four and a half years* ahead of GitHub's.

For more information, please see our [CI-CD Roadmap Comparison](https://about.gitlab.com/devops-tools/github-vs-gitlab/ci-cd-roadmap-comparison/), our [Version Control Roadmap Comparison](https://about.gitlab.com/devops-tools/github-vs-gitlab/vcc-roadmap-comparison/), and our [DevSecOps Roadmap Comparison](https://about.gitlab.com/devops-tools/github-vs-gitlab/security-roadmap-comparison/) pages.

Source: [GitHub roadmap](https://github.com/github/roadmap/projects/1)

## GitLab is the most popular SCM tool among JVM devs

[![1 in 3 developers use GitLab, making it the most popular SCM tool among JVM devs](/images/jvm_ecosystem_report_2020.png){: .large .margin-top20 .margin-bottom20}](https://snyk.io/blog/jvm-ecosystem-report-2020/)

The [JVM Ecosystem Report 2020](https://snyk.io/blog/jvm-ecosystem-report-2020/) data affirmed [what The New Stack saw in 2019](https://twitter.com/lawrencehecht/status/1225808065229926400?s=12) — that GitLab is outperforming GitHub in source code management.

In this annual JVM ecosystem report, 1 in 3 developers use GitLab, making it the most popular SCM tool among Java (Java Virtual Machine, or JVM) developers.

As Snyk reports, "It is possibly surprising for people to hear that GitLab is the winner of this battle... On top of that, GitLab offers a lot more than just a repository, including CI."

## GitLab CI is a leader in the The Forrester Wave™

[![Forrester Wave graphic](/images/home/homepage-forrester-wave-2019.png){: .small .margin-top20 .margin-bottom20}](/resources/forrester-wave-cloudnative-ci/)

[Forrester evaluated GitLab as a Leader in Cloud-Native Continuous Integration in The Forrester Wave™: Cloud-Native Continuous Integration Tools, Q3 2019 report.](/resources/forrester-wave-cloudnative-ci/)

As the report states, “GitLab’s simple and cohesive approach lands it squarely as a leader.”

## GitLab recognized as an IDC Innovator in Tools Supporting Open Developer Platforms

[![GitLab recognized as an IDC Innovator in Tools Supporting Open Developer Platforms](/images/blogimages/IDCInnovator2019.png){: .small.right.wrap-text}](https://www.idc.com/getdoc.jsp?containerId=US45074219)

[GitLab was recognized as an IDC Innovator in Tools Supporting Open (Unopinionated) Developer Platforms](https://www.idc.com/getdoc.jsp?containerId=US45074219) in May, 2019 by International Data Corporation.

IDC Innovators are emerging vendors "that have demonstrated either a groundbreaking business model or an innovative new technology — or both."

## GitLab has YoY growth in adoption of version control services study, while GitHub and Bitbucket both decline

[![YoY Adoption of Version Control Services Graph](/images/blogimages/yoy-adoption-vcs-2018-2019.svg){: .medium .margin-top20 .margin-bottom20}](https://thenewstack.io/i-dont-git-it-tracking-the-source-collaboration-market/)

[Source: New Stacks's analysis of the JetBrains Developer Ecosystem surverys](https://thenewstack.io/i-dont-git-it-tracking-the-source-collaboration-market/)

## GitLab ranked number 4 software company (44th overall) on Inc. 5000 list of 2018's Fastest Growing Companies

[![GitLab ranked number 4th fastest-growing private software company on the Inc. 5000 list of 2018's Fastest Growing Companies](/images/blogimages/inc-5000-2018-no4-software.png){: .small.right.wrap-text}](/blog/2018/08/16/gitlab-ranked-44-on-inc-5000-list/)

In 2018, GitLab was America's [4th fastest-growing private software company and 44th overall on the Inc. 5000 list](/blog/2018/08/16/gitlab-ranked-44-on-inc-5000-list/) with revenue growth of 6,213 percent over the previous three years. 2018 was the first year GitLab appeared on the Inc. 5000 list.

The 2018 Inc. 5000 ranking system was based on the percentage of revenue growth qualifying companies saw from 2014 to 2017. For consideration, companies needed to be private, for-profit, independent and U.S.-based as of December 31, 2017. The companies must have also been incorporated by March 31, 2014 with a minimum revenue of $200,000 for that year and $2 million for 2017.

## GitLab is a top 3 innovator in IDC's list of Agile Code Development Technologies for 2018

[![IDC innovators banner](/images/logos/idc-innovator.png){: .margin-top20 .margin-bottom20}](/analysts/idc-innovators/)

[IDC recognized GitLab as a top 3 IDC Innovator in Agile Code Development Technologies](/analysts/idc-innovators/) after considering an overall assessment, key differentiators, challenges in the market, and related research.

## GitLab is a strong performer in the new Forrester Value Stream Management Tools 2018 Wave Report

[![Forrester VSM Wave graphic](/images/home/forrester-vsm-graphic.png){: .small .margin-top20 .margin-bottom20}](/analysts/forrester-vsm/)

[Forrester evaluated GitLab as a strong performer for VSM capabilities on top of its end to end DevOps capabilities.](/analysts/forrester-vsm/)

Forrester's assessment included: "GitLab combines end-to-end capability with the power of open source. GitLab offers a DevOps tool for each step of the software development process. Top-level views sitting across these tools provide its VSM functionalities."

They added: "GitLab is best for companies that are looking for a broad, integrated solution. Organizations that want a comprehensive VSM solution that can also serve as their DevOps tool chain will really appreciate GitLab. GitLab's vision is firmly open source and dev-centric; companies that live and breathe dev-first will appreciate this approach."

## GitLab has 2/3 market share in the self-managed Git market

With more than 100,000 organizations self-hosting GitLab, we have the largest share of companies who choose to host their own code. We’re estimated to have two-thirds of this single-tenant market.

When [Bitrise surveyed](http://blog.bitrise.io/2017/01/27/state-of-app-development-in-2016.html#self-managed) ten thousand developers who build apps regularly on their platform, they found that 67 percent of self-managed apps prefer GitLab’s on-premise solution.

[![Image via Bitrise blog](/images/blogimages/bitrise-self-hosted-chart.png){: .medium}](http://blog.bitrise.io/2017/01/27/state-of-app-development-in-2016.html#self-managed)<br>

Similarly, in their survey of roughly one thousand development teams, [BuddyBuild found](https://www.buddybuild.com/blog/source-code-hosting#selfhosted) that 79% of mobile developers who host their own code have chosen GitLab:

[![Image via buddybuild blog](/images/blogimages/buddybuild-self-hosted-chart.png){: .medium}](https://www.buddybuild.com/blog/source-code-hosting#selfhosted)<br>

In their articles, both Bitrise and BuddyBuild note that few organizations use self-managed instances. We think there is a [selection effect](https://en.wikipedia.org/wiki/Selection_bias), because both of them are SaaS-only offerings.

Based on our experience, the vast majority of enterprises ([organizations with 2,000+ employees](/handbook/sales/field-operations/gtm-resources/#segmentation)) self-host their source code server, frequently on a cloud service like AWS or GCP, instead of using a SaaS service.

Another assumption is that Git is the most popular version control technology for the enterprise. Certainly not every enterprise has completely switched yet, but it does seem that Git is [almost 10 times larger than SVN and Mercurial](https://trends.google.com/trends/explore?q=git,svn,perforce,mercurial,tfs).

Per the dominance of Git, the SCM market is lacking analysis that is as complete, independent, and directly relevant to the question of market share as GitLab would prefer, but with these qualifications and multiple sources, we find the data and discussions sufficient to share. GitLab has encouraged additional, independent analysts to address the SCM market and will continue to do so.

## GitLab CI is the fastest growing CI/CD solution

In his post on [building Heroku CI](https://blog.heroku.com/building-tools-for-developers-heroku-ci) in mid 2017, Heroku’s Ike DeLorenzo noted "GitLab is surging in VCS and CI (and in developer buzz)" and that [GitLab CI](/stages-devops-lifecycle/continuous-integration/) is “clearly the biggest mover in activity on Stack Overflow,” with more popularity than both Travis CI and CircleCI.

GitLab was the second most popular CI system by the end of that year, in [a report on cloud trends from Digital Ocean](https://assets.digitalocean.com/currents-report/DigitalOcean-Currents-Q4-2017.pdf).

[![Image via Digital Ocean](/images/ci/gitlab-popular-ci.png){: .medium}](https://assets.digitalocean.com/currents-report/DigitalOcean-Currents-Q4-2017.pdf)

GitLab's commitment to seamless integration extends to CI itself. Integrated CI/CD is more time and resource efficient than a set of distinct tools, and allows developers greater control over build pipelines, so they can spot issues earlier and address them at a lower cost. Tighter integration between different stages of the development process makes it easier to cross-reference code, tests, and deployments while discussing them, allowing you to see more context and iterate more rapidly.

We've heard from customers like [Ticketmaster](/blog/2017/06/07/continous-integration-ticketmaster/) that adopting GitLab CI can transform the entire software development lifecycle (SDLC), in their case helping the Ticketmaster mobile development team deliver on the longstanding goal of weekly releases. As more companies look to embrace CI as part of their development methodology, having CI fully integrated into their overall SDLC solution will ensure these companies are able to realize the full potential of CI. You can read more about the benefits of integrated CI in our white paper, [Scaling Continuous Integration](/resources/scaled-ci-cd/).

## GitLab is one of the top 30 open source projects

The Cloud Native Computing Foundation placed us in the top right quadrant of their [Highest Velocity Open Source Projects](/blog/2017/07/06/gitlab-top-30-highest-velocity-open-source/) in 2017, by measuring GitLab commits, authors, issues, and merge requests.

## Our customers love sharing their successes

GitLab customers are often asked to speak at events. [Hear their stories and learn from their experiences](/customers/marketplace/).

## GitLab has more than 3,000 contributors

[Our more than 3,000 contributors have more than 9,000 merge requests merged](http://contributors.gitlab.com/), including those from contributing customers and organizations such as [Siemens](/blog/2018/12/18/contributor-post-siemens/), [CERN](/blog/2016/11/23/gitlab-adoption-growing-at-cern/), and [Creationline](/blog/2019/11/27/creationline-post/).

## GitLab is SOC2 Type 1 certified

SOC2 certification, developed by [AICPA](https://www.aicpa.org/interestareas/frc/assuranceadvisoryservices/aicpasoc2report.html), defines requirements to minimize risk and exposure of customer data stored in the cloud. SOC2 goes beyond just a technical audit - vendors like GitLab complying to SOC2 are required to follow strict information security policies and procedures, encompassing the security, availability, processing, integrity, and confidentiality of customer data.

As of March 2020, our SaaS offering GitLab.com is certified for **SOC2 Type 1**. This certification report clarifies a majority of the security questions you may have for a SaaS vendor. Due to the sensitive nature of the content in the report, it cannot be distributed publicly. Request for your copy of the report [here](/handbook/engineering/security/security-assurance/security-compliance/certifications.html#requesting-a-copy-of-the-gitlab-soc2-type-2-report).

## GitLab is One of the 10 Coolest DevOps Tools Of 2021
CRN took a look at a sampling of some of the [coolest DevOps tools of 2021](https://www.crn.com/slide-shows/cloud/the-10-coolest-devops-tools-of-2021-so-far-/5) and listed GitLab in the top 10.

Cisco’s Eric Thiel, Director, Developer Experience is quoted saying: “Our team has also been spending a lot of time in GitLab and GitLab CI and really enjoys having such a broad range of DevOps capabilities all in a single tool."



