---
layout: handbook-page-toc
title: "Integrated Campaigns"
description: "GitLab Marketing Handbook: Integrated Campaigns"
twitter_image: '/images/tweets/handbook-marketing.png'
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

## Global Integrated Campaigns
{: #integrated-campaigns .gitlab-purple}

The goal of GitLab integrated campaigns is to strategically land a cohesive message to a target audience across a variety of channels and offers dependent on the goals of the campaign. Content types include a combination of blog posts, webinars, whitepapers, reports, videos, case studies, and more. Channels include digital ads, social, paid social, SEO, PR, email, and more.

**Campaign Managers** are responsible for the strategizing of global integrated campaigns and organizing timelines and DRIs for deliverables to execute on the campaigns. Campaign Managers are also aligned to specific segments and geos to define strategy for new first order logo MQLs and SAOs, aligned to pipeline goals.

In an effort to continually improve our ability to derive success of campaigns, reporting is a focus using Bizible data in Sisense. [Issue here for more information on progress](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/982)

**Questions? Please feel free to ask in the [#marketing-campaigns slack channel](https://gitlab.slack.com/archives/CCWUCP4MS).**

## Anchor Campaigns and Activation in Geos
{: #anchor-campaigns-and-activation .gitlab-purple}

Global campaigns 

<div style="width: 600px;" class="embed-thumb"> <div style="position: relative; height: 0;overflow: hidden; height: 400px; max-width: 800px; min-width: 320px; border-width: 1px; border-style: solid; border-color: #d8d8d8;"> <div style="position: absolute;top: 0;left: 0;z-index: 10; width: 600px; height: 100%;background: url(https://murally.blob.core.windows.net/thumbnails/gitlab2474/murals/gitlab2474.1598468466093-5f46b1720e54f644475491fc-61c7931a-18e2-4a73-90dd-eafcb724b939.png?v=27e40478-1cf5-43f3-8970-f6d66232a7d1) no-repeat center center; background-size: cover;"> <div style="position: absolute;top: 0;left: 0;z-index: 20;width: 100%; height: 100%;background-color: white;-webkit-filter: opacity(.4);"> </div> <a href="https://app.mural.co/t/gitlab2474/m/gitlab2474/1598468466093/3fecfe10c92a0e8b26403fe8e44305c6049185e4" target="_blank" rel="noopener noreferrer" style="transform: translate(-50%, -50%);top: 50%;left: 50%; position: absolute; z-index: 30; border: none; display: block; height: 50px; background: transparent;"> <img src="https://app.mural.co/static/images/btn-enter-mural.svg" alt="ENTER THE MURAL" width="233" height="50"> </a> </div> </div> <p style="margin-top: 10px;margin-bottom: 60px;line-height: 24px; font-size: 16px;font-family: Proxima Nova, sans-serif;font-weight: 400; color: #888888;"> You will enter this mural in View Only mode. </p></div>

<iframe src='https://app.mural.co/embed/9f6f85dc-e19e-4d54-851a-c0a786871c1c'
        width='100%'
        height='480px'
        style='min-width: 640px; min-height: 480px; background-color: #f4f4f4; border: 1px solid #efefef'
        sandbox='allow-same-origin allow-scripts allow-modals allow-popups allow-popups-to-escape-sandbox'>
</iframe>

## In Progress and Future Campaigns
{: #upcoming-integrated-campaigns .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

FY22 Integrated Campaigns and GTM Motions TBD. **[See Epic for FY21 Integrated Camapigns >>](https://gitlab.com/groups/gitlab-com/marketing/-/epics/749)**. 

## Active Integrated Campaigns
{: #active-integrated-campaigns .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

### 🚀 Competitive campaign 3.0
{: #competitive-campaign-3}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Agnes Oetama**

**Launched (Original): 2020-11-16**

* [Parent epic with tactical issues](https://gitlab.com/groups/gitlab-com/-/epics/1029)
* [Live landing page - Manager Persona](https://about.gitlab.com/blog/2019/11/26/migrating-from-jenkins/)
* [Live landing page - Practitioner Persona](https://page.gitlab.com/gitlabci-vs-jenkins-demo.html)
* [SFDC Campaign - Manager >>](https://gitlab.my.salesforce.com/7014M000001ll99?srPos=0&srKp=701)
* [SFDC Campaign - Practitioner >>](https://gitlab.my.salesforce.com/7014M000001dlvz?srPos=0&srKp=701)

### 🚀 GitOps Use Case
{: #gitops-use-case}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Eirini Pan**

**Launched: 2020-06-22**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/745)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/891)
* [Live landing page](https://about.gitlab.com/why/gitops-infrastructure-automation/)
* [Slack](https://gitlab.slack.com/archives/C0119FNPA84)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001dgJ9)

### 🚀 Version Control & Collaboration (VC&C) Use Case
{: #vcc-use-case}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Jenny Tiemann**

**Launched: 2020-06-09**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/742)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/927)
* [Live landing page](hhttps://about.gitlab.com/why/simplify-collaboration-with-version-control/)
* [Slack](https://gitlab.slack.com/archives/CVB3AKJNA)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001dfsw)

### 🚀 France CI Localized
{: #ci-france-localized}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Eirini Pan**

**Launched: 2020-05-15**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/752)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/956)
* [Live landing page](https://about.gitlab.com/fr/pourquoi/integration-continue-pour-construire-et-tester-plus-rapidement/)
* [Slack](https://gitlab.slack.com/archives/C012N4QKYQY)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001dfjQ)

### 🚀 Germany CI Localized
{: #ci-germany-localized}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Indre Kryzeviciene**

**Launched: 2020-04-29**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/809)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/982)
* [Live landing page](https://about.gitlab.com/de/warum/nutze-continuous-integration-fuer-schnelleres-bauen-und-testen/)
* [Slack](https://gitlab.slack.com/archives/C012QLG1NJD)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001dfqW)

### 🚀 DevSecOps Use Case
{: #devsecops-use-case}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Megan Mitchell**

**Launched: 2020-04-29**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/743)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/874)
* [Live landing page](https://about.gitlab.com/solutions/dev-sec-ops/)
* [Slack](https://gitlab.slack.com/archives/CV8GZ63GR)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001df4w?srPos=0&srKp=701)

### 🚀 CI Use Case
{: #ci-use-case}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Agnes Oetama**

**Launched: 2020-04-27**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/741)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/873)
* [Live landing page](https://about.gitlab.com/why/use-continuous-integration-to-build-and-test-faster/)
* [Slack](https://gitlab.slack.com/archives/CVDQL20BA)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001df8y)

### 🚀 AWS Partner
{: #aws-partner}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Nout Boctor-Smith**

**Launched: 2019-09-22**

* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/624)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/638)
* [Live landing page](https://about.gitlab.com/webcast/aws-gitlab-serverless/)
* [Slack](https://gitlab.slack.com/archives/CRTE89C66)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001loj0)


## Past Integrated Campaigns
{: #past-integrated-campaigns .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

### 🌄 Operation OctoCat
{: #operation-octocat}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Jackie Gragnola**

**Launched: 2019-10-31**

* *Note: this is being sunsetted Q2 FY21 and existing records in the nurture will move to the CI use case campaign nurture.*
* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/439)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/445)
* [MURAL of campaign user journey](https://app.mural.co/t/gitlab5736/m/gitlab5736/1584992460277/f66e45f8067dded0ae81d711aeb897c1547e2a80)
* [Live landing page](/compare/github-actions-alternative/)
* [Campaign brief](https://docs.google.com/document/d/1Mcy_0cwMsTPIxWUXPgoqw9ejsRJxaZBHi4NikYTabDY/edit#heading=h.kf9lglu57c0t)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001lmdK)

### 🌄 Increase operational efficiencies
{: #increase-operational-efficiencies}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Jenny Tiemann**

**Launched: 2019-08-23**

* *Note: this is being sunsetted in Q4 FY21 and existing records in nurture will continue until all emails have deployed.*
* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/367)
* [Live landing page](/topics/devops/reduce-devops-costs/)
* [Slack](https://gitlab.slack.com/archives/CCWUCP4MS)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000000CyiL)

### 🌄 Deliver better products faster
{: #deliver-better-products-faster}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Zac Badgley**

**Launched: 2019-08-23**

* *Note: this is being sunsetted Q2 FY21 and existing records in the nurture will move to the CI use case campaign nurture.*
* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/363)
* [Live landing page](/blog/2018/10/12/strategies-to-reduce-cycle-times/)
* [Campaign brief](https://docs.google.com/document/d/1dbEf1YVLPnSpFzSllRE6iNYB-ntjacENRMjUxCt8WFQ/edit)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000000Cyhm?srPos=0&srKp=701)

### 🌄 Reduce security and compliance risk
{: #reduce-security-compliance-risk}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Originally Jackie Gragnola, transitioned to Megan Mitchell**

**Launched: 2019-08-23**

* *Note: this is being sunsetted Q2 FY21 and existing records in the nurture will move to the DevSecOps use case campaign nurture.*
* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/368)
* [Live landing page](/solutions/dev-sec-ops/)
* [Campaign brief](https://docs.google.com/document/d/1NzFcUg-8c1eoZ1maHHQu9-ABFfQC65ptihx0Mlyd-64/edit)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000000CyeJ)

### 🌄 CI Build & Test Auto
{: #ci-build-test-auto}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Agnes Oetama**

**Launched: 2019-08-23**

* *Note: this is being sunsetted Q2 FY21 and existing records in the nurture will move to the CI use case campaign nurture.*
* [Epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/379)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/432)
* [Live landing page](/resources/ebook-single-app-cicd/)
* [SFDC campaign](https://gitlab.my.salesforce.com/7014M000001lkp9)

### 🚀 Competitive campaign
{: #competitive-commit}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Agnes Oetama**

**Launched (Original): 2019-04-02**

* [Parent epic with tactical issues](https://gitlab.com/groups/gitlab-com/marketing/-/epics/10)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/85)
* [Live landing page](/resources/ebook-single-app-cicd/)
* [SFDC Campaign >>](https://gitlab.my.salesforce.com/70161000000VxvJ)

### 🌄 Just Commit
{: #just-commit}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Campaign Manager: Jackie Gragnola**

**Launched: 2019-02-18**

* [Parent epic with child epics and issues >>](https://gitlab.com/groups/gitlab-com/marketing/-/epics/7)
* [SDR & Sales enablement epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/50)
* [Former landing page (repurposed since close of the campaign)](/blog/2019/03/27/application-modernization-best-practices/)
* [SFDC Campaign](https://gitlab.my.salesforce.com/70161000000VwZq)
* [Meeting recordings >>](https://drive.google.com/drive/u/1/folders/147CtTEPz-fxa0m1bYxZUbOPBik-dkiYV)
* [Meeting slide deck >>](https://docs.google.com/presentation/d/1i2OzO13v77ACYo1g-_l3zV4NQ_46GX1z7CNWFsbEPrA/edit#slide=id.g153a2ed090_0_63)

## Campaign Planning
{: #campaign-planning .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

**There will be an epic for every campaign - created by the Campaign Manager managing the campaign - and a content pillar will align to the campaign, including gated assets to be created, pages to be created/revamped, blog posts, etc.**

* The campaign will have a clear launch date
* The new resources and webpages will have a clear due date prior to the launch date or defined for a cascading roll-out of content post-launch
* All action items will have DRIs and a timeline, working back from the launch date
* The campaign will be determined at least 45 days prior to launch to allow for a proactive, not reactive, timeline

Ideally, campaigns will be agreed at least a quarter in advance to allow for ample planning and execution, prep time with agencies, creative concepting, and communication internally. This is a collaborative effort to deliver a cohesive program.

#### Overall Campaign Steps
{: #campaign-steps}
<!-- DO NOT CHANGE THIS ANCHOR -->

* **Campaign and launch date is determined by Marketing Leadership**
* **Assign:** Campaign Manager is assigned
* **Assign:** Marketing team leaders assign DRIs from their teams
* **Meeting:** Campaign Manager organizes campaign kickoff call with early-stage DRIs (Campaign Manager, DMP, PMM, TMM)
* **Meeting:** Campaign Manager organizes campaign brief call with early-stage DRIs (Campaign Manager, DMP, PMM, TMM)
* **Plan:** Campaign Manager creates project plan (GANTT chart) with timelines and DRIs
* **Plan:** Campaign Manager creates the epic and related issues, including the due dates and DRIs from project plan
* **Meeting:** Campaign Manager organizes the bi-weekly 30 minute check-in call to cover milestones met and determine any blockers/at-risk action items
* **Async:** *DRIs are responsible for delivering by their due dates, with timeline adherence being critical due to dependencies for later tasks to be completed by other teams*
* **Reporting:** Campaign Manager organizes reporting issue with clear DRI to include overall metrics and more detailed drill-in by channel (one month post-launch)
* **Optimization:** Campaign Manager creates issues for optimizing the landing page, channels, etc. and assigning to relevant DRIs

#### Integrated Campaign Meetings
{: #meetings}
<!-- DO NOT CHANGE THIS ANCHOR -->

## Campaign Manager Epic Creation
{: #epic-creation .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

#### Epic Code
{: #epic-code}
<!-- DO NOT CHANGE THIS ANCHOR -->

***The Campaign Manager of the integrated campaign will create the epic with the following structure - details and links to be added up on creation of documents and timeline defined.***

```
> NAME THIS EPIC: Name of Campaign [Integrated Campaign] (i.e. DevSecOps Use Case [Integrated Campaign])

> Start Date: date epic is opened, Due Date: launch date

## 🙌 Overview of _[Campaign Name]_ Integrated Campaign Epic

This is the parent epic, organizing the epics to and issues for the integrated campaign, launch date `TBD`. The related issues will be included below (upon rollout of the campaign) with _DRIs, due dates, and labels_ assigned appropriately.

_Please see related issues for details related to their respective purposes - this epic will be used for high level communication regarding the integrated campaign._

## Team
* Marketing Programs: [name] @[handle] 
* Digital Marketing: 
* Content Marketing: 
* Product Marketing: 
* Technical Marketing: 
* Evangelism: 
* Growth Marketing: 
* Web Design/Dev: 
* SDR: 
* Organic Social: 

### [Campaign Execution Timeline >>]() - Owned by Campaign Manager
### [Campaign Brief >>]() `to be added once created`
### [Live Campaign Page >>]() `to be added upon launch when live`
### [Google Drive >>]() `to be added once created`

## 🔗 UTM for tracking URLs
* Overall utm_campaign - `tbd`, i.e. **`utm_campaign=`** [determine UTM with DMP]
* More on [when](/handbook/marketing/demand-generation/digital-marketing/digital-marketing-management/#utm-tracking) and [how](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0) to use UTMs

## ⚡ Quick Links
* [Marketo Program]()
* [SFDC]()
* [Sisense Overview Dashboard](https://app.periscopedata.com/app/gitlab/631669/WIP:-Integrated-Campaigns-Overview-Dashboard)

## 👥 Target persona
**[Target Persona & Positioning Matrix]()**
* **Level:** [to be filled in after kickoff call]
* **Function:** [to be filled in after kickoff call]


/label ~"dg-campaigns" ~"mktg-demandgen" ~"Content Marketing" ~"Digital Marketing Programs" ~"Product Marketing" ~SDR ~"Campaign Manager Integrated" ~mktg-growtn ~tech-evangelism ~"Campaign Manager Priority" ~mktg-website ~design ~"Socil Media" ~"Strategic Marketing" ~"mktg-status::plan"
```

#### SDR Enablement Epic Creation
{: #epic-creation-sdr-enablement}
<!-- DO NOT CHANGE THIS ANCHOR -->

<details>
<summary>Hiding until discussed with SDR enablement for most scaled process moving forward</summary>

***The Campaign Manager of the integrated campaign will create the SDR enablement epic with the following structure - details and links to be added up on creation of documents and timeline defined. Name of the epic should be `Operation OctoCat - SDR & Sales Enablement`***

```
# [See parent epic >>]()

## Key Links
* :white_check_mark: **[Live campaign page >>]()**
* :memo: **[Campaign brief >>]()**
* :busts_in_silhouette: **[Persona / positioning matrix >>]()
* :video_camera: [SDR enablement training video]()  (WIP)

## SDR follow up process

### 🔔 About the inbound leads

* **What did they do?** They filled out a form on the [campaign page]()
* **What did they get from the form?** They will <watch our on-demand webcast OR view our eBook OR register for our live webcast OR etc.> "<Asset Name>" in a Pathfactory experience that provides like-minded <topic/use case> content.
* **Are they MQLs?** If filling out the form makes them reach the 90 point threshold, they will MQL.
* **What if they don't hit the threshold?** They will receive additional related assets via a [bi-weekly email nurture campaign in Marketo](link-to-nurture-issue). As they engage more, their score increases toward the 90 point threshold.

### :information_desk_person: Great... so where do I see these leads in SFDC?

In an effort to keep you focused on the prioritized lead and contact views, an **Interesting Moment** is triggered from Marketo.

**Interesting Moment to Look For: `<Name> Campaign - downloaded <asset type> "<Asset Name>"`**

#### Please prioritize lead AND contacts views per normal SLAs:
*  [My P2 - MQL excl SaaS trials](https://gitlab.my.salesforce.com/00Q?fcf=00B4M000004oR0o)
*  [My P4 - MQL - Trial SaaS](https://gitlab.my.salesforce.com/00Q?fcf=00B4M000004oR0o)
*  [My P7 - Inquiry](https://gitlab.my.salesforce.com/00Q?fcf=00B4M000004oR0o)
*  [My P8 - Stale MQLs or Nurture](https://gitlab.my.salesforce.com/00Q?fcf=00B4M000004Zcsp&rolodexIndex=-1&page=1) with new last interesting moment

*If they are in salesadmin / raw status / legacy AE owned, managers will monitor these leads for re-routing.*

### :large_blue_circle: MASTER|Inbound sequence for each region

* [NORAM](add-link-to-outreach-noram)
* [EMEA](add-link-to-outreach-noram)
* [APAC](add-link-to-outreach-noram)

**Note: These master sequences all have the same steps and copies, the only difference is the delivery schedules are aligned to each region's weekday business hours.* 

/label ~"Marketing Programs" ~"Strategic Marketing" ~"SDR" ~"mktg-status::wip"
```

Note: The Marketing Team owns content on marketing pages; do not change content or design of these pages without consulting the #dmpteam slack channel. Marketing will request help from product/UX when we need it, and work with them to ensure the timeline is reasonable.
</details>

## Sales Lead Funnel Cross-Over with All-Remote
{: #funnel-crossover-all-remote .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

#### Diagram of sales lead funnel cross-over with all-remote
{: #funnel-crossover-all-remote-diagram}
<!-- DO NOT CHANGE THIS ANCHOR -->

Below is a V1 visual in Mural of the sales lead funnel (pre-opportunity) on the left and the all-remote funnel on the right. Discussion of how and when to "bridge the gap" between the funnels - potentially sharing GitLab audience-targeted offers within All-Remote communications - is included [in this issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2590).

<div style="width: 600px;" class="embed-thumb"> <h1 style="position: relative;vertical-align: middle;display: inline-block; font-size: 24px; line-height:22px; color: #393939;margin-bottom: 10px; font-weight: 300;font-family: Proxima Nova, sans-serif;"> <img src="https://app.mural.co/avatar/jgragnola2053" style="position: absolute; border-radius: 50%; width: 36px;height: 36px;margin-right: 14px; display: inline-block; margin-top: -6px;margin-right: 10px; vertical-align: middle;"> <div style="padding-left:50px"> <span style="max-width:555px;display: inline-block;overflow: hidden; white-space: nowrap;text-overflow: ellipsis;line-height: 1; height: 25px; margin-top: -3px;">Lead Generation Funnel + All-Remote Breakout</span> <span style="position:relative;top:-3px;font-size: 16px; margin-top: -6px; line-height: 24px;color: #393939; font-weight: 300;"> by Jackie Gragnola</span> </div> </h1> <div style="position: relative; height: 0;overflow: hidden; height: 400px; max-width: 800px; min-width: 320px; border-width: 1px; border-style: solid; border-color: #d8d8d8;"> <div style="position: absolute;top: 0;left: 0;z-index: 10; width: 600px; height: 100%;background: url(https://murally.blob.core.windows.net/thumbnails/gitlab24743204/murals/gitlab24743204.1585283889844-5e7d833115ecf04b79301159.png?v=51ddc0c0-b456-443c-aa7c-92557e78efe9) no-repeat center center; background-size: cover;"> <div style="position: absolute;top: 0;left: 0;z-index: 20;width: 100%; height: 100%;background-color: white;-webkit-filter: opacity(.4);"> </div> <a href="https://app.mural.co/t/gitlab24743204/m/gitlab24743204/1585283889844/5837d15d7eed38c848745c9e63cef77eba194009" target="_blank" rel="noopener noreferrer" style="transform: translate(-50%, -50%);top: 50%;left: 50%; position: absolute; z-index: 30; border: none; display: block; height: 50px; background: transparent;"> <img src="https://app.mural.co/static/images/btn-enter-mural.svg" alt="ENTER THE MURAL" width="233" height="50"> </a> </div> </div> <p style="margin-top: 10px;margin-bottom: 60px;line-height: 24px; font-size: 16px;font-family: Proxima Nova, sans-serif;font-weight: 400; color: #888888;"> You will be able to edit this mural. </p></div>
