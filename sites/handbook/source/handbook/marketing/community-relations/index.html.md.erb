---
layout: handbook-page-toc
title: "Community Relations"
+description: The Community Relations team supports GitLab's mission by working with our community to ensure they receive support and recognition for contributing to GitLab.
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## <i class="far fa-newspaper" id="biz-tech-icons"></i>  Our mission

At GitLab, our mission is to change all creative work from read-only to read-write so that [everyone can contribute](/company/mission/#mission).

The Community Relations team supports this mission by working with our community to ensure they receive support and recognition for contributing to GitLab. Contributing to GitLab can include blog posts, code, documentation, discussions on forums/social media, meetups, presentations, translations, UX design, and more. We also support educational institutions and open source projects to host their work on GitLab.

## <i class="fas fa-users" id="biz-tech-icons"></i> Meet the Team

<%= department_team(base_department: 'Community Relations') %>

## <i class="far fa-paper-plane" id="biz-tech-icons"></i> How to reach us

### Chat with us
- On the internal Slack instance: by visiting the [`#community-relations`](https://gitlab.slack.com/messages/community-relations) Slack channel, or by tagging the `@community-team`
- [On the public Gitter room](https://gitter.im/gitlabhq/contributors)

### Email us
- Please use the [e-mail address specific to each program](/handbook/marketing/community-relations/program-resources/#contact-e-mails)

### File an issue
- File an issue to work with us: [See our project management section](/handbook/marketing/community-relations/project-management/) to find program-specific workboards where you can file an issue

## <i class="fas fa-tasks" id="biz-tech-icons"></i> How we work

### Community Team resources
- [Community Relations project management](/handbook/marketing/community-relations/project-management/)
- [Common program resources](/handbook/marketing/community-relations/program-resources/)

### Our handbooks
- [Community Operations](/handbook/marketing/community-relations/community-operations/)
- [Code Contributor Program](/handbook/marketing/community-relations/code-contributor-program/)
- [Evangelist Program](/handbook/marketing/community-relations/evangelist-program/)
- [Open Source Program](/handbook/marketing/community-relations/opensource-program/)
- [Education Program](/handbook/marketing/community-relations/education-program/)
     - [Education Evangelism](/handbook/marketing/community-relations/education-program/education-evangelism/)
- [Startups Program](/handbook/marketing/community-relations/startups-program/)
- [Developer Evangelism](/handbook/marketing/community-relations/developer-evangelism/)
- [Education Evangelism](/handbook/marketing/community-relations/education-program/education-evangelism/)

### Our workflows

- [Community Operations response workflows](/handbook/marketing/community-relations/community-operations/workflows/) including Code of Conduct violations.
- [Developer Evangelism Community Response Process](/handbook/marketing/community-relations/developer-evangelism/community-response/)

#### Group Conversation
Every six weeks, we run a [Group conversation](/handbook/group-conversations/) for the benefit of other team members. 
When posting to the [What's happening at GitLab](https://gitlab.slack.com/archives/C0259241C) Slack to announce a Group Conversation, use the following format to create an engaging and entertaining post. 
```
[Intro with emoji]
Please join me on [Day, 202X-XX-XX, time] for the :community: community Relations Group Conversation! 

:flashlight: We plan on highlighting:
- [list of highlights]

:google_slides: Check out the [slides link] to learn what we've been up to. 
:google_docs: Please add questions to the [ agenda doc link] ahead of time so we can jump right in. :ninja_turtle_jumping_yay:

[outro with call to action to join and some more emojis]
See you then! 
```

### Our guidelines
- TBD

### Our calendars

We use team-wide calendars for collective notification and to manage team logistics and events. Specific teams within Community Relations, might have their own calendars, such as the [Developer Evangelism calendar](/handbook/marketing/community-relations/developer-evangelism/#-team-calendar).

- [Community Relations Team](https://calendar.google.com/calendar/u/0?cid=Y190M2JhY2k5MnFoYnJmYTMyMDdpZHZwdnYwMEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t)
- [Community Events](https://calendar.google.com/calendar/b/1?cid=Z2l0bGFiLmNvbV85MHQ1dWUxcThrYmpvcTViMHI5MW51N3J2Y0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t)


### Community Relations OKRs

Every quarter, we work on [team Objectives and Key Results (OKRs)](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=OKR) that align with company OKRs.

OKRs we seek to align with: 
 * [CEO OKRs](/company/okrs/#most-recent-okrs)
 * [Marketing OKRs](/handbook/marketing/#most-recent-okrs)
 * [Corporate Marketing OKRs](/handbook/marketing/corporate-marketing/#most-recent-okrs)

#### How we update our OKRs

To update our [list of current OKRs](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=OKR): 
 1. **New Epic.** Create a new epic in the [`Community Relations` project](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics). The naming convention we use for OKRs is: `FY-Year-Quarter OKR: Description of OKR`
 1. **Program label.** Tag the new epic with a program-specific label (e.g. `open source program` `education program` etc).
 1. **OKR label.** Add the red `OKR` label.
 
**Issue Health:** We use issue health indicators to help people understand an OKRs status at a glance. These status indicators are: 
 * `on track` 
 * `needs attention` 
 * `at risk`

### Community Relations KPIs
The Community Relations team has several [Key Performance Indicators and related Performance Indicators](/handbook/marketing/community-relations/performance-indicators/).

<%= kpi_list_by_org("Community Relations Department") %>

## <i class="fas fa-receipt" id="biz-tech-icons"></i> Team Budgets
Learn more on the [Community Operations budget page](/handbook/marketing/community-relations/community-operations/#-team-budgets)

## <i class="fas fa-globe"></i> Community Diversity, Inclusion and Belonging

In alignment with [GitLab's core value of Diversity, Inclusion, and Belonging (DIB)](/handbook/values/#diversity-inclusion), the Community Relations team seeks to purposefully design DIB into every facet of its programs and operations. We seek to foster DIB at GitLab and within the wider GitLab community.

This section is meant to document tips and best practices that the Community Relations team, and GitLab team, should keep in mind as they plan events and activities.

### Event organization best practices

 * **Tools:** We purposefully think about the tools we are using to engage our audience. For example, some people in the open source world may not be able to use tools that are proprietary software. Also, some people in certain countries may not be able to use certain tools because of country restrictions. Sending a survey beforehand with different tool options could help identify good tools to use for a specific event.
 * **Time zones:** We try to be as inclusive of as many different time zones as possible. It is recommended to have 4 hour segments to spread out participation. Possible times: 4 pm UTC - 8 pm UTC for EMEA and NORAM, and 2 am UTC - 6 am UTC for APAC and India.
 * **Code of Conduct:** We make our [Code of Conduct](/community/contribute/code-of-conduct/) a requirement at all GitLab events and actively help our community with friction and conflict.
 * **Accessibility:** We advocate for accessibility in all of our events and materials.
 * **Captioning and Interpretation:** Having both captioners and interpreters for online events is important. There are differences in American English and American Sign Language, for example, so captioning alone is not enough. Having a dial-in relay service would work as a "hack" to captions/interpreter, in the worst case scenario. This depends on if the blind person has a video phone/captioned phone to access these services.
 * **Share slides beforehand:** 1 week before your online event -- share the slides with attendees so that people who use screen readers can follow along.
 * **Images should have alt text:** This is for screen readers for people who are blind or are visually imparied. Be descriptive with the image. Maybe a few sentences about what the most important content is.
 * **Use proper headings in your editor:** Use your editor's headings so that screen readers understand the organization of the page.
 * **Introduce yourself descriptively when you give a talk:** In talk intros, briefly describe what you look like so that people who are deaf can create a mental image of what you look like and associate that image with your voice.
 * **Schedule breaks with the 30:5 rule:** Make sure to have a 5 min break after each 30 min section.
 * **Describe infographics with audio:** Describe the most important parts of infographics to help with accessibility.
 * **Hybrid events:** Keep having a virtual even option even when in-person begins again!


### Other ways we foster diversity, inclusion, and belonging
 * **Images:** We seek to promote the use of images that represent a diverse group of users, customers, and community members. When we see a lack of diverse representation, we speak up and actively help update those images when possible.
 * **Speakers:** As event organizers and participants, we seek to include a diverse set of speakers in events that GitLab organizes or touches.
 * **We are open-minded:** We actively seek feedback and keep an open mind about our current policies. We are open to change and are willing to make structural changes to ensure that we continue to foster DIB among our team and the wider GitLab community.
 * **We retain a growth-mindset and keep learning:** We read articple, attend workshops, and participate in trainings that help educate us about how to foster DIB in our community and how to be inclusive ourselves.
 * **We encourage a diverse set of community members to participate in research:** We recognize the product inclusivity is important and that we need to build with our community, not just for them. We encourage a divers set of community members to participate in our [First Look product research program](/community/gitlab-first-look/).
 * **We promote the [GitLab Diversity Scholarship program](/community/sponsorship/):** This scholarship supports Diversity, Inclusion, and Belonging-focused events financially.

## <i class="fas fa-lightbulb"></i> Communites who inspire us 

We take inspiration from the great work being done by other communities. Some of the communities who we take inspiration from:  
- Linux Foundation 
- CNCF 
- FINOS 
- Debian
- GNOME
- KDE
- Fedora
- Drupal
- Wikimedia Foundation
- Kubernetes
- AWS 
- Dev.to
- Hacker News 
- Google Summer of Code 
- Outreachy
- Grace Hopper Community / Systers
- Lesbians Who Tech
- Techqueria
- Latinas in Tech
- Women in Tech
- Women Who Code
- Rails Girls

## <i class="fas fa-book"></i> Community Learning Pathway
The [Community Learning Pathway](/handbook/marketing/community-relations/community-pathway) is a course built to educate the community on how the Community works, the different community programs and how to contribute the GitLab. Members of the Community and GitLab team members who complete the course will earn a badge.