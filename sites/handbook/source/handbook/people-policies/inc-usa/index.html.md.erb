---
layout: handbook-page-toc
title: People Policies - GitLab Inc (USA)
description: "GitLab's USA-specific People Policies list the mandatory US people-related policies for our US-based team members."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Equal Employment Opportunity Policy
GitLab, Inc. is an equal opportunity employer. In accordance with anti-discrimination laws, it is the purpose of this policy to effectuate these principles and mandates. GitLab prohibits discrimination and harassment of any type and affords equal employment opportunities to team members and applicants without regard to race, color, religion, sex, sexual orientation, gender identity or expression, marital status, pregnancy, age, national origin, disability status, genetic information, protected veteran status, or any other characteristic protected by law. GitLab conforms to the spirit as well as to the letter of all applicable laws and regulations.

### Scope
The policy of equal employment opportunity (EEO) and anti-discrimination applies to all aspects of the relationship between GitLab and its team members, including: Recruitment, Employment, Promotion, Transfer, Training, Working conditions, Wages and salary administration, and Team member benefits and application of policies.

The policies and principles of EEO also apply to the selection and treatment of independent contractors, personnel working on our premises who are employed by temporary agencies and any other persons or firms doing business for or with GitLab.

### Dissemination and Implementation of Policy
The officers of GitLab, Inc. will be responsible for the dissemination of this policy. Directors, managers and supervisors are responsible for implementing equal employment practices within each department. The People Group is responsible for overall compliance and will maintain personnel records in compliance with applicable laws and regulations.

### Procedures
GitLab administers our EEO policy fairly and consistently by:

- Posting all required notices regarding employee rights under EEO laws in areas highly visible to team members.
- Advertising for job openings with the statement "We are an equal opportunity employer and all qualified applicants will receive consideration for employment without regard to race, color, religion, sex, sexual orientation, gender identity or expression, pregnancy, age, national origin, disability status, genetic information, protected veteran status, or any other characteristic protected by law."
- Posting all required job openings with the appropriate state agencies.
- Forbidding retaliation against any individual who files a charge of discrimination, opposes a practice believed to be unlawful discrimination, reports harassment, or assists, testifies or participates in an EEO agency proceeding.
- Requiring team members to report to a member of management, an HR representative or the Director of Legal - Employment any apparent discrimination or harassment. The report should be made within 48 hours of the incident.
- Promptly notifying the Director of Legal - Employment of all incidents or reports of discrimination or harassment and takes other appropriate measures to resolve the situation.

### Harassment
Please refer to the [GitLab Anti-Harassment Policy](https://about.gitlab.com/handbook/anti-harassment/) for more information.

### Remedies
Violations of this policy, regardless of whether an actual law has been violated, will not be tolerated. GitLab will promptly, thoroughly and fairly investigate every issue that is brought to its attention in this area and will take disciplinary action, when appropriate, up to and including termination of employment.

## Individuals with Disabilities Policy
GitLab, Inc. (“The Company”) complies with the Americans with Disabilities Act (ADA) and all applicable state laws, which make it unlawful to discriminate in employment against a qualified individual with a disability. The Company prohibits discrimination against team members and applicants with disabilities in all aspects of employment. Our company's commitment to this policy includes making reasonable accommodations to otherwise qualified persons with disabilities to enable them to perform the essential functions of their jobs, unless doing so would pose an undue hardship on our business, would pose a direct threat of substantial harm to the team member or others or is otherwise not required by applicable law. 

### Our Commitment
A team member or applicant in need of a reasonable accommodation should make the Company aware of their request by notifying the [Team Member Relations](/handbook/people-group/team-member-relations) team at `teammemberrelations@gitlab.com`. The Company will work with each individual to define their job-related or application-related needs and to try to accommodate those needs. 

### Qualified Individuals with Disabilities
Qualified individuals with disabilities are defined as individuals with disabilities who can perform the essential functions of the job in question with or without reasonable accommodation. The term disability is defined by applicable law.

Per the Americans with Disabilities Act (ADA), a disability means, with respect to an individual: a physical or mental impairment that substantially limits one or more of the major life activities of such individual; a record of such an impairment; or being regarded as having such an impairment as described in Section 36.105, paragraph (f) of the ADA. So to clarify, you may have been or currently be diagnosed by a disability as per the ADA and not feel that it substantially limits your life activites and still self-identify as having a disability status.

Please find an overarching list of what can fall into the disability defition per the ADA, including:

-Developmental disabilities, for example,  cerebral palsy or autism spectrum disorder
-Traumatic brain injuries
-Deafness or serious difficulty hearing, benefiting from, for example, American Sign Language
-Blindness or serious difficulty seeing even when wearing glasses
-Missing extremities (arm, leg, hand and/or foot)
-Significant mobility impairments, benefitting from the utilization of a wheelchair, scooter, walker, leg brace(s) and/or other supports
-Partial or complete paralysis (any cause)
-Epilepsy and other seizure disorders
-Intellectual disabilities (formerly described as mental retardation)
-Significant psychiatric disorders, for example, bipolar disorder, schizophrenia, PTSD, or major depression
-Dwarfism
-Significant disfigurement, for example, disfigurements caused by burns, wounds, accidents, or congenital disorders

And not all disabilities are obvious to the eye. These are known as invisible disabilities. An invisible disability is a physical, mental, or neurological condition that can’t be seen from the outside. But it can impact someone’s movements, senses, or activities, according to the Invisible Disabilities Association. Some examples of invisible disabilities include autism spectrum disorder, depression, diabetes, and learning and thinking differences such as ADHD and dyslexia. Invisible disabilities can also include symptoms such as chronic pain, fatigue, and dizziness.

GitLab values all team members for their strengths. We offer team members with disabilities — whether visible or invisible — an equal opportunity to succeed, to learn, to be compensated fairly, and to advance. True inclusion is about embracing differences. At GitLab, we are proud to make reasonable accommodations to the known disability of a team member. 

### Pregnancy Accommodation
Pregnancy accommodation is governed by the Pregnancy Discrimination Act, the Americans with Disabilities Act, the Family and Medical Leave Act, and numerous state and local laws. If you need a temporary change to how, when, or where you work due to pregnancy or related
conditions, you may request an accommodation under this policy. A team member in need of a reasonable accommodation should make GitLab aware of their request by notifying the [Team Member Relations](/handbook/people-group/team-member-relations) team at `teammemberrelations@gitlab.com`.

### Reasonable Accommodation
A reasonable accommodation is any change or adjustment to a job, the work environment or the way things usually are done that enables a qualified individual with a disability to perform the essential functions of the job and that does not pose an undue hardship for the Company or create a direct threat to health or safety. When requesting an accommodation, team members are required to notify the People Operations Team of the need for the accommodation. The Company may ask for medical documentation supporting the need for an accommodation and all supporting documentation should be returned as quickly as possible to prevent a delay in the accommodation process. Requests for a reasonable accommodation for a medical condition and any supporting documentation, will be treated as confidential, maintained in a file separate from an employee’s other personnel documents and disclosed only as permitted by applicable law.

### Determining Appropriate Accommodations
Frequently, when a qualified individual with a disability requests a reasonable accommodation, the appropriate accommodation is easily agreed upon. The individual may recommend an accommodation based on their life or work experience. The ultimate decision as to whether a particular accommodation will be made rests with the Company. When the appropriate accommodation is not obvious, the Company may assist the individual in identifying one. If more than one accommodation will enable the individual to perform the job, the Company reserves the right to choose which accommodation it will make. If you feel that you have been unreasonably denied an accommodation request, please speak with the [Team Member Relations](/handbook/people-group/team-member-relations) team by emailing `teammemberrelations@gitlab.com`. If you have any questions concerning this policy you should speak with the People Operations team.

## US Access to Personnel Records

Active team members have the right to request a copy of their personnel file every 6 months or 2 times per year. Former team members may request their files once each year after separation for as long as the personnel record is maintained. Upon receipt of the request to `peopleop@gitlab.com`, GitLab must send the complete digital copy of the personnel file to the current or former team member within 7 business days.

### Personnel File Definition

A personnel file is defined as the personnel records of an employee, in the manner maintained by the employer and using reasonable efforts by the employer to collect, that are used or have been used to determine the employee's qualifications for employment, promotion, additional compensation, or employment termination or other disciplinary action. All personnel records must include any records indicating their exposure to potentially toxic materials or harmful physical agents.

The disclosure of the following is not required:

- Documents or records required to be placed or maintained in a separate file from the regular personnel file by federal or state law or rule such as Medical reports;
- Documents or records pertaining to confidential reports from previous employers of the employee;
- An active criminal investigation, or investigation of possible criminal offenses;
- Legally privileged information;
- An active disciplinary investigation by the employer;
- An active investigation by a regulatory agency, including a government security investigation;
- Any information in a document or record that identifies any person who made a confidential accusation or grievance, as determined by the employer, against the employee who makes a request for his or her personnel file under the law;
- Letters of Employment reference;
- Portions of test documents;
- Information relating to convictions, or arrests;
- Credit information;
- Any pending claim between the employer and the employee;
- Materials used by the employer for staff management planning or business operations;
- Materials of a personal nature about a person other than the employee, if such access would be an unwarranted invasion of privacy, or any records regarding another employee.

## Military Leave Policy
GitLab, Inc. recognizes the obligation of those team members serving in any branch of the military or other uniformed services of the United States. Employment status within GitLab is protected by the Uniformed Services Employment and Reemployment Rights Act of 1994 (“USERRA”).

### Leave and Re-Employment
Team members who serve on active or reserve duty will be granted a leave of absence up to the maximum time required by law. GitLab is committed to preserving the job rights of team members absent on military leave in accordance with law.

### Compensation
Military leave runs concurrently with GitLab PTO. Team members on unpaid military leave will be paid at 100% of their salary for the first 25 days of leave. Exempt team members will not incur any reduction in pay for partial week absences for leave under this policy.

### Health Care Continuation
During a military leave of less than 31 days, a team member is entitled to continued group health plan coverage under the same conditions as if the employee had continued to work. For military leaves of more than 30 days, an employee may elect to continue their health coverage in accordance with USERRA and COBRA. For additional information on health care continuation contact Total Rewards `total-rewards@gitlab.com`.

### Notification of Supervisor/Manager
Team members are expected to inform their supervisor/manager of their need for military leave as far in advance as possible. Team members must also record their leave in PTO by Roots and submit a copy of the military orders to Total Rewards.

### No Retaliation
Team members who request military leave will not be retaliated against or penalized in any manner. Any team member who believes they have been retaliated against in violation of this policy should notify Team Member Relations `us_teammemberrelations@gitlab.com` immediately.

### Other Leaves
This leave may run concurrently with any available Family and Medical Leave, where applicable.

### Additional Information
Additional leave may be required under state law and can be found in the state specific Handbook Addendum, if applicable.

## Pay Transparency Non-Discrimination Provision
GitLab will not discharge or in any other manner discriminate against team members or applicants because they have inquired about, discussed, or disclosed their own pay or the pay of another team member or applicant. However, team members who have access to the compensation information of other team members or applicants as a part of their essential job functions cannot disclose the pay of other team members or applicants to individuals who do not otherwise have access to compensation information, unless the disclosure is (a) in response to a formal complaint or charge, (b) in furtherance of an investigation, proceeding, hearing, or action, including an investigation conducted by the employer, or (c) consistent with the GitLab’s legal duty to furnish information. 41 CFR 60-1.35(c) If you believe that you have experienced discrimination contact OFCCP.

**OFCCP Makes it Safe for People to Ask About, Discuss, and Disclose Their Pay**

**1. What is employment discrimination based on inquiring about, discussing or disclosing my pay or that of other team members?**

This type of discrimination generally exists where an employer takes an adverse employment action against team members or job applicants because they inquired about, discussed, or disclosed their own compensation or the compensation of other team members or applicants. One example is an employer firing a team member because they discussed their salary with another team member. Another example is an employer decreasing a team member’s work hours because they asked their coworkers about their rates of overtime pay. 

**2. What are my rights?**

Under Executive Order 11246, you have the right to inquire about, discuss, or disclose your own pay or that of other team members or applicants. You cannot be disciplined, harassed, demoted, terminated, denied employment, or otherwise discriminated against because you exercised this right. However, this right is subject to certain limited exceptions. 

**3. Are contractors prohibited from having formal and informal pay secrecy policies?**

Yes. Federal contractors are generally prohibited from having policies that prohibit or tend to restrict team members or job applicants from discussing or disclosing their pay or the pay of others. For example, a government contractor’s policy that prohibits team members from talking to each other about end of-the-year bonuses would be considered a violation, as it prohibits team members from discussing their compensation.

**4. What is considered pay?**

Pay generally refers to any payments made to a team member, or on behalf of a team member, or offered to an applicant. This includes but is not limited to salary, wages, overtime pay, shift differentials, bonuses, commissions, vacation and holiday pay, allowances, insurance and other benefits, stock options and awards, profit sharing, and retirement. 

**5. Does the protection include employer defenses or exceptions?**

Yes. The Executive Order provides federal contractors with two ways to justify or defend actions taken that might otherwise be seen as discriminatory and prohibited: the “essential job functions” defense, and the general, or “workplace rule” defense. 

**6. What are “essential job functions” under the Executive Order?** 

The term “essential job functions” means the fundamental job duties of the employment position an individual holds. A job function may be considered essential if: 

- The access to the amount of pay provided to team members or offered to applicants, salary structures and market studies related pay and policies related to setting or changing employee pay are necessary in order to perform that function or another routinely assigned business task; or 
- The function or duties of the position include protecting and maintaining the privacy of team member personnel records, including amounts and types of pay provided to team members, salary structures, market studies related to pay, and policies related to setting or changing a team member’s pay. 

**7. What is the “essential job functions” defense?**

Under the “essential job functions” defense, a federal contractor can defend against a claim of discrimination by showing that it took adverse action against a team member because they (a) had access to the compensation information of other team members or applicants as part of their essential job duties and ( b) disclosed that compensation information to individuals who did not otherwise have access to it. However, even team members who have access to compensation information as part of their essential job functions may discuss, disclose, or inquire about compensation in some instances. For example, they can: 

- Discuss or disclose the pay of applicants or team members in response to a formal complaint or charge; as a part of an investigation, proceeding, hearing, or action, including an investigation conducted by the employer; or in accordance with the contractor’s legal duty to furnish information. 
- Discuss their own pay with other team members. 
- Discuss possible pay disparities involving other team members with a contractor’s management official or while using the contractor’s internal complaint process. 
- Discuss or disclose amount or types of pay of other applicants or team members based on information received through means other than access granted through their essential job functions. 

**8. What is the “workplace rule” defense?**

Under the “workplace rule” defense, a federal contractor can defend against a claim of discrimination by showing that it took adverse action against an team member for violating a consistently and uniformly applied company rule. The rule must not prohibit, or tend to prohibit, team members or applicants from discussing or disclosing their compensation or that of a co-worker or job applicant. Examples of “workplace rules” may include rules on the use of leave and the length of breaks. 

**9. Does my employer have to tell me what other team members are being paid?**

No. Executive Order 11246 does not require employers to provide team members or job applicants with information on the pay of other team members or applicants. 

**10. Who does OFCCP protect?**

OFCCP protects the rights of team members and job applicants of companies doing business with the Federal Government. This includes team members at banks, information technology frms, meat packing plants, retail stores, manufacturing plants, accounting firms, and construction companies, among others. 

Filing a Complaint 

**11. What can I do if I believe my employer discriminated against me because I asked about, discussed, or disclosed my pay or the pay of another team member or applicant?** 

You can file a complaint with OFCCP. You do not need to know with certainty that your employer is a federal contractor or subcontractor to file a complaint. 

**12. How do I file a complaint with OFCCP?**

You may fle a discrimination complaint by: 

- Completing and submitting a form online through OFCCP’s Web site; 
- Completing a form in person at an OFCCP ofce; or 
- Mailing, e-mailing or faxing a completed form to the OFCCP regional ofce that covers the location where the alleged discrimination occurred. 

The form is available online [here](http://www.dol.gov/ofccp/regs/compliance/pdf/pdfstart.htm) and in paper format at all OFCCP offices. To find the office nearest you, visit the online listing of [OFCCP offices](http://www.dol.gov/ofccp/contacts/ofnation2.htm). 

You must remember to sign your completed complaint form. If you fail to do so, OFCCP will still take your complaint but an OFCCP investigator will ask you to sign the form during a follow-up interview. Complaints alleging discrimination for discussing, disclosing, or inquiring about pay must be fled within 180 days from the date of the alleged discrimination, unless the time for fling is extended for good cause. The same 180-day time frame applies to complaints alleging discrimination based on race, color, religion, sex, sexual orientation, gender identity, or national origin. 

**13. Can my employer fire, demote, or treat me less favorably because I filed a complaint?** 

No. It is illegal for your employer to retaliate against you for filing a complaint or participating in an investigation. OFCCP’s regulations protect you from harassment, intimidation, threats, coercion, or retaliation for asserting your rights. 

**14. Can I file a complaint with OFCCP and the Equal Employment Opportunity Commission (EEOC)?**

Yes, if you fle with both OFCCP and EEOC, your complaint will be investigated by the appropriate agency. In some instances, OFCCP and EEOC may decide to work together to investigate your complaint. OFCCP generally keeps complaints filed against federal contractors that allege discrimination based on discussing, disclosing, or inquiring about pay. OFCCP also generally keeps complaints fled against federal contractors where there appears to be a pattern of discrimination that afects a group of team members or applicants, and those that allege discrimination based on a person’s sexual orientation or gender identity, disability, or protected veteran status. 

**15. What will happen if there is a fnding that I was a victim of employment discrimination?**

You may be entitled to a remedy that places you in the position you would have been in if the discrimination had never happened. You may be entitled to be hired, promoted, reinstated, or reassigned. You may also be entitled to receive back pay, front pay, a pay raise, or some combination of these remedies. In addition, if OFCCP finds that the federal contractor or subcontractor discriminated, OFCCP could seek to have the company debarred or removed from consideration for future federal contracts or have the company’s current contracts or contract modifications cancelled. 

For more information: The U.S. Department of Labor Office of Federal Contract Compliance Programs, 200 Constitution Avenue, NW Washington, D.C. 20210. 1-800-397-6251 TTY: 1-877-889-5627 [www.dol.gov/ofccp](https://www.dol.gov/agencies/ofccp)

## Infectious Disease Policy
It is the goal of GitLab, Inc. during a period of an infectious disease outbreak or pandemic to maintain essential functions and services and provide a safe and healthy work environment for team members, customers/clients, vendors and the public. GitLab is committed to establishing methods for monitoring the severity and duration of an outbreak or pandemic, implementing measures to minimize exposure in the workplace and sustaining essential functions until the organization can resume normal operations.

### Communication
People Operations will oversee the implementation of this policy and coordinate communications from management to team members and other stakeholders. Duties include:

- Monitoring and coordinating events and communications around an infectious disease outbreak or pandemic; and
- Creating work rules that could be implemented to promote safety through infection control.

Oversight includes the maintenance of a current list of contacts including:

- Government agencies;
- Emergency response and healthcare facilities and services; and
- Equipment suppliers and service contractors who can or have agreed to assist during and after an outbreak or pandemic.

### Safety and Health Measures
GitLab will implement disease mitigation and protective measures for team members working onsite and offsite and for interactions with customers/clients, vendors and the public during the outbreak or pandemic. GitLab is committed to providing the most current and credible information about the disease, including the way it spreads, symptoms and measures to prevent its transmission.

#### Health Monitoring
All team members will be notified on how to self-monitor for symptoms and report to their supervisor/manager when they are ill or experience infectious disease symptoms. 

#### Rest When Sick
Team members are urged not to report to work when they are feeling ill or are experiencing symptoms of an infectious disease. A team member who appears to exhibit infectious disease symptoms when reporting to work or who becomes sick during their time at work will be strongly encouraged to log off from work.

If a team member is confirmed to have contracted an infectious disease, they may be eligible for additional leave after utilizing the Paid Time Off Policy. In the event that the sick leave provided under your local law is more generous than GitLab’s policy, then your local law would supersede this policy.

#### Business Travel Restrictions
GitLab will evaluate the risk of team member exposure to the infectious disease from business travel, and may restrict, cancel or ban business travel as necessary to minimize or prevent risk of infection. In making such determinations, consideration will be given to any travel bans or advisories issued by government agencies, including the U.S. Department of State and the CDC.

#### Non-Essential Activities
During an infectious disease outbreak or pandemic GitLab may postpone or cancel all nonessential activities, including meetings, gatherings and training sessions. Affected team members would be notified as soon as practicable.

### Attendance and Leave
GitLab’s attendance policies will remain in place during an infectious disease outbreak or pandemic, unless otherwise notified. If a team member has a challenge (e.g., childcare issues in the event of a school closure), they should speak to their supervisor/manager to determine an alternative plan. Team members will be notified of any work schedule changes caused by an infectious disease outbreak or pandemic. Requests to adjust individual work schedules will be addressed on a case-by-case basis.

If a team member is out of work because of exposure to an infectious disease, or other illness or condition recognized by federal, state or local law, the team member may be required to submit additional information for the absence. To the extent permissible by law, GitLab may modify its leave policies to reflect conditions during a declared infectious disease outbreak or pandemic.

### Compensation
Team members will be paid for all hours worked during an infectious outbreak or pandemic. Team members will be notified of any changes in pay rates for non-exempt team member hourly rate of pay or exempt employee salary as a result of long-term business needs caused by significant business disruption or economic shutdown due to an infectious disease outbreak or pandemic.

### Furloughs/Layoffs and Closings
In the event of a temporary or permanent closing due to unforeseen business circumstances related to the infectious disease outbreak or pandemic, team members will be notified as soon as practicable concerning a furlough, layoff or business closing, including an explanation as to why notice was not provided if a furlough/layoff is implemented without advance notice. Team members subject to a furlough/layoff under this policy will be notified about available benefits and where to obtain additional information and guidance.

### On-Site Work Prohibited
GitLab reserves the right to prohibit an employee or another individual with a confirmed positive test for an infectious disease or who is displaying symptoms (even without a confirmed positive) or has been in close contact with someone with a confirmed or presumed positive test for an infectious disease from entering on-site facilities, programs and functions if a determination is made that the entry introduces a recognized hazard to the workplace and the restriction protects the safety and health of team members, customers/clients and others. 

### Confidentiality
Infectious disease-related diagnostic information about team members will be treated as confidential, privileged information. All information about a team member's illness will be treated as a confidential medical record in compliance with federal or state law. GitLab will adhere to all federal, state and local public health reporting requirements.

### No Retaliation
Team members who raise a concern or make a complaint regarding any aspect of this policy in good faith will not be retaliated against or penalized in any manner. Any team member who believes they have been retaliated against in violation of this policy should notify Team Member Relations `us_teammemberrelations@gitlab.com` immediately.

### Additional Information
Team members may contact People Operations `peopleops@gitlab.com` with questions regarding this policy.

## Fair Credit Reporting Act (FCRA) and Related State Law Compliance

**Step 1: Disclosure and Authorization**

The applicant must give the employer consent to have a third party service conduct a background check. The Disclosure and Authorization form can be presented to the applicant at the time they complete the employment application form. The form should grant the employer permission to conduct an initial background check (and, subject to state law, subsequent background checks if the applicant is hired) utilizing a third party service. Also, a “Summary Of Your Rights Under The Fair Credit Reporting Act” should be enclosed with the consent and disclosure form. For New York applicants, a copy of Article 23-A of the Correctional Law also should be enclosed and any other relevant state summary of rights.

The background investigation cannot be lawfully conducted without a signed Disclosure and Authorization form. Applicants can be advised that they will not be considered for employment without submitting the signed form. Equally for current team members, they can be advised that their employment may be impacted if they do not consent to the background check.

**Step 2: Pre-Adverse Action: Notify the Applicant of Negative Report BEFORE Adverse Action is taken**

If the consumer reporting agency reports information which may be used, in whole or in part, as a basis for an adverse employment action (e.g., rescinding a conditional offer of employment), the applicant must receive notification before a final decision is made to deny employment. As a result, the employer must provide a copy of the consumer report, a pre-adverse action letter, and another copy of the FCRA notice of rights (and for New York applicants, the Article 23-A notice). The applicant shall also receive any applicable state rights as required.

If the disqualification decision is not based on a misrepresentation or omission in the employment application, it is a best practice to discuss the potentially disqualifying information with the individual prior to issuing the pre-adverse action notice. This practice supports the individual job-related nature of any disqualification decision.

**Step 3:	Wait for a Reasonable Period of Time to Find Out What, if Any, Explanation is Offered by the Applicant**

If the applicant does not respond at all to the notification within a reasonable period of time (5 days), the employer may proceed with its decision to rescind the conditional offer. If the applicant responds, the employer should carefully consider the information submitted and then make a decision. If the explanation is reasonable under the circumstances, then it may still be possible to go forward with the new hire (for example, a case of mistaken identity). However, if the applicant's explanation is determined to be insufficient, then the employer should proceed to the next step.

**Step 4:	Notify Applicant of Adverse Action**

The employer must provide the applicant with written notice of the adverse action and the name, address, and telephone number of the consumer reporting agency.
The Adverse Action Notice form should be sent along with the federal summary of rights and any applicable state summary of rights. The notice includes a statutorily required statement that the consumer reporting agency did not make the decision and does not know why the decision was made should be included as well as a notice of the applicant's right to obtain the report and dispute the information.

**Step 5: Maintain Documentation**

For all adverse decisions, document each step taken. Keep copies of all consent and disclosure forms and other documentation sent to the applicant in the event the company has to defend its decision at some later point.

### Record Retention
All documents related to the background check process must be retained for the minimum period of time required by applicable law.

### Financial Checks
Finance team members **only** will be required to participate in a federal check through Sterling, which searches for any tax-related or financial offenses. See the [Initiating a Background Check through Greenhouse](/handbook/hiring/talent-acquisition-framework/coordinator/#initiating-a-background-check-through-greenhouse) section for process details.
