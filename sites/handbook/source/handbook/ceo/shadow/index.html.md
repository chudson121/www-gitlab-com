---
layout: handbook-page-toc
title: CEO Shadow Program
description: "At GitLab, being a CEO shadow is not a job title, but a temporary assignment to shadow the CEO"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

### Introduction

At GitLab, being a CEO shadow is not a job title, but a temporary assignment to shadow the CEO.
The shadows will be present at [all meetings of the CEO](/handbook/ceo/#ceo-meeting-cadence) during their rotation.
GitLab is an [all-remote](/company/culture/all-remote/) company, but the CEO has in-person meetings with external organizations.
Unless you're joining the program during one of our [remote rotations](#remote-shadow-rotations), you will stay in San Francisco during the entire [rotation](#rotation-rhythm) and travel with the CEO.

### Goal

The goal of the CEO Shadow Program is to give team members and [eligible individuals](/handbook/ceo/shadow/#eligibility) an overview of all aspects of the [company](/company/). This transparency enables CEO Shadow participants to better engage and collaborate cross-functionally, as well as better perform [global optimizations](/handbook/values/#global-optimization).

As a CEO Shadow, you'll gain this context through the [meetings you attend](#meetings--events) and while [completing](/handbook/values/#bias-for-action) short-term [tasks](#tasks) from across the company.
The program also creates opportunities for the CEO to build relationships with team members across the company and to identify challenges and opportunities earlier.
The shadows will also often connect with one another, developing new cross-functional relationships.

### What it is not

The CEO Shadow Program is not a performance evaluation or the next step to a promotion. Being a CEO shadow is not needed to get a promotion or a raise, and should not be a consideration factor for a promotion or raise, as diverse applicants have different eligibilities.

### Benefits for the company

Apart from creating leadership opportunities, the CEO Shadow Program:

1. Leaves a great impression on both investors and customers
1. Gives feedback immediately to the CEO
1. Enables the CEO to create immediate change

This is why the program is worth the extra overhead for the CEO and [EBA team](/handbook/eba/).

### Naming of the program

For now, this role is called a [CEO shadow](https://feld.com/archives/2015/03/ceo-shadowing.html) to make it clear to external people why a shadow is in a meeting.

Other names considered:

1. Technical assistant: This title could be mixed up with the [executive assistant](/job-families/people-ops/executive-business-administrator/) role. ["In 2003, Mr. Bezos picked Mr. Jassy to be his technical assistant, a role that entailed shadowing the Amazon CEO in all of his weekly meetings and acting as a kind of chief of staff."](https://www.theinformation.com/articles/amazons-cloud-king-inside-the-world-of-andy-jassy).
1. Chief of Staff to the CEO: This commonly is the ["coordinator of the supporting staff"](https://en.wikipedia.org/wiki/Chief_of_staff) which is not the case for this role since people rotate out of it frequently.
1. [Global Leadership Shadow Program:](https://www2.deloitte.com/gr/en/pages/careers/articles/leadership-shadow-program.html) This is too long if only the CEO is shadowed.

### About the CEO Shadow Program

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/ExG8_bnIAMI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->

### Reasons to participate

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Cg0LzET_NWo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->

### What is it like?

Considering joining the program? Hear from past shadows about their experience:

1. [Day 2 of Erica Lindberg](https://www.youtube.com/watch?v=xrWR0uU4nbQ)
1. [Acquisitions, growth curves, and IPO strategies: A day at Khosla Ventures](/blog/2019/04/08/khosla-ventures-gitlab-meeting/)
1. [GitLab CEO Shadow Update - May 30, 2019](https://www.youtube.com/embed/EfBMu9dTpno)

<!-- blank line -->

<figure class="video_container"><iframe src="https://www.youtube.com/embed/EfBMu9dTpno"></iframe></figure><!-- blank line -->
1. [Key takeaways from CEO Shadow C Blake](https://youtu.be/3hel57Sa2EY)
<!-- blank line -->
<figure class="video_container"><iframe src="https://www.youtube.com/embed/3hel57Sa2EY"></iframe></figure><!-- blank line -->
1. [AMA with the CEO Shadow Alumni on 2019-08-23](https://www.youtube.com/watch?v=TxivABJ16jE)
<figure class="video_container"><iframe src="https://www.youtube.com/embed/TxivABJ16jE"></iframe></figure><!-- blank line -->
1. [Reflecting on the CEO Shadow Program at GitLab](https://youtu.be/DGJCuMVp6FM)
<!-- blank line -->
<figure class="video_container"><iframe src="https://www.youtube.com/embed/DGJCuMVp6FM"></iframe></figure>
1. [Key Takeaways and Lessons Learned from a Remote GitLab CEO Shadow Rotation](https://youtu.be/4yhtYcOZn3w)
<!-- blank line -->
<figure class="video_container"><iframe src="https://www.youtube.com/embed/4yhtYcOZn3w"></iframe></figure>
1. [AMA with the CEO Shadow Alumni on 2020-07-02](https://www.youtube.com/watch?v=Yey83s3zGMM)
<!-- blank line -->
<figure class="video_container"><iframe src="https://www.youtube.com/embed/Yey83s3zGMM"></iframe></figure><!-- blank line -->

### Impact of the CEO Shadow Program

This is feedback received from some Alumni shadows and their managers.

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ1FyJrtnLS76qq-Oi5wi3F7Gd5yJFQc-DyOSVTNZgxM63rJQUtYmWa_4mp5MjaGa_rut5o68fNdNep/embed?start=true&loop=false&delayms=2000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

### What is the feedback from the CEO?

Hear what our CEO has to say about the CEO shadow program.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/gJWMBI64sZk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Participating in the program

### Eligibility

You are eligible to apply for the program if you have been with GitLab for at least 1 month (recommended more than 3 months) and you are a:

1. [Manager or up](/company/team/structure/#layers), Staff engineer ([backend](/job-families/engineering/backend-engineer/) or [frontend](/job-families/engineering/frontend-engineer/)), [SAL](/job-families/sales/strategic-account-leader/), [PMM](/job-families/marketing/product-marketing-manager/), or [Product Manager or up](/job-families/product/product-manager/)
1. [Individual Contributor](/company/team/structure/#layers), if there is 1 consideration. This includes TAM and SA roles.

Considerations are cumulative and can be:

1. You belong to a select under-represented group as defined in our [referral bonus program](/handbook/incentives/#add-on-bonus-for-select-underrepresented-groups). Multiple under-represented groups are cumulative
1. You are a recipient of GitLab’s Value Award of Transparency, Collaboration, Iteration, Efficiency, Results, or Diversity at previous GitLab Contribute events
1. There is last minute availability. Last minute means the first day of a rotation is less than a month out. This applies to all GitLab team members, _including_ CEO Shadow Alumni who wish to participate again
1. You are based in APAC
1. You belong to a GitLab Team Member Resource Group ([TMRG](/company/culture/inclusion/erg-guide/))


Exceptions to eligibility:

CEO Shadow rotations will be reserved for [All-Directs](/company/team/structure/#all-directs) during the week of [E-Group Offsites](/company/offsite/#schedule). An exception will be made if there is last minute availability as two All-Directs are not available to serve in this role during this window.

**COVID-19 Note:** During this time, all shadow rotations are fully remote.
Given the CEO generally works from 8 a.m. to 6 p.m. Pacific,
it's best for remote shadows to be in Pacific, Mountain, or Central time zones.
Other time zones will be considered on a case-by-case basis.

Learn more about what to expect from a [remote shadow rotation](#remote-shadow-rotations).

Shadows with scheduled rotations always have the option to delay to later in the year if they'd prefer to do their rotation in person.
Shadows who have chosen to delay their rotations until another time include:

- Jarka Kosanova
- Liam McNally
- Bartek Marnane

### How to apply

1. Create a merge request to add yourself to the [rotation schedule](#rotation-schedule). Ensure the merge request description highlights how you meet the eligibility criteria (merge request examples: [Anastasia Pshegodskaya](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/43159), [Jackie Meshell](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/42958), [Philippe Lafoucrière](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51265)).
1. Assign your manager and ask them to approve (**but not to merge**) the merge request. Managers, please ensure the candidate meets the eligibility criteria.
1. Once your manager approves the merge request, assign the merge request to the [Executive Business Admin supporting the CEO](/handbook/eba/#executive-business-administrator-team), link to the merge request in the `#ceo-shadow` channel, and `@mention` the Executive Business Admin supporting the CEO in the message.

Please keep in mind when selecting dates that the CEO's schedule is fluid and subject to constant change, which also means that the CEO shadow rotation is subject to constant change. The posted dates are not guaranteed. We will work with you to reschedule your rotation if a conflict arises.


### Parental Participation

We understand that participation in the CEO Shadow Program is optional and can cause hardships at home. To help overcome these challenges and to allow flexibility for parents to participate, there will be some rotations identified as "parent-friendly" weeks. These are weeks when Sid doesn't need a shadow for the full 5 workdays or where the program is split so the weeks are not consecutive.

### Rotation rhythm

We want many people to be able to benefit from this program, therefore we rotate often.
It is important that an incoming person is trained so that the management overhead can be light.
Currently, a rotation is two weeks:

1. See one, you are trained by the outgoing person.
1. Teach one, you train the incoming person.

The shadow should be available for the full two weeks.

When the CEO has a week or more of paid time off, or during [Contribute](/events/gitlab-contribute/), the program will pause, one shadow will "see one" before the break and "teach one" after the break.
The rotations with breaks of one or more weeks without a shadow are great if you can't be away from home for more than one week at a time.

If you need childcare to be able to participate, GitLab will [reimburse you](/handbook/spending-company-money/) for it.

This program is not limited just to long-term GitLab team members.
For new team members, this might even be the first thing they do after completing our [onboarding](/handbook/people-group/general-onboarding/).
Exceptional community members may be able to participate, as well.

### Rotation schedule

| Start date | End date | See one | Teach one |
| ---------- | -------- | ------- | --------- |
| 2021-12-13 | 2021-12-17 | [Cynthia Ng](https://gitlab.com/cynthia) - Sr. Support Engineer | [James Heimbuck](https://gitlab.com/jheimbuck_gl) - Sr. Product Manager |
| 2022-01-03 | 2022-01-07 | [Josh Zimmerman](https://gitlab.com/Josh_Zimmerman) - Learning & Development Manager | [Cynthia Ng](https://gitlab.com/cynthia) - Sr. Support Engineer |
| 2022-01-10 | 2022-01-14 | AVAILABLE-REMOTE (All Directs and 11 Cross-Functional Initiaive Folks Have Priority) | [Josh Zimmerman](https://gitlab.com/Josh_Zimmerman) - Learning & Development Manager |
| 2022-01-18 | 2022-01-21 | [Cesar Saavedra](https://gitlab.com/csaavedra1) - Sr. Technical Marketing Manager | AVAILABLE-REMOTE (All Directs and 11 Cross-Functional Initiaive Folks Have Priority) |
| 2022-01-24 | 2022-01-28 | [Darby Frey](https://gitlab.com/darbyfrey) - Staff Fullstack Engineer | [Cesar Saavedra](https://gitlab.com/csaavedra1) - Sr. Technical Marketing Manager |
| 2022-01-31 | 2022-02-04 | [Kurt Dusek](https://gitlab.com/kdusek) - Sr. Solution Architect | [Darby Frey](https://gitlab.com/darbyfrey) - Staff Fullstack Engineer |
| 2022-02-07 | 2022-02-11 | UNAVAILABLE - SKO | UNAVAILABLE - SKO |
| 2022-02-14 | 2022-02-18 | AVAILABLE - REMOTE | [Kurt Dusek](https://gitlab.com/kdusek) - Sr. Solution Architect|
| 2022-02-22 | 2022-02-25 | AVAILABLE - REMOTE (short week due to holiday) | AVAILABLE - REMOTE |
| 2022-02-28 | 2022-03-04 | AVAILABLE - REMOTE | AVAILABLE - REMOTE |
| 2022-03-07 | 2022-03-11 | AVAILABLE - REMOTE | AVAILABLE - REMOTE |
| 2022-03-14 | 2022-03-18 | AVAILABLE - REMOTE | AVAILABLE - REMOTE |
| 2022-03-21 | 2022-03-25 | AVAILABLE - REMOTE | AVAILABLE - REMOTE |
| 2022-03-21 | 2022-03-25 | AVAILABLE - REMOTE | AVAILABLE - REMOTE |
| 2022-03-28 | 2022-04-01 | AVAILABLE - REMOTE | AVAILABLE - REMOTE |


If you have questions regarding the planned rotation schedule, please ping the [Staff Executive Business Admin to the CEO](/handbook/eba/#executive-business-administrator-team). The EBA to the CEO manages the rotation schedule, please do not add new dates to the schedule when adding in your rotation. The CEO's schedule is subject to constant change and your rotation may need to be rescheduled.

## Preparing for the Program

### Important things to note

1. This is not a performance evaluation.
1. Plan to observe and ask questions.
1. Don't plan to do any of your usual work. Prepare your team as if you were on vacation. In PTO by Roots, you should select "CEO Shadow Program" as the type, and assign someone to cover for you while you are in the program. This type of PTO by Roots is set as "Available" which sets Slack status and a free / visibility calendar event only. You may disable the Slack OOO auto-responder within `PTO by Roots app > Slack > CEO Shadow Program` if you find it obtrusive.
1. Be ready to add a number of [handbook](/handbook/handbook-usage/) updates during your shadow period.
1. Participating in the shadow program is a privilege where you will be exposed to confidential information. This is underpinned by trust in the shadows to honor the confidentiality of topics being discussed and information shared. The continuation of this program is entirely dependent on shadows past, present, and future honoring this trust placed in them.
1. Give feedback to and receive feedback from the CEO. Participants in the shadow program are encouraged to deliver [candid feedback](/handbook/people-group/guidance-on-feedback/#guidelines-for-delivering-feedback). Examples of this are to the CEO and to the world about the company if they make a blog post or video. Shadows maintaining confidentiality during the program is separate from shadows being able to provide candid feedback.

### What to wear

You **do not need to dress formally**; business casual clothes are appropriate. For example, Sid wears a button-up with jeans most days. GitLab shirts are acceptable when there aren't any external meetings. Review Sid's calendar to check if there are formal occasions - this may require different clothing. If unsure, please ask the Executive Business Administrator (EBA) in the `#ceo-shadow` Slack channel
Make sure to bring comfortable shoes with you to Mission Control any time there are meetings in the city. Wear whatever you are comfortable in, keeping in mind that Sid prefers to walk, even if his calendar says Uber.

### Pre-Program Tasks

#### Create an onboarding issue

Outgoing shadows are responsible for training incoming shadows. We currently track onboarding and offboarding in the [ceo-shadow](https://gitlab.com/gitlab-com/ceo-shadow) project.

**Make sure you do the following:**
 * **Create an onboarding issue.** The incoming shadow is responsible for creating their onboarding issue by the Friday before they start the program using the [`onboarding` template](https://gitlab.com/gitlab-com/ceo-shadow/onboarding/-/issues/new?issuable_template=onboarding). Assign the issue to both the incoming and outgoing shadows (the person who will be "teaching one" and yourself).
 * **Create an offboarding issue.** Prepare the issue on your second week by using the [`offboarding` template](https://gitlab.com/gitlab-com/ceo-shadow/onboarding/-/issues/new?issuable_template=offboarding).

#### Consider creating goals

Consider adding goals for your time as a CEO Shadow, and adding them to your onboarding issue. To make your goals more actionable, you may want to use the [SMART goals framework](https://www.mindtools.com/pages/article/smart-goals.htm).

Doing this will help you reflect upon your overall CEO Shadow experience more easily, and it may help you write a better blog post after you complete the program.

For inspiration, here is [an example](https://gitlab.com/nmccorrison/ceo-shadow/-/issues/1#my-goals) of a CEO Shadow who added goals to their onboarding issue.

#### Practice your introduction

You will get asked about yourself during the program, and it's important to describe it correctly. So stand in front of a mirror and practice 3 times. The main point is, do _not_ say that your role is to "follow Sid around" or "follow the CEO around". The program is for exploring and learning about all the parts of GitLab, and there's where the emphasis should lie. See [CEO Shadow Introductions](#ceo-shadow-introductions) for specifics.

#### Coffee chat with Co-shadow

Before your scheduled rotation, try to schedule coffee chats with your co-shadow before you start the program. This gives you the opportunity to get to know them and help set expectations for the rotation.

#### Coffee Chat with CEO Shadow Alumni

Feel free to schedule a coffee chat with any of the CEO Shadow Alumni. You can review the list of [CEO Shadow Alumni](/handbook/ceo/shadow/#alumni) below. These chats can be helpful when deciding whether to apply to participate or if you're unable to participate but want to hear about the experience and what alumni have learned while shadowing.

#### Coffee Chat with the CLO

Due to the sensitivity of some of the conversations you'll be listening in on, you may end up having questions about what you can talk about or share with others. Should it be useful or complementary to your time as a CEO Shadow, our [Chief Legal Officer (CLO)](/job-families/legal/chief-legal-officer/) hosts Monthly Shadow Chats on the 3rd Tuesday of every month at 08:30 PST.

Please @ mention the CLO's [Sr. EBA](/handbook/eba/#executive-business-administrator-team) in `#ceo-shadow` should you wish to be added to an upcoming session, or if you would like to schedule a 1:1 coffee chat with the CLO. This option is available to all Shadows, past, present, and future.

#### Explore the CEO Shadow project

CEO Shadows use the [ceo-shadow](https://gitlab.com/gitlab-com/ceo-shadow) project to track issues and coordinate the requests that result from the [CEO's meetings](#meetings--events). It is linked in the CEO Shadow channel description on Slack.
Check out the ongoing CEO Shadow tasks on the [To Do issue board](https://gitlab.com/gitlab-com/ceo-shadow/tasks/-/boards/1385894).

#### Review the CEO's calendar

Review the [CEO's calendar](#ceos-calendar) to get an idea of what your upcoming weeks will be like.

#### Review the [CEO Handbook](/handbook/ceo/)

The CEO has a [section in the handbook](/handbook/ceo/) that details processes and workflows specific to him as well as his background, communication style, strengths, and flaws. Take time to invest in your relationship with him upfront by reviewing this part of the handbook. Here are some helpful sections:

1. [Communication](/handbook/ceo/#communication)
1. [Pointers from direct reports](/handbook/ceo/#pointers-from-ceo-direct-reports)
1. [Strengths](/handbook/ceo/#strengths)
1. [Flaws](/handbook/ceo/#flaws)

#### Review acronyms

If you're not familiar with some of the business acronyms, take a bit of time to review them. The [Product Performance Indicators](/handbook/product/performance-indicators/) handbook page has some useful acronyms as well as concepts you're likely to come across.

- [KPI (Key Performance Indicator)](/company/kpis/#what-are-kpis)
- [OKRs (Objectives and Key Results)](/company/okrs/#what-are-okrs)
- Product performance indicators such as [xMAU (Monthly Active User)](/handbook/product/performance-indicators/#three-versions-of-xmau) and [SpU (Stages per User)](/handbook/product/performance-indicators/#stages-per-user-spu)
- [Revenue definitions](/handbook/sales/sales-term-glossary/#revenue-definitions) such as [ARR (Annual Recurring Revenue)](/handbook/sales/sales-term-glossary/arr-in-practice/#annual-recurring-revenue-arr)
- Other business terminology may come up. [Wall Street Oasis Finance Dictionary](https://www.wallstreetoasis.com/finance-dictionary#DicT) is a handy guide.

**Note:** This list is not meant to be exhaustive and [should not become a glossary](/handbook/handbook-usage/#single-source-of-truth). While we strive to be handbook first, you may find that we are using acronyms without a clear handbook definition. If you can't find it in the handbook or find a standard definition on Google, ask someone what the acronym means. Not being able to find it could be a sign that we need to do a better job with documentation.

## What to expect during the program

### Tasks

The value of the CEO Shadow Program comes from the [broader context](#goal) you'll gain and the interesting conversations you'll witness.

Since your rotation is over a short period of time, there are no long-term tasks you can take on.
However, there are many short-term administrative tasks you'll be asked to perform as a shadow. Here are some examples:

1. Make [handbook](/handbook/) updates (use the [ceo-shadow](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests?scope=all&utf8=%E2%9C%93&state=all&label_name%5B%5D=ceo-shadow) label).
Post the MR links in the `#ceo` Slack channel and `@`-reference the CEO so the CEO knows they have been completed. It is not required to create issues for these tasks. Go directly to a merge request if it is more [efficient](/handbook/values/#efficiency).
1. Prepare for, take notes during, and follow up on meetings. See more details below about your [meeting responsibilities as a shadow.](#responsibilities)
1. Go through open merge requests and work towards merging or closing any that have [not been merged](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?label_name%5B%5D=ceo-shadow).
1. Go through open issues in the [CEO shadow project](https://gitlab.com/gitlab-com/ceo-shadow/tasks/-/issues) and work towards closing or creating a subsequent merge request to close out. Communicate updates on those tasks in the `#ceo-shadow` channel.
1. Publicly advise when people are not following the [communication guidelines](/handbook/communication/) ([see more details below.](/handbook/ceo/shadow/#promote-communication-best-practices)). For example, remind team members to stop screen sharing to encourage communication.
1. Iterate and complete small tasks as they come up. Clear them out immediately to allow for rapid iteration on more crucial tasks. Communicate updates on those tasks in the `#ceo-shadow` channel.
1. Solve urgent issues. For example, help solve a complaint from a customer or coordinate the response to a technical issue.
1. Share [thanks](/handbook/communication/#say-thanks) in the `#thanks` channel in Slack when it comes from a customer or wider community member in a meeting.
1. Compile a report on a subject.
1. Write an blog post on the public company blog, a recorded reflection of your experience, or a CEO Interview on a topic of your choice. Please see [information about pitching and publishing a blog post](/handbook/marketing/blog/#how-to-suggest-a-blog-post) for information about the publishing process, and be sure to read previous CEO shadows' blog posts before you start writing to ensure that your post has a new angle. Link this to the table in the [Alumni](/handbook/ceo/shadow/#alumni) section. These do not need to be approved by the CEO but he will happily review them if you'd like. The posts should however follow the parameters outlined in the [additional note-taking guidelines](https://docs.google.com/document/d/1vkHile2eHVTEl1S7-qv4eEFDc64ghUSesBfvFNz7qfI/edit). In the event you have any questions about what is okay to share, please reach out to the GitLab Legal team in slack at `#legal` or the Corporate Communications team in `#corpcomms`.
1. Ensure visual aids and presentations are visible to guests during in-person meetings.
1. Prepare for and receive guests at Mission Control.
1. Offer GitLab swag to guests at Mission Control before they leave.
1. Answer the phone and door at Mission control.
1. Provide training to the incoming CEO Shadow(s).
1. Speak up when the [CEO displays flawed behavior](/handbook/ceo/#flaws).
    - Share an overall Positivity Score in the `#ceo-shadow-private` Slack channel with the CEO immediately following meetings (excluding 1:1s). Scores can range from 1 (the least positive someone could respectfully be) to 10 (the most positive someone could authentically be), focused on the CEO's presence and reactions during the meeting.

#### Collecting and managing tasks
{:.no_toc}

The CEO shadows maintain a project called [CEO Shadow Tasks](https://gitlab.com/gitlab-com/ceo-shadow/tasks/-/boards/1385894).
It is linked in the `#ceo-shadow` Slack channel description.
Collect tasks using the first name of the shadow who captured it and
the name of the person that will complete the task.
Once an MR has been opened, post in the `#ceo-shadow` channel.

### Meetings & Events

#### CEO's Calendar

1. At the start of the week, review the CEO's calendar. The CEO's calendar is the single source of truth. Shadows should check the CEO's calendar for updates often. You will not be invited to each meeting. Meetings that the shadows may **not** attend will have a separate calendar entry on the CEO's schedule that states "No CEO Shadows". When in doubt, reach out to CEO Executive Business Admin to confirm if you should attend or not. There will be some meetings and events the shadows do not attend. Do not feel obligated to attend every meeting — all meetings are considered optional.
1. As a reminder, you should have your title updated within your last name on zoom. During the Shadow rotation update your title to "CEO Shadow". [Here is the how to ](https://about.gitlab.com/handbook/tools-and-tips/zoom/#adding-your-title-to-your-name)update your title as part of your last name to ensure it shows up on zoom.
1. Add the CEO's calendar to your Google Calendar by clicking the `+` next to "Other Calendars". Then click `Subscribe to Calendar`, search for the CEO's name, and click enter.
1. Because candidate interviews are marked as "private" (busy) for confidentiality reasons, the Executive Business Admin will invite the shadows to those events directly. As a result, you will get an email from Greenhouse asking for candidate feedback, which is not necessary.
1. Meetings with those outside of GitLab may not be on Zoom. Prior to the call, check the CEO's calendar and load any other conferencing programs that may be needed. It may be necessary to dial in via phone for audio-conferences. If you have any problems confirming the link, reach out to the `#ceo-shadow` slack channel.

#### Types of meetings

There are three types of meetings on the CEO's calendar: GitLab meetings, Valley meetings, and personal meetings. Please note, the program's continued success depends on the participants respecting confidentiality during the program, after the program, and after they leave GitLab.

##### GitLab Meetings
{:.no_toc}

You will attend all GitLab meetings of the CEO, including but not limited to:

1. [1-1s](/handbook/leadership/1-1/) with reports.
1. Interviews with candidates.
1. Conversations with board members.

Like all meetings at GitLab, meetings will begin promptly, regardless of the shadows' attendance. You will travel with the CEO to meetings, team off-sites, and conferences outside of San Francisco per the CEO's schedule.
Executive Business Admin to the CEO will assist you with conference registration and travel accommodations during these time frames.

The CEO's Executive Business Admin will ask external people if they are comfortable with the shadows joining prior to the scheduled meeting, and will share a link to the CEO shadow page to provide context.

Meeting agendas should be shared with `ceo-shadow@gitlab.com`, as shadows will be added to this email alias prior to the rotation, and removed at the conclusion of it.
For agendas that contain sensitive information, the sensitive information should be removed and the document shared with "View only" access to restrict access to the document's history.
Not all agendas will be shared, though, and the CEO Shadows should feel empowered to ask for access if that is the case.
Sometimes, the answer will be "no" for sensitive reasons.

These meetings can have different formats:

1. Video calls.
1. In-person meetings.
1. Dinners that are business related.
1. Customer visits.
1. Conferences.

You will not attend a meeting when:

1. Someone wants to discuss a complaint and wants to stay anonymous.
1. If any participant in the meeting is uncomfortable.
1. If the CEO wants more privacy.

###### Social calls
{:.no_toc}

When it comes time for the [Social calls](/handbook/communication/#social-call), CEO shadows should attend on their own.
You are encouraged to share your experience as a shadow with your call group while you are shadowing.

###### E-Group Weekly Meetings and E-Group Quarterly Offsites
{:.no_toc}

The weekly E-group meeting and quarterly E-Group offsites are fast-paced, with a lot of back and forth discussion between team members. Remember that it is more important to capture accurate takeaways than precise notes if you can't type fast enough to keep up with the conversation.

Occasionally, other team members are invited to discuss a specific topic with E-Group. If so, a separate agenda shared with the invited team members will be included in a calendar invite adjacent to the main calendar event and agenda. In this case, shadows should take notes in the agenda for the specific topic versus the main E-Group agenda

###### Media Briefings
{:.no_toc}

CEO Shadows may be the point of contact for helping coordinate (not schedule) media briefings.
Take initiative, for example finding a quiet space for the CEO to take the call, if it is done while traveling.
When participating in media briefings, CEO Shadows are to act as silent participants, except when directly asked a question.

###### Candidate Interviews
{:.no_toc}

If the candidate is comfortable with it, CEO shadows will attend interviews performed by the CEO.
When scheduling an interview with the CEO, the EBA to the CEO will create a shared Google Doc for notes between the shadows and the CEO. The doc template can be found by searching "Notes Doc for Candidate Interviews" in Google Drive. If you have any questions, please @ mention the EBA to CEO in `#ceo-shadow` in Slack.
This notes document is then added to the scorecard for the candidate in Greenhouse.

1. Shadows should ensure they mark comments they provide with their full name.
1. Please **do not** complete the automated Greenhouse report that follows the interview, entitled `REMINDER: Please fill out your scorecard for [NAME]`. CEO shadows are asked to simply delete this email.

##### Valley Meetings
{:.no_toc}

The CEO may occasionally invite you to optional meetings that may not be explicitly GitLab related, but can help provide insight into his day-to-day activities. In these meetings, it is asked that you not take notes because we don't want you to do work that isn't for GitLab. Additionally if the agenda document is not shared with you most likely it is because it is owned outside of the GitLab domain therefore requesting access is not advised. However, keeping time can still be very helpful so you are encouraged to do so if you are in attendance. These meetings are optional and you can leave at any time.

Valley meetings can usually be identified in two ways:

1. The text `For the CEO shadows: this is a Valley Meeting and attendance is optional` in the meeting description.
1. The organizer's email address ends in `@sijbrandij.com`.

##### Personal Meetings
{:.no_toc}

Personal meetings will be marked as "busy" on the calendar. Shadows do not attend personal calls.

##### CEO Handbook Learning Sessions
{:.no_toc}

The CEO and the [Learning & Development](/handbook/people-group/learning-and-development/) team host weekly [handbook learning sessions](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions) to record videos for handbook pages. During these sessions, CEO Shadows are recommended to do the following:
1. Follow the [agenda doc](https://docs.google.com/document/d/1dKmYWl1OQB5LIm43iZv5ExZz8Iqty2kiys3RuVUVRhs/edit#heading=h.954v91mukl7r) during the session
1. Take notes of important points during the call (no need to be comprehensive, this is geared towards improvements later on) and collaborate with the CEO shadows on points to include on the page
1. Open an MR for the specific page being discussed during or following the recording
1. Make updates to the page based on points captured in the agenda doc and discussion, ([example MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77125/diffs#4b5137b156df6eb619d14177c6847ba13f774506)).
1. The goal is to make five improvements to the page
1. Following the recording, include the video on the respective handbook page
1. Solicit feedback on the MR with the [L&D team](/handbook/people-group/learning-and-development/#ld-organization) and CEO in the CEO channel
1. Make additional MR's to the specific page if necessary



#### Removing yourself from personal CEO documents

For certain meetings, such as [Valley Meetings](/handbook/ceo/shadow/#valley-meetings), a CEO Shadow may be added to an agenda document that is accessible to people outside of the GitLab organization.

At the conclusion of the call, the CEO Shadows should remove themselves from document(s) they were added to via the following steps.

1. Click the `Share` button atop the Google Doc
1. On the resulting pop-up, click into `Advanced`
1. Click the `X` by your name to remove yourself
1. Click `Save changes`
1. On the resulting `Are you sure?` dialog box, click `Yes`
1. You should see a dialog appear noting that `Your access has expired`

#### Responsibilities

Meetings come in many different formats. Your responsibilities may change slightly based on the kind of meeting.

Here are the responsibilities shadows have during meetings:

| Meeting type                     | Notes?         | Timekeeping?   |
| -------------------------------- | -------------- | -------------- |
| 1-1                              | No notes unless requested | No timekeeping unless requested |
| Legal meetings outside of Key Reviews/GCs/etc. (see [doc](https://docs.google.com/document/d/1vkHile2eHVTEl1S7-qv4eEFDc64ghUSesBfvFNz7qfI/edit) for details)                | No notes       | Timekeeping    |
| GitLab Board meeting or 1-1 meetings with a GitLab [Board Member](/handbook/board-meetings/#board-of-directors)                   | No notes       | Timekeeping    |
| Internal meeting (CEO not host)  | Notes optional | Timekeeping    |
| [Valley Meetings](/handbook/ceo/shadow/#valley-meetings)  | No notes       | Timekeeping    |
| L & D Weekly Learning Session    | Special ([See above](/handbook/ceo/shadow/#ceo-handbook-learning-sessions)) | No Timekeeping |
| Customer Meeting    | Yes, please use the externally-shared collaboration doc | Timekeeping |
| Anything else (unless specified) | Notes          | Timekeeping    |

Assume that you are taking notes in a Google Doc affixed to the meeting invite unless it is explicitly stated not to take notes.

If you're unsure whether or not to take notes, default to take them, ask the CEO before the meeting begins, or ping the [EBAs](/handbook/eba/#executive-business-administrator-team) on Slack via `#ceo-shadow` or `ceo-shadow-private` (depending on the sensitivity of the meeting).


##### Taking notes

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/DtRWvIzOnP4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

The goal of the notes is to collect the main points and outcomes, not a full transcript of the conversation. As you are taking notes, be mindful that the goal of the program is to absorb what is being said in the meetings you are in.

In many instances, when shadows are taking notes during meetings, the discussion is moving too quickly to fully or accurately capture all of the discussion points. Additionally, the shadows may not be fully aware of the full context of the discussion or familiar with certain terminology being used in the meeting. The meeting notes are also not reviewed nor approved by the meeting attendees. Accordingly, meeting notes should not be the SSOT for any meeting as they may not fully or accurately capture the discussion that was had at the meeting.

**CEO Shadows are required to read [these additional note-taking guidelines](https://docs.google.com/document/d/1vkHile2eHVTEl1S7-qv4eEFDc64ghUSesBfvFNz7qfI/edit).** If a notes document is not already linked, see the [templates available here](/handbook/eba/#meeting-request-requirements).
CEO Shadows are not always the DRI for notes and everyone can contribute to taking notes when in a meeting.

Tips:

1. It's helpful if shadow one takes notes as the first speaker is talking, then shadow two starts when the next speaker continues the conversation. Shadow one can pick up note taking again when the next speaker contributes. By alternating this way, the shadows are better able to keep up with all the participants in the conversation.
1. Add extra blank lines after a bulleted or numbered line to make it easier for multiple notetakers to type notes. In the agenda provided, press enter to add a numbered line, followed by a space and press enter again to create a blank line (as Google documents will not allow a numbered line to be empty).
1. Consider clicking somewhere in the document where people are not actively reading or writing so that your name next to the cursor doesn't hide that text.
1. Showing up one to two minutes early to a meeting may give you an opportunity to network with GitLab team members who you do not know.
1. Put Zoom in "gallery mode" so you can see all participants (rather than only the person currently speaking). This allows everyone to see the entire audience which creates a more inclusive environment and fosters better communication.
1. If you aren't sure where meeting attendees are in a Google document, click on their face or initials in the upper-right side of the window. Doing this will cause your cursor to jump to wherever their cursor is in the document.
1. It can be challenging to coordinate with the other shadow on who is taking notes at any point in time. Consider agreeing with the other shadow to "show your hands" in Zoom to indicate that you are not currently taking notes.
1. Sometimes people will provide some color or context before providing the main point. Consider listening to the first sentence before documenting to ensure you are capturing main points instead of what they are saying verbatim.
1. Observe how Sid takes notes as a guide to the level of detail and summarization.

##### Keeping time

Shadows are responsible for being aware of the current time and verbally providing timekeeping in many [types of meetings](#responsibilities). This allows participants to comfortably wrap up the meeting.

**Tips for time-keeping:**
- Decide who. Past shadows have found it helpful for the individual in the second week of the program to commit to keeping time when applicable.
- Understand meeting length. Use the calendar invite as an indication of how long the meeting should last. Remember, we do [speedy meetings](/handbook/communication/#scheduling-meetings).
- Shadows should provide a 5-minute and a 1-minute notification. You can write "Time check, 5 minutes" on Zoom chat.
- **If a meeting is running over the allocated time, unmute and verbally say "we're in overtime".** Don't wait for a break in the conversation.

**Tools to help.**

You can use this [shell script](setalarm.sh) (MacOS only) to run a timer for the desired number of minutes. The script will notify you 5 minutes before the end of the meeting, and will copy "We have five minutes left" to you clipboard, so you can paste the text directly in the Zoom chat in addition to verbalizing it. At the end of the meeting, the same will occur with "We are at time".

To use the script:
1. Download [setalarm](setalarm.sh), making sure to preserve the .sh extension. (recommended within your Home folder for easy access)
1. Load MacOS Terminal by running `Cmd+Space` and typing `terminal`.
1. Run `source setalarm.sh` to load the `setalarm` function into memory.
1. Then simply type `setalarm 50` to set a 50-minute timer (`setalarm` defaults to 25 minutes if no argument is provided).

Press `Ctrl+C` if you need to cancel the alarm. You may keep the terminal window running indefinitely so that it is at your finger tips throughout your program duration.

A handy App for time keeping is Senzillo's "Speech Timer for Talks".  It is available for [iOS](https://apps.apple.com/us/app/speech-timer-for-talks/id979433325) and [Android](https://play.google.com/store/apps/details?id=senzillo.talk_timer_free&hl=en_US&gl=US). Compared to other apps, this timer is easy to setup for the meeting warning levels and to switch between meeting time lengths. It costs $1 to have 3 warning levels - purchasing the application is not required if you just wish to have one warning visualization and watch the timer for the others.

##### If you see something, say something

Shadows should notify GitLab meeting participants if their [name and job title](/handbook/tools-and-tips/zoom/#adding-your-title-to-your-name) are not mentioned on Zoom.

If a GitLab team member is sharing their screen before introductions have occurred, shadows should remind GitLab presenters that GitLab does not [recommend sharing screens during zoom meetings](https://about.gitlab.com/handbook/tools-and-tips/zoom/#sharing-your-screen-in-zoom) and to please wait until after introductions have been completed to allow participants to see each other clearly during introductions. This is especially important during external calls when participants are meeting each other for the first time.

If you notice that someone does not have their picture set in Google when a Google document is being actively worked on, [let them know how to set one](https://myaccount.google.com/personal-info) so their picture will show up in the document rather than their first initial. That makes it easier for everyone to find where they are in the document, especially when they are speaking. Let them know that this is 100% optional.

Shadows need to speak up in video calls, and speak up when the CEO's camera isn't working or when the green screen isn't working correctly because of the sun angle.
![Sun on Green Screen Zoom Issue](/images/ceoshadow/ceo_sun_zoom_issue.png)

##### Notify CEO on use of 'I think'

When attending **external meetings**, notify the CEO in a private Zoom chat message when the CEO uses the phrase `I think`. [Hedge words](https://ethos3.com/2020/02/hedging-the-speaking-habit-harming-your-credibility/) can have the effect of making the speaker seem uncertain or unconfident.

In internal meetings, `I think` can be used to signal opportunity for others to present a contrasting opinion.

#### CEO shadow introductions

+**Order of introductions:** At the start of meetings, CEO shadows will introduce themselves. There is no set order for which shadow introduces themselves first. Sometimes one shadow will arrive to the meeting first, and make their introduction as the **first shadow** to speak. During some meetings, Sid may decide the order for CEO Shadow introductions by mentioning one of the CEO Shadows first, usually the CEO Shadow who is completing their last week in the two week program.

It's important to set the correct tone, so please stick to the following introductions verbatim.

When attending [Valley meetings](/handbook/ceo/shadow/#valley-meetings) please be sure to use the **For Valley meetings** intro. It can cause much confusion if you mention GitLab when Sid is acting in an individual capacity.

When attending investor meetings, please introduce yourself and hand it off to the next team member by announcing their name.

When introducing yourself in a meeting as the first shadow, say:

1. I'm NAME.
1. I normally am a/the TITLE.
1. This is my first/last week in the two-week CEO shadow program.
1. **For GitLab-related meetings:** The goal of the program is to give participants an overview of the functions at GitLab.
1. **For Valley meetings (not related to GitLab):** The goal of the program is to give participants an introduction to Silicon Valley discussions.


When introducing yourself in a meeting as the second shadow, say:

1. I'm NAME.
1. I normally am a/the TITLE.
1. This is my first/last week in the two-week CEO shadow program.

Remember, do _not_ say that your role is to "follow the CEO around". It's about getting an overview of the functions at GitLab.

#### Finding meeting recordings

If Sid records a video to the cloud in a meeting it will eventually end up
being uploaded to [the Google Drive](https://drive.google.com/drive/folders/0APOeuCQrsm4KUk9PVA)
folder.
Finding the video will require searching based on the calendar event name and
checking the "last modified" date.

#### Attending in-person events with the CEO

When attending events with the CEO, keep the following in mind:

1. Remind the CEO to bring extra business cards before leaving. And bring a few for yourself.
1. The CEO has outlined his [transport preferences](/handbook/ceo/#transport).
1. When traveling to events in the Bay Area by car, the CEO will request the ride sharing service.
1. When traveling to events on foot, CEO shadows should take responsibility for navigating to the event.
1. After a talk or panel, be ready to help the CEO navigate the room, particularly if there is a time-sensitive obligation after the event.

The CEO often has work events that are also social events.
In Silicon Valley, social and work are very intertwined.
These mostly take the form of lunches or dinners.
CEO shadows are invited unless otherwise specified, but there is no expectation or obligation to join.

### Look for values being lived out

Even in meetings where you are unfamiliar with the subject matter, there is opportunity to learn, document, and shape the evolution of [GitLab's values](/handbook/values/). Re-read GitLab's values prior to your CEO Shadow rotation, and be mindful of new and inventive ways that [CREDIT](/handbook/values/#credit) is lived out during the meetings you attend. You can make a merge request to propose new [sub-values](/handbook/values/#sub-values-as-substantiators), which substantiate top-level values.

### Promote Communication Best Practices

It's important that everyone encourages others to follow the [communication guidelines](/handbook/communication/), not just the CEO. As shadows, in Group Conversations and other settings, you should remind team members to:

1. Verbalize questions
1. Stop sharing their screens to encourage conversations
1. Provide full context for the benefit of new team members
1. When someone starts a Group Conversation with a presentation it is the CEO Shadow responsibility to ask them to stop and [record a video next time](/handbook/group-conversations/#presentation) Recommendation on how to approach this message "Apologies for the interruption, it is the responsibility for the CEO Shadow to remind team members that we do not present during Group Conversations. If we may move to the document for questions and in the future consider recording a video to promote asynchronous presentations".

### Email Best Practices

In order to ensure continuity across CEO shadow participants. Always, cc `ceo-shadow@gitlab.com` on emails as part of the program. This ensures that even after you've left the program the response and follow-up can be tracked.

### Friendly competition

CEO shadows label the handbook MRs they create with the `ceo-shadow` label.
It's a point of competition between CEO shadows to try to best the previous shadows' number of merge requests.
<iframe class="dashboard-embed" src="https://app.periscopedata.com/shared/40ce74a5-00ff-4a5d-8162-b1fe212d8cbc?embed=true" height="700"> </iframe>

### Follow activity from the CEO

Shadows are encouraged to follow the CEO's activity on various platforms to get a complete picture of his everyday activities and where he directs his attention.

#### In Slack
{:.no_toc}

Go to the Slack search bar and type "from:@sid" and it will populate the results.

![Slack User Activity](/images/ceoshadow/slackuseractivity.png)
Follow Sid's Slack activity to follow his everyday engagements

#### In GitLab
{:.no_toc}

This can be seen on the CEO's [GitLab activity log](https://gitlab.com/users/sytses/activity).

![GitLab Activity Log](/images/ceoshadow/gitlabactivitylog.png)
See what issues and MRs Sid is interacting with

#### On Twitter
{:.no_toc}

Check out [Sid's Twitter account](https://twitter.com/sytses).

![Twitter notification](/images/ceoshadow/twitternotification.png)
Sign up for Twitter notifications (Twitter account required) to follow his everyday engagements.

### Documentation focus

An ongoing shadow program with a fast rotation is much more time consuming for the CEO than a temporary program or a rotation of a year or longer.
That's why most organizations choose to either have a shadow for a couple of days, or have someone for a year or more.
We want to give as many people as possible the opportunity to be a shadow, which is why we rotate quickly.
To make this happen without having to invest a lot of time with training, we need great documentation around the program.
A quick turnaround on documentation is crucial, and the documentation will have a level of detail that may not be necessary in other parts of the company.

### Traveling with the CEO

When traveling with the CEO, keep the following in mind:

1. Book flights that will allow you to land before the CEO so there is no delay in transportation to the next event.
1. For airport pickup with the CEO, research the terminal the CEO arrives in and plan to be there to meet with the driver before the CEO.
1. Keep the EBA to the CEO and onsite EBA updated regularly and promptly on estimated arrival time in `#ceo-shadow` Slack channel to ensure the schedule is on time.
1. If travel plans change, please update the EBA(s) in Slack immediately so cancellations to prior transportation can be made promptly to not incur fees.
1. When returning to San Francisco, if on a different airline, be sure to arrive before the CEO and communicate a [meetup location if traveling back to Mission Control](#rideshare-from-airport) together.

### Remote shadow rotations

![GitLab all-remote mentor](/images/all-remote/ceo-shadow-gitlab-awesomeness.jpg)

The CEO Shadow Program is temporarily fully remote because of COVID-19 [travel restrictions](#travel-guidance-covid-19).
The shadows will participate in all meetings from their usual work environment.

While remote shadows won't get to work from Mission Control or attend in-person meetings with the CEO, they will still get an immersive experience through the program.
A remote rotation may also be an ideal opportunity for a team member who has been unable to travel for an in-person rotation in the past.

For insights on maximizing a remote CEO Shadow rotation, view takeaway recap videos from [Betsy](https://youtu.be/DGJCuMVp6FM) (Talent Brand Manager) and [Darren](https://youtu.be/4yhtYcOZn3w) (Head of Remote), as well as Darren's [blog](/blog/2020/05/22/gitlab-remote-ceo-shadow-takeaways/).

#### Tips for remote shadows

1. Be sure that you have an [ergonomic workspace](/company/culture/all-remote/workspace/). You'll be taking lots of notes during meetings, and will want a comfortable setup.
1. Communicate clearly with your co-shadow about shared tasks since you will not be working together in person.
1. Consider alternating who leads note-taking from one meeting to the next, which reduces confusion on who writes first and who follows from one event to the next.
1. Take breaks from your desk when there is a break in the CEO's schedule. Because you're not working from Mission Control or traveling to other meetings with the CEO, it's important to take time to [move around.](/company/culture/all-remote/tips/#dedicate-time-for-health-and-fitness)
1. Depending on your time zone, working in Pacific Time may be an adjustment to your typical working hours. [Plan ahead,](/company/culture/all-remote/getting-started/#establish-routine-with-family-and-friends) especially if you're balancing responsibilities with family, pets, roommates, etc.
1. Consider switching locations in your home or workspace during the CEO's 1:1 meetings. Since you [won't be taking active notes](#responsibilities) in these calls, this is a good opportunity to change position or scenery intermittently throughout the day.
1. Particularly in a remote CEO Shadow rotation, life is more available to sidetrack you. It is OK if you need to drop from a meeting to handle tasks at home, from childcare to answering the door, or anything in between. GitLab puts [family and friends first, work second](/handbook/values/#family-and-friends-first-work-second). If feasible, communicate these instances in the `#ceo-shadow` Slack channel so your co-shadow can assist with ongoing note-taking/tasks.
1. Consider taking one or two days off after your rotation. Being a CEO shadow can be intense. Be aware of [symptoms of burnout](/handbook/paid-time-off/#recognizing-burnout).

## Mission Control Guide

### Working from Mission Control

You are welcome to work from Mission Control, but it is not required to be there in person unless there is an in-person meeting, event, or dinner. It's up to you to manage your schedule and get to places on time. If you are traveling somewhere, meet the CEO at Mission Control at the beginning of the allotted travel time listed on the calendar.

If there is a day during your rotation where all meetings are Zoom meetings, you can work from wherever you want, as you normally would.
You can also work from Mission Control if you prefer.
If you decide to split your day between remote work and working from Mission Control, make sure you give yourself enough time to get to Mission Control and set up for the guest.
It's OK to join calls while mobile.
In addition, feel free to coordinate or join a co-working day with team members in the Bay Area.
To coordinate join the `#loc_bayarea` Slack channel.

Shadows are welcome at Mission Control 10 minutes prior to the first meeting until 6 p.m. Feel free to ask if you can stay later. Don't worry about overstaying your welcome, if Karen or Sid would like privacy they will ask you to leave explicitly. When you arrive and depart, please make sure the door to Mission Control closes fully.

One more thing: the cat feeder is automatic and goes off daily at 10:22am PT (as well as another time, it's a surprise!). No need to be alarmed by the metallic clanging sound.

### Working outside Mission Control

Outside of Mission Control hours, shadows have the following options:

1. If not an in-person meeting, you are welcome to take the meeting from your lodging and then proceed to Mission Control during the Group Conversation.
1. There are coffee shops with early opening hours with Wifi access near Mission Control. This is a great venue to meet with your co-shadow if coordination is needed before heading in.
    1. [Philz Coffee on Folsom Street](https://www.google.com/maps/place/Philz+Coffee/@37.7890402,-122.3975675,16z/data=!4m8!1m2!2m1!1sphilz+coffee!3m4!1s0x0:0xba68b065a8d2c93b!8m2!3d37.7887131!4d-122.3930718), opens at 05:30am.
    1. [Philz Coffee on Front Street](https://www.google.com/maps/place/Philz+Coffee/@37.7890402,-122.3975675,16z/data=!4m8!1m2!2m1!1sphilz+coffee!3m4!1s0x0:0x4b3009758e346914!8m2!3d37.7917908!4d-122.3990035), opens at 05:30am.
    1. [Starbucks Reserve on Mission Street](https://www.google.com/maps/place/Starbucks+Reserve/@37.7896722,-122.3933832,17z/data=!4m8!1m2!2m1!1sstarbucks!3m4!1s0x0:0x76ac8e682da13edb!8m2!3d37.791511!4d-122.3951029), opens at 05:00am.
    1. [Starbucks on the 5th floor of the Salesforce tower](https://www.google.com/maps/place/Starbucks/@37.7899796,-122.3966609,18z/data=!4m8!1m2!2m1!1sstarbucks!3m4!1s0x0:0x4e742c0956f70966!8m2!3d37.7899051!4d-122.394609), opens at 04:00am.

### Mission Control access

When entering the building, the doorperson may ask who you are there to see. Don't say "GitLab" since there is no GitLab office. The doorperson will direct you to the correct lobby.

While there are two sets of keys, it's worthwhile coordinating access to Mission Control with the outbound shadow on your first day. Meeting up on Sunday evening or at a specific time on Monday morning. This will enable the incoming shadow to be introduced into Mission Control without impacting Sid and/or Karen.

### Recommended Food/Drinks nearby

#### Food
{:.no_toc}

There is a [Yelp list](https://www.yelp.com/collection/sVnYVUU_npZzJ6h9koBbxw) that contains recommendations from previous shadows, as well as the [CEO's Favorite Restaurants](/handbook/ceo/#favorite-restaurants).

The list is administered by the `ceo-shadow@` email address. Log in to Yelp with the credentials in the CEO Shadow 1Password vault to add restaurants, update notes, or remove items from the collection.

There are usually also food trucks in front of the Salesforce Tower and on the opposite side of Mission Street from the Salesforce Tower [in an alley](https://goo.gl/maps/oSJMfJ26QerGVPqPA).

#### Drink
{:.no_toc}

1. [Bluestone Lane](https://www.google.com/maps/place/Bluestone+Lane/@37.7900021,-122.4031358,16.56z/data=!4m12!1m6!3m5!1s0x8085808814a10285:0x3b8f6e4330a367d9!2sBluestone+Lane!8m2!3d37.7878896!4d-122.4028954!3m4!1s0x8085808814a10285:0x3b8f6e4330a367d9!8m2!3d37.7878896!4d-122.4028954)
    - Tye Davis Favorite (AMAZING COFFEE) - long walk
1. [Starbucks](https://www.google.com/maps/place/Starbucks/@37.7899941,-122.3977364,18.43z/data=!4m12!1m6!3m5!1s0x8085808814a10285:0x3b8f6e4330a367d9!2sBluestone+Lane!8m2!3d37.7878896!4d-122.4028954!3m4!1s0x80858067244672f9:0xea9a5743328af8ff!8m2!3d37.789545!4d-122.397592)

#### Rewards Cards ("punch cards")
{:.no_toc}

Occasionally, food trucks or restaurants have loyalty rewards cards. It is **not required** but if you get one and want to leave it for future shadows to use, please add to this list and put the reward card in the CEO shadow drawer at Mission Control.

1. [Bowld Acai](https://www.bowldacai.com/) Food Truck

### Mission Control operations

#### Monitors

We have six monitors at Mission Control. They show the following content:

|:---:|:---:|
| **Top Left** | [Team](/company/team) |
| **Top Middle** | [Category Maturity](/direction/maturity/) |
| **Top Right** | [Is it any good?](/is-it-any-good/#gitlab-has-yoy-growth-in-adoption-of-version-control-services-study-while-github-and-bitbucket-both-decline) |

| **Bottom Left** | [Category Maturity](/direction/maturity/) |
| **Bottom Middle** | [Who we replace](/devops-tools/) |
| **Bottom Right** | [Remote Manifesto](/company/culture/all-remote/) on how to work remotely |

##### Configuring the Monitors
{:.no_toc}

Turning all screens on or off might be challenging, since a single remote controls all of them. The easiest way to do that is by covering the tip of the remote with your hand and getting as close as possible to a single screen while turning it on or off.

To configure the sales dashboards:

1. Go to [Clari](https://app.clari.com).
1. Go To Pulse tab.
1. Open the left side bar.
1. Click on the funnel icon. Select “CRO”.
1. Click on the gear icon. Go to Forecasting. Select Net IACV.

##### How to use keyboard and mouse to update screens
{:.no_toc}

The wireless mouse and keyboard are connected to the bottom left TV by default because that one is visible from both sides of the conference table. To update the view on another TV, you have to connect the wireless keyboard and mouse to the desired screen. Afterward, don't forget to return it to the bottom left position for use during meetings.

1. Find the USB jacks and Logitech receiver underneath the bottom, right TV (they're all labeled).
![USB jacks and Logitech Receiver](/images/ceoshadow/HDMI_Logitech.png)
1. Connect the Logitech receiver to the USB receiver for the desired screen.
1. To log into the chrome devices at Mission Control, use the login information in the "[CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi)" in 1Password.

##### Troubleshooting the monitors
{:.no_toc}

Turning all screens on or off might be challenging since a single remote controls all of them. The easiest way to do that is to make a cone with the foil (a piece of foil can be found on the white TV-remote tray) around the front edge of the remote, and getting as close as possible to a single screen while turning it on or off.

Each of the screens at Mission Control use an Asus Chromebit to display the preferred content and are connected to the HDMI1 port of each TV. If you turn on the TVs and one displays a blank screen while on the HDMI1 input, the Chromebit may need to be reset. You can do this by power cycling the Chromebit (they are located behind the bottom right TV and are labeled), connecting the Chromebit to the [keyboard and mouse](/handbook/ceo/shadow/#how-to-use-keyboard-and-mouse-to-update-screens), and logging into it using the credentials in the "[CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi)" via 1Password. Once you have logged into the Chromecast, you can quickly find the proper content as listed in [Configuring the Screens](/handbook/ceo/shadow/#configuring-the-monitors) via the browser's recent history.

##### Updating the software on the monitors
{:.no_toc}

To check that a Samsung TV is up to date:

1. Grab the remote labeled for the TV
1. Hit the home button
1. Go to "Settings"
1. Go to "Support"
1. Select "Software Update"
1. Select "Update Now"

#### AirPlay

To screencast from an iPad or MacBook to the top left screen, switch the "Source" on the **top left** screen to "Apple TV" (HDMI 2).

Using the larger remote (with the white buttons), you can press the white center button in the top row of buttons; this will bring up a list of sources. There is a direction pad on the remote towards the bottom that has `< ^ > v` buttons, as well as the selection button in the center.

When the TV is on the Apple TV source, you may need to change the Apple TV to
AirPlay mode. The Apple TV remote is the small black one with only 5 buttons.
You can click "MENU" until it says AirPlay.

To return to the normal configuration, choose "Source" then "HDMI 1".

#### iPads

There are two iPads at Mission Control (without password protection). You can use the [duet app](https://www.duetdisplay.com/) as a second monitor if desired. Both iPads are linked to the EBA to the CEO's Apple ID. Please verify any purchases ahead of time with the EBA to the CEO.

#### Printer

The printer at Mission Control is called `HP Officejet Pro 8610` and is available over AirPlay/Wifi. The printer is located in Sid's office.

#### Zoom Room

Zoom Rooms is an application used for team members not in San Francisco to participate in meetings happening at Mission Control. There's a separate screen (the **large** one on wheels), a Mac Mini and, iPad at Mission Control for this purpose. The Mac Mini is connected to HDMI1 on the screen, and the iPad operates as a remote control for Zoom Rooms.

1. **Setup**
    1. Turn on the big screen on wheels.
    1. Turn on the Mac Mini.
    1. Start the Zoom Rooms application on the Mac Mini.
    1. If you have to log in or, provide a passcode, both are in the [CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi).
    1. Start the Zoom Rooms application on the iPad (you may need the passcode).
    1. If there's any problem connecting to the service, log out and back in. If that fails, contact the Executive Admin to the CEO.
1. **If you do not have the option to join a meeting on the iPad (It's in "Scheduling mode" - you don't have a "Meet Now" or "Join" button):**
    1. Make sure Zoom Rooms on the iPad is logged in.
    1. Click the settings "gear" icon in the top right hand corner.
    1. Disable the "lock settings" option (you'll need the passcode).
    1. Tap on "Zoom Room Mission Control".
    1. Tap "Switch to Controller".
1. **If the Zoom Rooms app on the iPad is not connecting:**
    1. Check the credentials for the Zoom Room in the [CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi). If unable to log in, please contact the Executive Admin to the CEO on slack.
    1. Log out of the Zoom Rooms app on the iPad.
    1. Log in using the PeopleOps Zoom account credentials in the [CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi).

If the EBA to the CEO is unavailable, the CEO shadow may be responsible for handling the technical details of coordinating Zoom meetings. If using a **webinar**, you will need to be a co-host in order to promote participants to panelists so that they can verbalize their own questions.

#### Podcasts on Zencaster

When joining a podcast in Zencaster, a microphone error can be avoided by leaving your microphone unmuted for the first 30 seconds / minute.

1. Zencaster checks that your mic is working by recording audio so, muting the mic causes the error.
1. The system check happens when you first load the page
1. If you get the microphone error, reload the page and wait for the checks to finish before muting.

#### Maintaining Software and Hardware

The devices in Mission Control are configured to auto-update when new versions are available. As a precaution, shadows in their second week should confirm all software in Mission Control is up to date.

### Visitors

#### Preparing for Visitors

In preparation for guests (customers, investors, etc.) who will be meeting with the CEO or other team members at Mission Control, please note the following prior to the meeting start:

1. All GitLab team members sit on one side. This allows for easy communication with the guests.
1. Set the lighting mode to 'Evening' - lighting controls are located next to the kitchen entrance.
1. Have drinks from the fridge (on the tray) available on the table.
1. Get the glasses from the cupboard above the coffee machine and place them next to the drink tray.
1. Add the Brita water pitcher on the table.
1. Place the keyboard and the mouse (used for Mission Control screens) on the table.
1. Ensure that all 6 monitors are displaying the [correct content](/handbook/ceo/shadow/#monitors). If any of the screens are blank, follow the [trouble-shooting instructions](/handbook/ceo/shadow/#troubleshooting-the-monitors).
1. For investor or banker meetings, see the [confidential issue](https://gitlab.com/gitlab-com/ceo-shadow/tasks/issues/50) with more details on preparations. As a precaution, do not discuss anything related to company performance (i.e. bookings, pipeline, quota attainment, and pricing), the current quarter, or products in development, other than those announced. These restrictions are meant to enforce [Fair Disclosure, Regulation FD](https://www.sec.gov/fast-answers/answers-regfdhtm.html) and avoid the accidental dissemination of material nonpublic information.
1. If someone is attending a meeting at Mission Control via [Zoom Rooms](#zoom-room):
    1. Follow the setup steps for [Zoom Rooms](#zoom-room).
    1. Move the screen to the head of the table.
    1. Click 'Join' in the Zoom Rooms menu and enter the meeting ID from the Google Calendar invite of the meeting in question.
    1. Once the meeting is loaded, click on the participants list and make sure that the iPad is visible from the table.

#### Welcoming Visitors to Mission Control

1. The front desk will call the apartment when a visitor arrives.
1. A Shadow should meet guests at the elevator, greet them, and guide them to Mission Control.
1. Once the meeting is over, a Shadow should escort the guests back to the elevator.

### Suzy (the cat)

Please note that we have a cat named [Suzy](/company/team-pets/#7-suzy). It is a Russian Blue mix which is a [hypoallergenic variety](https://www.russianbluelove.com/russian-blue-cat-allergies/).

Suzy likes attention and will invite you to pet her. If you're allergic to cats consider washing your hands after petting.
If you don't want to wash your hands every time after petting Suzy you can _gently_ paddle pet her using ping pong paddles available in Mission Control. Check out the videos linked below for techniques!
Please note the white pillow on the sofa in Mission Control is the **only** place to paddle pet Suzy. She really enjoys it when you _gently pat_ her sides with the ping pong paddles when she is **on** the white pillow, if she steps **off** the pillow stop petting her. When she gets back onto the pillow you can resume gently paddle petting her.
Please don't pet her after/when she meows since that reinforces the meowing which can be annoying during calls and the night.
You can pick her up but she doesn't like it much and will jump out after about 30 seconds. Lastly, Suzy sometimes tries to drink out of the toilet, please make sure to leave the toilet lid down.
![](/images/handbook/suzypetpillow.jpg)
<!-- blank line -->

#### Paddle Pat technique with Eric Brinkman
{:.no_toc}

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/vf-uEPweMUg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->

#### Paddle Pat & Rub technique with JJ Cordz
{:.no_toc}

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/KLWQH0EDtcg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Mission Control FAQs

1. Everything in the fridge that is liquid can be consumed including Soylent and alcohol.
1. The thermometer is located next to the kitchen entrance.
1. Lighting controls are located next to the kitchen entrance, select 'Evening'.
1. Swag (socks and stickers) is located on the bookshelf near the dashboards. (If the swag is running low, reach out to the Executive Admin to the CEO with a list for restocking)

### Food and drinks at Mission Control

Everything in the fridge that is liquid can be consumed including Soylent and alcohol. The coffee machine is located in the kitchen, coffee pods are in the drawer below the coffee machine.

If the beverages are running low, start by checking the top cupboard above the oven (next to the fridge). There's a step ladder in the laundry (last door on the **left** down the hallway)
If items in the cupboard are also running low, reach out to the Executive Admin to the CEO list of items that need to be ordered in the `#ceo-shadow` slack channel tagging the EBA to the CEO so all items can be ordered at once.
This should happen every Monday. Here is a list of items that may need to be restocked:

1. La Croix (usually there are 3 flavors)
1. Red Bull
1. Sugarfree Red Bull
1. Sprite Zero
1. Coca Cola
1. Diet Coke
1. Fiji water
1. Soylent Cacao
1. Snacks in the lockbox (try to keep enough so that the box is full) - Preferences of snacks for the incoming shadow and restocking any snack preferences from the outgoing shadow.

## Travel & Expenses

### Lodging

Lodging during the CEO shadow program is provided by the company. Executive Admin to the CEO books the accommodation based on availability and cost. You can express your preference (hotel or AirBnB) via email to the Executive Admin to the CEO in question. However, the final decision is made by the Executive Admin based on the distance from the CEO and costs. Executive Admin will provide the accommodation details no earlier than 1 month and no later than 2 weeks before the scheduled rotation.

Accommodation is provided only for the active shadowing period, it is not provided during the shadow program pause (cases when the CEO is unavailable).
In case you are coming from a timezone that is more than 6 hours difference with Pacific Time, it is possible to book the weekend before the first shadow work day to adjust to the new timezone.

If your CEO shadow rotation is two consecutive weeks, it is expected you will be staying the weekend. Accommodation is provided during the weekend.

### Airfare

Airfare can be booked according to our [travel policy](/handbook/travel/#booking-travel-and-lodging) or [spending company money](/handbook/spending-company-money/) policy.
In case your shadow rotation includes time without shadowing, it is possible to expense airfare to fly home and back within the continental USA. If you are from outside of the USA, it is also possible to expense airfare during the time without shadowing because of the possible high cost of lodging in San Francisco if you chose to stay at a different location.

#### Rideshare from airport

At San Francisco International airport (SFO), all rideshare apps (Uber, Lyft, etc) pick up on level 5 of the parking structure. When coordinating travel from SFO to Mission Control with other Shadows, GitLab team members, or Sid, arranging to meet on level 5 of the parking structure is most efficient, as each terminal has its own baggage claim area.

### Expensing meals

Shadows are able to expense food and beverage during their rotation and should follow our [spending company money](/handbook/spending-company-money/) policy. Previous shadows have created a [recommendation list](/handbook/ceo/shadow/#recommended-fooddrinks-nearby) of their favorite food places to help.

### Childcare

Childcare is provided during the active shadowing period and will be reimbursed via your expense report. You must book the childcare yourself and it is advised you reach out far in advance as childcare "drop-ins" can be limited depending on the week. Currently, GitLab doesn't have a ["Backup Care"](https://www.brighthorizons.com/family-solutions/back-up-care) program so you must tell the childcare it is for a "drop-in". Depending on your hotel accommodations, finding a nearby daycare is most convenient or a daycare nearby the [Millennium tower](https://www.google.com/maps/place/Millennium+Tower+San+Francisco/@37.7905055,-122.3962516,15z/data=!4m2!3m1!1s0x0:0x9fe15ebd4a8300d8?sa=X&ved=2ahUKEwiUoZ_hpb_iAhXBop4KHeOAB2QQ_BIwGHoECAsQCA). Some childcare facilities will require payment at end-of-day or end-of-week via cash/check only so request an invoice/receipt for expense submission purposes.

Past Childcare facilities that have been accommodating:

1. [Bright Horizons at 2nd Street](https://child-care-preschool.brighthorizons.com/ca/sanfrancisco/2ndstreet?utm_source=GMB_yext&utm_medium=GMBdirectory&utm_campaign=yext&IMS_SOURCE_SPECIFY=GMB) - This facility is nearest the [Courtyard by Marriott SF Downtown Hotel](https://www.google.com/maps/place/Courtyard+by+Marriott+San+Francisco+Downtown/@37.785751,-122.3997608,16.7z/data=!4m12!1m6!3m5!1s0x8085807c643d1007:0x85815e04bf8d233c!2sCourtyard+by+Marriott+San+Francisco+Downtown!8m2!3d37.7859011!4d-122.3969222!3m4!1s0x8085807c643d1007:0x85815e04bf8d233c!8m2!3d37.7859011!4d-122.3969222).
    - Contact: [Rose - Current Director](https://child-care-preschool.brighthorizons.com/ca/sanfrancisco/2ndstreet)

### Travel Guidance: COVID-19

Our top priority is the health and safety of our team members, please refer to the current [travel policy](/handbook/travel/#travel-guidance-covid-19). The CEO Shadow program is classified as non-essential travel and travel to San Francisco will not be required during the time frame specified in the policy linked. CEO Shadows joining the program should plan on participating in the program remotely and matching the CEO's schedule which is primarily in the Pacific time zone unless the CEO is traveling to another time zone. If you have questions please use `#ceo-shadow` in slack and @ mention the [Staff EBA to the CEO](/handbook/eba/#executive-business-administrator-team)

## Considerations for other companies starting CEO Shadow programs

GitLab co-founder and CEO Sid Sijbrandij [answered questions in a YouTube livestream](https://youtu.be/ExG8_bnIAMI) from Sam Altman, as the two discussed considerations for implementing a CEO Shadow program in other organizations. Key takeaways are documented below.

1. CEOs should not optimize meetings for Shadows. They are learning by being in the room, either in-person or virtual, and it's OK if the Shadow doesn't fully understand everything.
1. A well-designed CEO Shadow program shouldn't burden a CEO; in fact, Shadows should actively make a CEO's day easier by assisting with notes and changing relevant portions of the company handbook upon request.
1. Non-obvious benefits for a CEO (and their organization) include CEO empathy and humanizing a CEO, such that team members are more comfortable contributing input to an executive. Shadow alumni are able to translate real-world examples of [assuming positive intent](/handbook/values/#assume-positive-intent) from their time in the program to their direct reports, further fortifying company culture.
1. Ensure that CEO Shadows do not plan to do any of their usual work. Shadows should prepare their team as if they were on vacation. Attempting to shadow the CEO while also maintaining a full workload creates undue stress for the CEO Shadow.

## Alumni

CEO Shadow program alumni are welcome to join the `#ceo-shadow-alumni` Slack channel to stay in touch after the program.

| Start date | End date | Name | Title | Takeaways |
| ---------- | -------- | ---- | ----- | --------- |
| 2019-03 | 2019-04 | [Erica Lindberg](https://gitlab.com/erica) | Manager, Content Marketing | [CEO shadow learnings video](https://www.youtube.com/watch?v=xrWR0uU4nbQ) |
| 2019-04 | 2019-05 | [Mayank Tahil](https://gitlab.com/mayanktahil) | Alliances Manager |  |
| 2019-04 | 2019-05 | [Tye Davis](https://gitlab.com/davistye) | Sr. Technical Marketing Manager | [Without a shadow of a doubt: Inside GitLab's CEO shadow program](/blog/2019/07/11/without-a-shadow-of-a-doubt/) |
| 2019-05 | 2019-06 | [John Coghlan](https://gitlab.com/johncoghlan) | Evangelist Program Manager | [5 Things you might hear when meeting with GitLab's CEO](/blog/2019/06/28/five-things-you-hear-from-gitlab-ceo/) |
| 2019-06 | 2019-06 | [Cindy Blake](https://gitlab.com/cblake) | Sr. Product Marketing Manager | [CEO shadow learnings video](https://www.youtube.com/watch?v=3hel57Sa2EY) |
| 2019-06 | 2019-06 | [Nnamdi Iregbulem](https://gitlab.com/whoisnnamdi) | MBA Candidate at Stanford University |  |
| 2019-06 | 2019-06 | [Clinton Sprauve](https://gitlab.com/csprauve) | PMM, Competitive Intelligence |  |
| 2019-06 | 2019-07 | [Lyle Kozloff](https://gitlab.com/lyle) | Support Engineering Manager |  |
| 2019-07 | 2019-07 | [Marin Jankovski](https://gitlab.com/marin) | Engineering Manager, Deliver |  |
| 2019-07 | 2019-08 | Danae Villarreal | Sales Development Representative, West |  |
| 2019-08 | 2019-08 | [Daniel Croft](https://gitlab.com/dcroft) | Engineering Manager, Package | [GitLab, CEO Shadow August 2019 week one, mind blown](https://www.youtube.com/watch?v=VHcA_2UsC2k) |
| 2019-08 | 2019-08 | [Emilie Schario](https://gitlab.com/emilie) | Data Engineer, Analytics | [What I learned about our CEO's job from participating in the CEO Shadow Program](/blog/2019/10/07/what-i-learned-about-our-ceo-s-job-from-participating-in-the-ceo-shadow-program/) |
| 2019-08 | 2019-08 | [Kenny Johnston](https://gitlab.com/kencjohnston) | Director of Product, Ops |  |
| 2019-09 | 2019-09 | [Eric Brinkman](https://gitlab.com/ebrinkman) | Director of Product, Dev |  |
| 2019-09 | 2019-10 | [Danielle Morrill](https://gitlab.com/dmor) | General Manager, Meltano |  |
| 2019-10 | 2019-10 | [Mek Stittri](https://gitlab.com/meks) | Director of Quality |  |
| 2019-10 | 2019-11 | [Kyla Gradin](https://gitlab.com/kyla) | Mid Market Account Executive |  |
| 2019-10 | 2019-11 | [Clement Ho](https://gitlab.com/ClemMakesApps) | Frontend Engineering Manager, Monitor:Health |  |
| 2019-11 | 2019-11 | [Brendan O'Leary](https://gitlab.com/brendan) | Sr. Solutions Manager |  |
| 2019-11 | 2019-11 | [Gabe Weaver](https://gitlab.com/gweaver) | Sr. Product Manager, Plan: Project Management |  |
| 2019-11 | 2020-01 | [Chenje Katanda](https://gitlab.com/ckatanda) | Technical Account Manager |  |
| 2020-01 | 2020-01 | [Dov Hershkovitch](https://gitlab.com/dhershkovitch) | Senior Product Manager, Monitor |  |
| 2020-01 | 2020-01 | [Keanon O'Keefe](https://gitlab.com/kokeefe) | Senior Product Manager, Plan : Portfolio Management |  |
| 2020-01 | 2020-01 | [Dylan Griffith](https://gitlab.com/DylanGriffith) | Staff Backend Engineer, Search |  |
| 2020-01 | 2020-02 | [Brittany Rohde](https://gitlab.com/brittanyr) | Manager, Compensation & Benefits | [How the CEO Shadow Program boosted my individual productivity during the COVID-19 Crisis](/blog/2020/05/29/how-the-ceo-shadow-program-boosted-my-individual-productivity-during-the-covid-19-crisis/) |
| 2020-01 | 2020-02 | [Nadia Vatalidis](https://gitlab.com/Vatalidis) | Senior Manager, People Operations |  |
| 2020-02 | 2020-02 | [Diana Stanley](https://gitlab.com/dstanley) | Senior Support Engineer |  |
| 2020-02 | 2020-02 | [Chloe Whitestone](https://gitlab.com/chloe) | Technical Account Manager |  |
| 2020-02 | 2020-02 | [Sarah Waldner](https://gitlab.com/sarahwaldner) | Senior Product Manager - Monitor: Health |  |
| 2020-02 | 2020-03 | [Shaun McCann](https://gitlab.com/shaunmccann) | Support Engineering Manager | [CEO Shadow AMA with Support Engineering](https://www.youtube.com/watch?v=A80HKDBRaNE) |
| 2020-03 | 2020-03 | [Lien Van Den Steen](https://gitlab.com/lienvdsteen) | People Ops Fullstack Engineer |  |
| 2020-03 | 2020-03 | [Michael Terhar](https://gitlab.com/mterhar) | Technical Account Manager | [The HyperGrowth Calendar](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44584) |
| 2020-03 | 2020-04 | [Christen Dybenko](https://gitlab.com/cdybenko) | Sr Product Manager |  |
| 2020-04 | 2020-04 | [Scott Stern](https://gitlab.com/sstern) | Frontend Engineer |  |
| 2020-04 | 2020-04 | [Stella Treas](https://gitlab.com/streas) | Chief of Staff to the CEO |  |
| 2020-04 | 2020-04 | [Bradley Andersen](https://gitlab.com/elohmrow) | Technical Account Manager |  |
| 2020-04 | 2020-04 | [Cassiana Gudgenov](https://gitlab.com/cgudgenov) | People Operations Specialist |  |
| 2020-04-28 | 2020-05-08 | [Betsy Church](https://gitlab.com/bchurch) | Senior Talent Brand Manager | [Reflecting on the CEO Shadow Program at GitLab](https://youtu.be/DGJCuMVp6FM) |
| 2020-05-04 | 2020-05-15 | [Darren Murph](https://gitlab.com/dmurph) | Head of Remote | [GitLab CEO Shadow recap — key takeaways and lessons learned from a remote rotation](/blog/2020/05/22/gitlab-remote-ceo-shadow-takeaways/) |
| 2020-05-11 | 2020-05-22 | [Emily Kyle](https://gitlab.com/emily) | Manager, Corporate Events |  |
| 2020-05-11 | 2020-05-22 | [Candace Williams](https://gitlab.com/cwilliams3) | Diversity, Inclusion & Belonging Manager |  |
| 2020-05-25 | 2020-06-05 | [Sophie Pouliquen](https://gitlab.com/spouliquen1) | Senior Technical Account Manager |  |
| 2020-06-01 | 2020-06-19 | [Jackie Meshell](https://gitlab.com/jmeshell) | Senior Product Manager, Release Management | [Recording 📹: Four things I learned as a CEO Shadow](https://youtu.be/iAiYAnkdQLQ) |
| 2020-06-15 | 2020-06-26 | [Wayne Haber](https://gitlab.com/whaber) | Director Engineering, Defend | [Video](https://www.youtube.com/watch?v=JVaPRtY6wMQ) and [Blog](/blog/2020/07/08/ceo-shadow-impressions-takeaways/): What is the GitLab CEO shadow program? Why should you apply to participate? How did I see the GitLab values in action? |
| 2020-06-22 | 2020-07-03 | [Jim Riley](https://gitlab.com/jrileyinva) | Area Sales Manager - Public Sector |  |
| 2020-06-28 | 2020-07-17 | [Hila Qu](https://gitlab.com/hilaqu) | Director of Product, Growth |  |
| 2020-07-13 | 2020-07-31 | [David DeSanto](https://gitlab.com/ddesanto) | Director of Product, Secure & Defend |  |
| 2020-07-20 | 2020-08-10 | [Tim Rizzi](https://gitlab.com/trizzi) | Senior Product Manager, Package |  |
| 2020-08-10 | 2020-08-21 | [Amy Brandenburg](https://gitlab.com/abrandenburg) | Technology Alliances Manager |  |
| 2020-08-17 | 2020-08-28 | [Sam White](https://gitlab.com/sam.white) | Senior Product Manager, Defend |  |
| 2020-08-24 | 2020-09-04 | [Mike Miranda](https://gitlab.com/mikemiranda) | Account Executive, SMB |  |
| 2020-08-31 | 2020-09-11 | [Francis Potter](https://gitlab.com/francispotter) | Sr. Solution Architect |  |
| 2020-09-07 | 2020-09-18 | [Shawn Winters](https://gitlab.com/ShawnWinters) | Acceleration SDR |  |
| 2020-09-07 | 2020-09-24 | [Chris Baus](https://gitlab.com/chris_baus) | Engineering Manager, Fulfillment |  |
| 2020-09-21 | 2020-10-02 | [Philippe Lafoucrière](https://gitlab.com/plafoucriere) | Distinguished Engineer, Secure & Defend | [Take Aways](https://youtu.be/kG-RhvHAeo4), [Retrospective](https://youtu.be/Gv_EQLEgCwU) |
| 2020-09-28 | 2020-10-05 | [Madeline Hennessy](https://gitlab.com/mhennessy) | Area Sales Manager, SMB - US East | [Takeaways & Suggestion](https://www.youtube.com/watch?v=khqnlj6iB9I&feature=youtu.be) |
| 2020-10-05 | 2020-10-23 | [Michael LeBeau](https://gitlab.com/mlebeau) | Strategic Content Lead |  |
| 2020-10-19 | 2020-10-30 | [Edmond Chan](https://gitlab.com/edmondchan) | Sr. Solutions Architect |  |
| 2020-10-26 | 2020-11-06 | [David Fisher](https://gitlab.com/dfishis1) | Acceleration SDR |  |
| 2020-11-06 | 2020-11-13 | [Fernando Diaz](https://gitlab.com/fjdiaz) | Technical Marketing Manager | [4 Things I've Learned as a CEO Shadow](https://awkwardferny.medium.com/4-things-ive-learned-as-a-ceo-shadow-gitlab-3dc0604f24cd) |
| 2020-11-09 | 2020-11-20 | [Dan Parry](https://gitlab.com/dparry) | Mid-Market Account Executive | |
| 2020-11-16 | 2020-12-04 | [Lis Vinueza](https://gitlab.com/lisvinueza) | Business Systems Analyst | |
| 2020-11-30 | 2020-12-11 | [Kevin Chu](https://gitlab.com/kbychu) | Group Product Manager | |
| 2020-12-07 | 2021-01-08 | [Charlie Ablett](https://gitlab.com/cablett) | Sr. Backend Engineer, Plan | |
| 2021-01-04 | 2021-01-15 | [Tanya Pazitny](https://gitlab.com/tpazitny) | Quality Engineering Manager, Secure & Enablement | |
| 2021-01-11 | 2021-01-22 | [Michael Preuss](https://about.gitlab.com/company/team/#mpreuss22) | Senior Manager, Digital Experience | |
| 2021-01-18 | 2021-01-29 | [Traci Robinson](https://gitlab.com/traci) | Sr. PMM, Regulated Industries | |
| 2021-01-25 | 2021-02-05 | [Parker Ennis](https://gitlab.com/parker_ennis) | Sr. PMM, GitLab CI/CD | |
| 2021-02-05 | 2021-02-16 | [Shane Rice](https://gitlab.com/shanerice) | Manager, Search Marketing | [CEO Shadow learning recap](https://youtu.be/VZ6XF0qhWVg) |
| 2021-02-15 | 2021-02-26 | [Lauren Barker](https://gitlab.com/laurenbarker) | Sr. Full Stack Engineer, Digital Experience | [CEO Shadow takeaways from Barker](/blog/2021/02/26/ceo-shadow-takeaways-from-barker/) |
| 2021-02-22 | 2021-03-12 | [Jessica Reeder](https://gitlab.com/jessicareeder) | All-Remote Campaign Manager | |
| 2021-03-08 | 2021-03-19 | [Robert Kohnke](https://gitlab.com/rkohnke) | Marketing Strategy and Performance, Data Analyst | |
| 2021-03-15 | 2021-03-26 | [Sarah Daily](https://gitlab.com/sdaily) | Senior Marketing Operations Manager | [Interview with Sid: Non-Technical Roles to Technical Roles](https://www.youtube.com/watch?v=6_ux_eX4ZYU) |
| 2021-03-22 | 2021-04-02 | [Darwin Sanoy](https://gitlab.com/darwinjs) | Senior Solutions Architect, Alliances | |
| 2021-03-29 | 2021-04-09 | [Anthony Ogunbowale - Thomas](https://gitlab.com/anthonyot) | Named, Account Executive EMEA | |
| 2021-04-05 | 2021-04-16 | [Katie Gammon](https://gitlab.com/kgammon) | Executive Business Administrator | |
| 2021-04-12 | 2021-04-23 | [Pilar Mejia](https://gitlab.com/pmejia) | Distribution Manager, Public Sector | [3 habits to maximize your work day: CEO Shadow insights](https://www.youtube.com/watch?v=JCgblax26LM) |
| 2021-04-19 | 2021-04-30 | [Joanna Shih](https://gitlab.com/jo_shih) | Quality Engineering Manager, Ops | |
| 2021-04-30 | 2021-05-07 | [Jacie Bandur](https://gitlab.com/jbandur) | Learning & Development Generalist | [CEO Shadow takeaways from Jacie](/blog/2021/05/18/ceo-shadow-recap/) |
| 2021-05-03 | 2021-05-14 | [Mikołaj Wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | Backend Engineer, Product Intelligence | |
| 2021-05-10 | 2021-05-21 | [James Komara](https://gitlab.com/ja-me-sk) | Area Sales Manager | |
| 2021-05-17 | 2021-05-28 | [Joshua Lambert](https://gitlab.com/joshlambert) | Director of Product, Enablement | |
| 2021-05-24 | 2021-06-04 | [Melissa Ushakov](https://gitlab.com/mushakov) | Group Manager, Product Management - Plan | |
| 2021-06-01 | 2021-06-11 | [April Malone](https://gitlab.com/april5) | Sijbrandij Personal EA | |
| 2021-06-07 | 2021-06-18 | [Taylor Medlin](https://gitlab.com/taylor) | Solutions Architect - Commercial| |
| 2021-06-14 | 2021-06-25 | [Vincy Wilson](https://gitlab.com/vincywilson) | Quality Engineering Manager, Fulfillment, Growth & Sec | |
| 2021-06-21 | 2021-07-02 | [Simon Liang](https://gitlab.com/sliang2) | Principal Internal Communications Manager | |
| 2021-06-28 | 2021-07-30 | [Christine Lee](https://gitlab.com/christinelee) | Strategy and Operations Director | |
| 2021-08-02 | 2021-08-06 | [Tye Davis](https://gitlab.com/davistye) Shadow Alumni | Manager, Technical Marketing | |
| 2021-08-02 | 2021-08-13 | [Nuritzi Sanchez](https://gitlab.com/nuritzi) | Sr. Open Source Program Manager | |
| 2021-08-09 | 2021-08-20 | [Neil McCorrison](https://gitlab.com/nmccorrison) | Frontend Engineering Manager, Secure | |
| 2021-08-16  | 2021-08-30 | [Christina Hupy](https://gitlab.com/c_hupy)| Manager, Education Programs | [CEO Shadow Reflections](https://youtu.be/ZmLIP5jQS4g) |
| 2021-08-30  | 2021-09-10 | [Sarah Bailey](https://gitlab.com/sbailey1) | Solutions Architect Manager | |
| 2021-09-07  | 2021-09-17 | [Kris Reynolds](https://gitlab.com/kreykrey) | Manager, Field Enablement Programs | |
| 2021-09-13  | 2021-09-24 | [Darren Murph](https://gitlab.com/dmurph) Shadow Alumni | Head of Remote | |
| 2021-09-20  | 2021-10-01 | [Christen Dybenko](https://gitlab.com/cdybenko) Shadow Alumni | Sr. Product Manager | |
| 2021-09-28 <br/>2021-10-18 | 2021-10-01 <br/>2021-10-22 | [Kyle Wiebers](https://gitlab.com/kwiebers) | Engineering Manager, Eng Productivity | |
| 2021-10-18 | 2021-10-29 | [Omar Eduardo Fernandez](https://gitlab.com/ofernandez2) | Director, Strategy and Operations | |
| 2021-10-25 <br/>2021-11-08 | 2021-10-29 <br/>2021-11-12 | [Taurie Davis](https://gitlab.com/tauriedavis) | Product Design Manager, Foundations | |
| 2021-11-08 | 2021-11-19 | [REB - Richard E. Baum](https://gitlab.com/xyzzy) | Sr. Solutions Architect | |
| 2021-11-15 | 2021-12-03 | [Sincheol (David) Kim](https://gitlab.com/dskim_gitlab) | Sr. Backend Engineer | |
| 2021-11-30 | 2021-12-10 | [Amy Qualls](https://gitlab.com/aqualls) | Sr. Technical Writer | |
