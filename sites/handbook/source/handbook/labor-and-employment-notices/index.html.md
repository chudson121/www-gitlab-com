---
layout: handbook-page-toc
title: "Labor and Employment Notices"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
{::options parse_block_html="true" /}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Since GitLab is an [all remote](/company/culture/all-remote/) company we don't have a physical worksite or breakroom wall upon which to post important labor and employment notices, so this page is our version of that.

<details>

<summary markdown="span">GitLab, Inc. & GitLab Federal LLC</summary>

### Alabama
* [Child Labor Law](https://labor.alabama.gov/docs/posters/childlaborlawposter_english.pdf)
* [Your Job Insurance - Unemployment Insurance](https://www.labor.alabama.gov/docs/posters/uc_jobinsurance.pdf)
* [Workers' Compensation Information](https://www.labor.alabama.gov/docs/posters/wc_information.pdf)

### Alaska
* [Safety and Health Protection on the Job](https://labor.alaska.gov/lss/forms/right-to-know.pdf)
* [Emergency Information](https://labor.alaska.gov/lss/forms/EmergInfo.pdf)
* [Human Rights Law - Sexual Harassment](https://labor.alaska.gov/lss/forms/EmergInfo.pdf)
* [Unemployment Insurance](https://labor.alaska.gov/lss/forms/1012.pdf)
* [Summary of Alaska Child Labor Law](https://labor.alaska.gov/lss/forms/child-labor-law-summary.pdf)
* [Summary of Alaska Wage and Hour Act](https://labor.alaska.gov/lss/forms/sum-wh-act-2021.pdf)
* [Alaska - Americans with Disabilities Act](http://doa.alaska.gov/ada/policy/DLWDColor.pdf)
* [Alaska - Alcohol and Drug-free Workplace Policy](https://doa.alaska.gov/dop/fileadmin/Employee_Orientation/pdf/DrugFreeWorkplacePoster.pdf)
* [State of Alaska Whistleblower Act](https://doa.alaska.gov/dop/fileadmin/StatewidePlanning/pdf/WhistleblowerActPoster.pdf)

### Arizona
* [Constructive Discharge Notice](https://hr.az.gov/sites/default/files/Notification_of_Constructive_Discharge_Poster.pdf)
* [Discrimination is Prohibited in Employment](https://www.azag.gov/sites/default/files/publications/2018-07/Discrimination_Brochure.pdf)
* [Earned Paid Sick Time](https://www.azica.gov/sites/default/files/AZ%20Earned%20Paid%20Sick%20Time%20Poster%202017.pdf)
* [Employee Safety and Health Protection](https://www.ica.state.az.us/sites/default/files/ADOSH_Poster_WorkplaceSafetyBilingual.pdf)
* [Minimum Wage Act](https://www.azica.gov/sites/default/files/media/THE%20FAIR%20WAGES%20AND%20HEALTHY%20FAMILIES%20ACT%20(6).pdf)
* [Unemployment Insurance](https://des.az.gov/sites/default/files/legacy/dl/POU-003.pdf)
* [Worker's Compensation](https://www.ica.state.az.us/sites/default/files/migrated_pdf/Claims_Poster_WorkersCompLawBilingual.pdf)
* [Work Exposure to Bodily Fluids](https://www.ica.state.az.us/sites/default/files/migrated_pdf/Claims_Poster_WorkExpToBodilyFluids_HIV_AIDS_HepC.pdf)
* [Work Exposure to MRSA, Spinal Meningitis, or Tuberculosis](https://www.ica.state.az.us/sites/default/files/migrated_pdf/Claims_Poster_WorkExpToMRSA_SpMen_TB.pdf)

### Arkansas
* [Minimum Wage, Overtime, Child Labor, Wage Collection Notice](https://www.labor.arkansas.gov/wp-content/uploads/2020/09/POSTER-FINAL-2019.pdf)
* [How to Claim Unemployment Insurance](https://www.dws.arkansas.gov/unemployment/how-to-file-a-ui-claim/)
* [Worker's Compensation Instructions](http://www.awcc.state.ar.us/revisedforms/formp.pdf)

### California
* [California Law Prohibits Workplace Discrimination and Harassment](https://www.dfeh.ca.gov/wp-content/uploads/sites/32/2020/10/Workplace-Discrimination-Poster_ENG.pdf)
* [Transgender Rights in the Workplace](https://www.dfeh.ca.gov/wp-content/uploads/sites/32/2019/08/DFEH_TransgenderRightsWorkplace_ENG.pdf)
* [Your Rights and Obligations as a Pregnant Employee](https://www.dfeh.ca.gov/wp-content/uploads/sites/32/2020/12/Your-Rights-and-Obligations-as-a-Pregnant-Employee_ENG.pdf)
* [Family Care & Medical Leave & Pregnancy Disability Leave](https://www.dfeh.ca.gov/wp-content/uploads/sites/32/2020/12/CFRA-and-Pregnancy-Leave_ENG.pdf) 
* [Industrial Welfare Commission Wage Order](https://www.dir.ca.gov/IWC/IWCArticle17.pdf)
* [California Minimum Wage](https://www.dir.ca.gov/iwc/MW-2019.pdf)
* [Paid Sick Leave](https://www.dir.ca.gov/DLSE/Publications/Paid_Sick_Days_Poster_Template_(11_2014).pdf)
* [Safety and Health Protection on the Job](https://www.dir.ca.gov/dosh/dosh_publications/shpstreng012000.pdf)
* [Notice to Employees - Injuries Caused by Work](https://www.dir.ca.gov/dwc/NoticePoster.pdf)
* [Whistleblower Protections](https://www.dir.ca.gov/dlse/WhistleblowersNotice.pdf)
* [Notice to Employees - unemployment, disability, and paid family leave insurance](https://www.edd.ca.gov/pdf_pub_ctr/de1857a.pdf)
* [Sexual Harassment Fact Sheet](https://www.dfeh.ca.gov/wp-content/uploads/sites/32/2020/03/SexualHarassmentFactSheet_ENG.pdf)
* [Supplemental Paid Sick Leave - COVID-19](https://www.dir.ca.gov/dlse/2021-COVID-19-Supplemental-Paid-Sick-Leave.pdf)
* [Payday Notice](https://drive.google.com/file/d/1eIhDxbf42Jfvy9DjuYiLyuLQFHqMcx9I/view?usp=sharing)
* [Right to Vote Notice](https://elections.cdn.sos.ca.gov/pdfs/tov-english.pdf)
* [Workers' Compensation Notice](https://www.dir.ca.gov/DWC/NoticePoster.pdf)
* **City of Berkeley** 
  - [Minimum Wage Poster](https://www.cityofberkeley.info/MWO/)
* **City of Los Angeles**
  - [Fair Chance Ordinance](https://bca.lacity.org/Uploads/fciho/Fair%20Chance%20Initiative%20for%20Hiring%20Ordinance%20for%20Private%20Employers.pdf)
  - [Minimum Wage and Paid Sick Leave](https://file.lacounty.gov/SDSInter/dca/245570_FinalMinimumWageOrdinancePosterEnglishStandardSize8.5x14.pdf)
* **County of Los Angeles**
  - [Minimum Wage Ordinance](https://file.lacounty.gov/SDSInter/dca/245570_FinalMinimumWageOrdinancePosterEnglishStandardSize8.5x14.pdf)
* **City of Oakland**
  - [Minimum Wage/Paid Sick Leave](https://www2.oaklandnet.com/oakca1/groups/contracting/documents/marketingmaterial/oak061391.pdf)
* **City of San Jose**
  - [Minimum Wage Bulletin](https://www.sanjoseca.gov/home/showpublisheddocument/67133/637417196663600000)
  - [Opportunity to Work Notice](https://www.sanjoseca.gov/home/showdocument?id=20073)
* **City of San Francisco**
  - [Minimum Wage Poster](https://sfgov.org/olse/sites/default/files/minimum%20wage%20poster%202021.pdf)
  - [Paid Sick Leave](https://sfgov.org/olse/sites/default/files/Document/Paid%20Sick%20Leave%20Poster%20-%20Post.pdf)
  - [Fair Chance Ordinance](https://sfgov.org/olse/fair-chance-ordinance-fco)
  - [Health Care Security Ordinance](https://sfgov.org/olse/sites/default/files/Document/HCSO%20Files/2019%20HCSO%20Poster%20Final.pdf)
  - [Family Friendly Workplace Ordinance](https://sfgov.org/olse/sites/default/files/FileCenter/Documents/11256-FFWO%20Official%20Notice.pdf)
  - [Paid Parental Leave Ordinance](https://sfgov.org/olse/sites/default/files/2020%20parental%20leave%20poster%20Print.pdf)
  - [Salary History Ordinance](https://sfgov.org/olse/sites/default/files/Document/Consideration%20of%20Salary%20History%20Poster%20upload.pdf)

### Colorado
* [Anti-Discrimination Laws](https://drive.google.com/file/d/1mre-jdp29cyAT7L-KjGaVYnPdJcUy9KL/view)
* [Employment Security Act](https://www.colorado.gov/pacific/sites/default/files/502_NoticeToWorkers-Poster.pdf)
* [Minimum Wage and Overtime Pay Standards](https://cdle.colorado.gov/sites/cdle/files/COMPS%20Order%20%2337%20%282021%29%20Poster%20CLEAN.pdf)
* [Notice to Employer of Injury](https://www.colorado.gov/pacific/sites/default/files/WC050_Notice_of_Injury_Poster.pdf)
* [Notice of Paydays](https://drive.google.com/file/d/1B9NG0eCOvxGD_KE_R9Zxtw58lTi4fkje/view?usp=sharing)
* [Pregnancy Accommodations](https://www.colorado.gov/pacific/sites/default/files/CCRD%20Notice%20re%20Pregnant%20Workers%20Fairness%20Act%20%282%29.pdf)
* [Worker's Compensation Act](https://drive.google.com/file/d/1OkdN7QSD23d5-etB7awiU4C_JjM9KXHM/view?usp=sharing)
* [Paid Leave, Whistleblowing, and Personal Protective Equipment](https://cdle.colorado.gov/sites/cdle/files/Poster%2C%20Paid%20Leave%20%26%20Whistleblower%20-%202021%20poster.pdf)

### Connecticut
* [Connecticut Commission on Human Rights and Opportunities Sexual Harassment Notice](https://portal.ct.gov/-/media/CHRO/SexualHarassmentPreventionPosterpdf.pdf)
* [Paid Sick Leave](https://www.ctdol.state.ct.us/wgwkstnd/NoticeSickLeavePoster2014%20.pdf)
* [CT Paid Leave](https://ctpaidleave.my.salesforce.com/sfc/p/#t00000004XRe/a/t00000017vPH/IYK_GaizuYSGI4PeNMl128HN2hn8O1vZ9diq3q7VKX8)
* [Discrimination is Illegal](https://portal.ct.gov/-/media/CHRO/DiscriminationFlyerpdf.pdf)
* [Minimum Wage](https://www.ctdol.state.ct.us/wgwkstnd/DOL-75.pdf)
* Unemployment Insurance Notice
* [Worker's Compensation Notice](https://drive.google.com/file/d/1SQgiLIBPJCPvl1jcPnRwqjOpsE-SfhV1/view?usp=sharing)
* [Electronic Monitoring Notice](https://www.ctdol.state.ct.us/wgwkstnd/ElectMonitoring.pdf)
* [Office of the Healthcare Advocate](https://portal.ct.gov/-/media/OHA/OHAPstr8p5x11AsnEngMchpdf.pdf)
* [Pregnancy Discrimination and Accommodation in the Workplace](https://www.ctdol.state.ct.us/gendocs/SS46a%20Pregnancy%20Disability%20Poster.pdf)

### Massachusetts
* [Paid Family and Medical Leave](https://www.mass.gov/doc/2022-paid-family-and-medical-leave-mandatory-workplace-poster/download)
* [Earned Sick Time](https://www.mass.gov/doc/earned-sick-time-notice-of-employee-rights/download)
* [Massachusetts Wage and Hours Laws](https://www.mass.gov/doc/massachusetts-wage-hour-laws-poster/download)
* [Fair Employment Law](https://www.mass.gov/doc/fair-employment-poster/download)
* [Parental Leave Act](https://www.mass.gov/service-details/parental-leave-in-massachusetts)
* [Information on Employees Unemployment Insurance Coverage](https://www.mass.gov/doc/information-on-employees-unemployment-insurance-coverage-form-2553a/download)

### New York
* [New York Correction Law, Article 23-A](https://labor.ny.gov/formsdocs/wp/correction-law-article-23a.pdf)
* [New York State Human Rights Law](https://dhr.ny.gov/sites/default/files/doc/poster.pdf)
* [Equal Pay Provision](https://labor.ny.gov/formsdocs/wp/LS603.pdf)
* [Minimum Wage](https://labor.ny.gov/formsdocs/wp/LS207.pdf)

### Pennsylvania
* [Minimum Wage Law](https://www.dli.pa.gov/Documents/Mandatory%20Postings/llc-1.pdf)
* [Abstract of Equal Pay Law](https://www.dli.pa.gov/Documents/Mandatory%20Postings/llc-8.pdf)
* [Employment Provisions of the PA Human Relations Act](https://www.phrc.pa.gov/About-Us/Publications/Documents/Required%20Posters/Fair%20Employment.pdf)

#### City of Philadelphia
* [Promoting Healthy Families and Workplaces](https://www.phila.gov/media/20191218103833/Paid-Sick-Leave-Poster-Translations.pdf)

#### City of Pittsburgh
* [Paid Sick Days Act](https://apps.pittsburghpa.gov/redtail/images/9692_Notice-Paid-Sick-Days-Act_06-2020.pdf)

### Washington
* [Paid Family and Medical Leave Act](https://paidleave.wa.gov/app/uploads/2019/12/Employer-poster.pdf)
* [Job Safety and Health Law](https://www.lni.wa.gov/forms-publications/f416-081-909.pdf)
* [Your Rights as a Worker](https://www.lni.wa.gov/forms-publications/F700-074-000.pdf)

### EEOC (U.S. Equal Employment Opportunity Commission) Notices
* ["EEO is the Law" English poster for screen readers](https://www.eeoc.gov/sites/default/files/migrated_files/employers/poster_screen_reader_optimized.pdf)
* ["EEO is the Law" English poster for printing](https://www.eeoc.gov/sites/default/files/migrated_files/employers/eeoc_self_print_poster.pdf)
* ["EEO is the Law" Spanish poster for printing](https://www.eeoc.gov/sites/default/files/migrated_files/employers/eeoc_self_print_poster_spanish.pdf)
* ["EEO is the Law" Poster Supplement](https://www.dol.gov/sites/dolgov/files/ofccp/regs/compliance/posters/pdf/OFCCP_EEO_Supplement_Final_JRF_QA_508c.pdf)
* All ["EEO is the Law"](https://www1.eeoc.gov/employers/poster.cfm) poster links.

### E-Verify 
* [Notice of E-Verify Participation Poster](https://www.e-verify.gov/sites/default/files/everify/posters/EVerifyParticipationPoster.pdf) 
* [Right to Work Poster](https://www.e-verify.gov/sites/default/files/everify/posters/IER_RighttoWorkPoster.pdf)

### Employee Polygraph Protection Act
* [EPPA Poster](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/eppac.pdf)

### Employee Rights on Government Contracts
* [Employee Rights on Government Contracts (SCA, PCA, CWHSSA, Walsh- Healey)](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/govc.pdf)

### Employee Rights under NLRA
* [Employee Rights Under the National Labor Relations Act (one-page Poster)](https://www.dol.gov/sites/dolgov/files/olms/regs/compliance/eo_posters/employeerightsposter11x17_2019final.pdf)
* [Employee Rights Under the National Labor Relations Act (two-page Poster)](https://www.dol.gov/sites/dolgov/files/olms/regs/compliance/eo_posters/employeerightsposter2page_19final.pdf)

### Fair Labor Standards Act (FLSA) Minimum Wage 
* [Fair Labor Standards Act Poster](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/minwagep.pdf)
* [Section 14(c)](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/disabc.pdf)
* [Lactation Accommodation Policy](https://drive.google.com/file/d/1davxfjYkXYNzChFXyRTgfmK90pNP3f7a/view?usp=sharing)

### Family First Coronavirus Act (FFCRA)
* [Employee Rights: Paid Sick Leave and Expanded Family and Medical Leave under the Family First Coronavirus Act](https://www.dol.gov/sites/dolgov/files/WHD/posters/FFCRA_Poster_WH1422_Non-Federal.pdf)

### Family and Medical Leave Act 
* [Family and Medical Leave Act (FMLA) Poster](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/fmlaen.pdf)

### Federal Minimum Wage for Contractors Poster
* [Worker Rights Under Executive Order 13658: Federal Minimum Wage for Contractors Poster](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/mw-contractors.pdf)

### OSHA Job Safety and Health
* [Job Safety and Health: It's the Law Poster](https://www.osha.gov/sites/default/files/publications/osha3165.pdf)

### Pay Transparency Nondiscrimination Provision
* [Pay Transparency Nondiscrimination Provision](https://www.laborposters.org/federal/1606-federal-pay-transparency-poster.htm)

### Paid Sick Leave for Federal Contractors
* [Worker Rights Under Executive Order 13706: Paid Sick Leave for Federal Contractors](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/wh1090.pdf)

### Service Contract Act
* [Service Contract Act](https://www.dol.gov/sites/dolgov/files/WHD/legacy/files/govc.pdf)

</details>

<details>

<summary markdown="span">GitLab Canada Corp.</summary>

### Ontario
* [Employment Standards in Ontario](https://files.ontario.ca/mltsd-employment-standards-poster-en-2020-09-08.pdf)
* [Occupational Health & Safety Poster](https://files.ontario.ca/mltsd_2/mltsd-prevention-poster-en-2020-07-22.pdf)
* [Occupational Health & Safety Act](https://www.ontario.ca/laws/statute/90o01)

### British Columbia
* [Working in B.C.](https://www2.gov.bc.ca/assets/gov/employment-business-and-economic-development/employment-standards-workplace-safety/employment-standards/factsheets-pdfs/working_in_bc_infosheet.pdf)
* [Occupational Health and Safety Regulation](https://www.worksafebc.com/en/law-policy/occupational-health-safety/searchable-ohs-regulation/ohs-regulation)
* [Workers Compensation Act](https://www.worksafebc.com/en/law-policy/occupational-health-safety/searchable-ohs-regulation/ohs-guidelines/guidelines-for-workers-compensation-act)

### Alberta
* [Workers Compensation Act](https://www.wcb.ab.ca/assets/pdfs/employers/123_english.pdf)
* [Employment Standards Code](https://www.alberta.ca/assets/documents/es-general-online-poster.pdf)

### Manitoba 
* [Safe Work Manitoba](https://www.safemanitoba.com/Page%20Related%20Documents/resources/BR_EveryonesResponsibilityLong_15SWMB.pdf)

</details>

<details>

<summary markdown="span">GitLab BV</summary>

### Netherlands
* [European Agency for Safety and Health at Work - Netherlands](https://osha.europa.eu/en/about-eu-osha/national-focal-points/netherlands)
* [OSH - Netherlands](https://www.arboineuropa.nl/en/arbo-in-the-netherlands/)
* [Health and Safety at Work - Netherlands](https://business.gov.nl/regulation/health-safety-work/)

</details>

<details>

<summary markdown="span">GitLab GmbH</summary>

* [European Agency for Safety and Health at Work](https://osha.europa.eu/en)
* [Working Hours Act](https://www.gesetze-im-internet.de/arbzg/index.html) 

</details>
