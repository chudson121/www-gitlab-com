---
layout: handbook-page-toc
title: Talent brand
description: "Talent Brand is sharing what it's like to work at GitLab, because we wouldn't be successful without our people and our culture."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

As a global, all-remote company, GitLab has a unique story to tell.
A key part of telling that story is sharing what it's like to work here, because we wouldn't be successful without our people and our culture.
This page outlines our approach to talent brand at GitLab.

### Our audience

The audience we hope to reach with our talent brand initiatives is both internal and external to GitLab:

- GitLab team members
- Job candidates and future team members
- The broader GitLab community
- People interested in remote work

## GitLab team member value proposition

We've defined our team member value proposition, and you can find it on the [GitLab Culture page](/company/culture/#life-at-gitlab). We focus on these tenets as we tell the story of what it's like to be part of our global, all-remote team.


## Digital channels for talent branding

### Jobs site

Our [GitLab jobs site](/jobs/) is where candidates can find information about working at GitLab, along with a link to view our job opportunities.

### Social media

We incorporate content about hiring and our culture on GitLab's [social media](/handbook/marketing/corporate-marketing/social-marketing/) accounts so that there's one central place for candidates and the community to find out more about the company as a whole.

On LinkedIn, we have a specific [career page](https://www.linkedin.com/company/gitlab-com/life) where candidates can find out more about life at GitLab.

There are a number of videos on our [YouTube channel](https://www.youtube.com/gitlab) that relate to working here:
- [Why work remotely?](https://youtu.be/GKMUs7WXm-E)
- [Everyone can contribute](https://youtu.be/kkn32x0POTE)
- [Working remotely at GitLab](https://youtu.be/NoFLJLJ7abE)
- [This is GitLab](https://youtu.be/5QeHmiMFhDE)
- [What is GitLab?](https://youtu.be/MqL6BMOySIQ)
- [GitLab's core values](https://youtu.be/_8DFFHYAtj8)


### Review sites

We want to be sure candidates who come across GitLab's profile on employer review sites have an accurate picture of what it's like to work here.
There are some sites where GitLab has a company profile, but we do not own it or pay for additional features. On others, we have a managed presence.

We encourage team members to leave reviews and share their stories on these sites to continue to keep an updated profile. We welcome both positive and negative feedback in these reviews.

#### Glassdoor

##### Engaged employer

As an engaged employer with Glassdoor, we're able to customize the branded content, videos, links, and images on our Glassdoor profile. Our contract with Glassdoor includes pages in these countries:
- United States
- United Kingdom
- Germany
- The Netherlands
- India
- Ireland
- Rest of World (all countries where Glassdoor does not have a separate presence)

##### Responding to Glassdoor reviews

The [Talent Brand Manager](/job-families/people-ops/talent-branding-manager/) keeps track of new company reviews and escalates them as needed for an external response and internal action. [Here's our process](/handbook/people-group/employment-branding/glassdoor-escalation/) for tracking and escalating reviews.

##### OpenCompany designation

As an open, transparent company, the OpenCompany designation is important for us to have to best represent our talent brand. We take action to maintain this status throughout the year.

Achieving and maintaining OpenCompany requires that you:
- Keep company profile up to date
- Add 5-10 new photos every 12 months
- Get 5-60 new employee reviews (depending on company size) every 12 months
- Respond to 2-10 reviews (depending on company size) every 12 months


##### Profile updates on Glassdoor

To be sure the details on our profile stay up to date, we [review these items](https://gitlab.com/gl-talent acquisition/talent-brand/-/issues/9) quarterly and make any needed updates:
- Headcounts and country numbers listed on our profile
- Our status as one of the world's largest all-remote companies
- New awards or recognition
- Photos or videos recently published


#### Comparably

Comparably is an employer review site that also offers recruitment marketing tools and [award programs](/handbook/people-group/employment-branding/#employer-awards-and-recognition). We no longer have a paid profile with Comparably (ended in 2020).

Our goal on the site is to maintain our profile and keep the feedback up to date by annually (Q3) sending a link to all team members where they can consider leaving feedback or a review.

#### Other employer sites

- [RemoteHub](https://remotehub.io/gitlab)
- [Indeed](https://www.indeed.com/cmp/Gitlab-Inc/about)
- [AngelList](https://angel.co/company/gitlab/)
- [RemoteFit](https://www.remotefit.io/c/gitlab)

### GitLab blog

To give the most authentic view of life at GitLab, we encourage team members to blog about their experiences.
You can find many of these posts in the [culture section](/blog/categories/culture/) of the GitLab blog.

### HackerNews

We promote life at GitLab and our open roles on [HackerNews](https://news.ycombinator.com/).

#### Who's Hiring monthly post

On the first of the month (or closest business day after) at 11 a.m. ET, the Talent Brand Manager will post a comment for GitLab in the Hacker News thread called ["Who's Hiring"](https://news.ycombinator.com/ask).
Here's a template that will be updated monthly by the Talent Brand Manager:

*Template text (updated for 2021-05-01):*

GitLab, Remote only, Full time

As one of the world’s largest all-remote companies, GitLab is a place where you can contribute from almost anywhere. We're an ambitious, productive team that embraces a set of shared values in everything we do.

We were recently named one of Fortune's Best Workplaces in Technology, and it’s an exciting time to join the GitLab team.

We're hiring across the company. Check out our opportunities: https://about.gitlab.com/jobs/careers/  

**HackerNews Notes:**
- Sid's HackerNews credentials should not to be used for the "Who's Hiring" post. The team member posting the comment should use their own account.
- When posting on HackerNews, remember that it is our most important social channel. Please follow the [best practices](/handbook/marketing/community-relations/developer-evangelism/hacker-news/#best-practices-when-responding-on-hacker-news) and [social media guidelines](/handbook/marketing/community-relations/developer-evangelism/hacker-news/#social-media-guidelines).

## Talent brand resources

### Content library

The [life at GitLab content library](/handbook/people-group/employment-branding/content-library/) is a (WIP) curated list of blog posts, articles, videos, awards, and quick facts that help tell the story of life at GitLab.

### GitLab talent ambassadors

Spread the word about life at GitLab by becoming a GitLab talent ambassdor.

Whether you're a hiring manager or an individual contributor, [this page](/handbook/hiring/gitlab-ambassadors) outlines the steps you can take to help represent our talent brand and attract more great people to join the team.

## Employer awards and recognition

Check out the content library for a [list of the latest awards and recognition](/handbook/people-group/employment-branding/content-library/#awards-and-lists-recognizing-gitlab-as-a-great-place-to-work) related to working at GitLab.

### How we apply for employer and workplace awards

We partner closely with the Corporate Marketing team to apply for and track employer awards and recognition each year. More details and our process for applying to these awards can be found [on this page](https://about.gitlab.com/handbook/people-group/employment-branding/awards-process/).

These awards are important to GitLab's talent brand beacuse they raise awareness for why GitLab is such a unqiue place to work.

## All-remote work

A foundational aspect of our talent brand is the flexibility and autonomy that all-remote work gives our team members. The Talent Brand Manager collaborates closely with the [All-Remote Marketing team](/handbook/marketing/corporate-marketing/all-remote/) to raise awareness about remote work and tell the stories of how it's impacted the lives of our team members. Learn more about GitLab's approach to remote work on our [all-remote page](/company/culture/all-remote/).

## Performance indicators

Here are the definitions for the performance indicators listed in the talent brand job family.

### Glassdoor engagement

- Company rating
- Page views
- Followers
- Apply starts on jobs

### LinkedIn Talent Brand metrics

- Career page impressions
- Percentage of page visitors who view jobs
- Percentage of new hires who visit our page

### Team member engagement score

Our team member engagement or net promoter ("I would recommend GitLab as a great place to work.") score is measured annually in the Culture Amp survey.
Here are samples of the statements team members were asked to consider:
- I would recommend GitLab as a great place to work
- GitLab motivates me to go beyond what I would in a similar role elsewhere
- I am proud to work for GitLab
- I rarely think about looking for a job at another company
- I see myself still working at GitLab in two years' time

### Team member turnover

[Defined](/handbook/people-group/people-operations-metrics/#team-member-turnover) on the People Group Metrics page.

### Team member referrals

Overall number of job candidate referrals from GitLab team members.

### Hires vs. plan

[Defined](/handbook/hiring/metrics/#hires-vs-plan) on the People Group Metrics page.

